export const environment = {
  production: true,
  region: 'us-west-2',
  identityPoolId: 'us-west-2:78b08530-0121-4f8e-9cea-05f72326ef72',
  userPoolId: 'us-west-2_DZH7aTJUY',
  clientId: 'f2vaj24au4qiup1nu6ttjfajn',

  rekognitionBucket: 'rekognition-pics',
  albumName: "usercontent",
  bucketRegion: '',
  ddbTableName: 'user_login_log',

  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  sts_endpoint: '',
  dynamodb_endpoint: '',
  s3_endpoint: ''
};