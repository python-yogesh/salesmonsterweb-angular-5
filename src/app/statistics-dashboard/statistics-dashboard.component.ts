import { Component, OnInit } from '@angular/core';
import { DealService, Pipeline, UserService } from '../service/user.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';

import { SharedComponent } from '../shared/_sharedComponent'
import { Baseurl } from '../service/baseUrl'

declare var moment:any;
// declare var toastr:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './statistics-dashboard.component.html',
  styleUrls: ['./statistics-dashboard.component.css']
})
export class StatisticsDashboardComponent implements OnInit {
  private _sharedComponent = new SharedComponent();
  private dealData; 
  private wondealData 
  private lostdealData;
  private toggleValue1 = true;
  private showcurrencyClass1 = 'showcurrency';
  private closeCurrencies1 = 'Close currencies';
  private toggleValue2 = true;
  private showcurrencyClass2 = 'showcurrency';
  private closeCurrencies2 = 'Close currencies';
  private toggleValue3 = true;
  private showcurrencyClass3 = 'showcurrency';
  private closeCurrencies3 = 'Close currencies';
  base_class_obj = new Baseurl();

  constructor(private daterangepickerOptions: DaterangepickerConfig, private deal_services: DealService, public pipeline_service: Pipeline,
              private authUser: UserService) {
    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: true,
      ranges: {
        'Yesterday':[moment().subtract(1, 'day'), moment().subtract(1, 'day')],
        'Today':[moment()],
        'This month':[moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'This year': [moment().startOf('year'), moment().endOf('year')],
      }
    };

    let start_at = this._sharedComponent.dateToYMD(new Date(this.mainInput.start) );
    let end_at = this._sharedComponent.dateToYMD(new Date(this.mainInput.end) );
    this.get_pipeline_list();
    this.get_user_list();
    for (var i = 1; i <= 3; i++) {
      this.get_statistics_deal_v2_(i);
    }
    // toastr.options = { "progressBar": true }
  }

  private static_data = { 'pipeline_id': 'all', 'user_id': 'all', 
    'start_date': this._sharedComponent.dateToYMD(new Date(moment().startOf('month'))),
    'end_date': this._sharedComponent.dateToYMD(new Date(moment().endOf('month')))
  };

  ngOnInit() {}

  // dealstatus, pipeline_uuid, startDate, deal_admin_name, endDate, subdomain
  get_statistics_deal_v2_(dealstatus){
    this.deal_services.get_statistics_deal_v2(dealstatus, this.static_data.pipeline_id, this.static_data.user_id, 
      this.static_data.start_date, this.static_data.end_date, this.base_class_obj.subdomain).subscribe(data=>{
        console.log(data)
        if (dealstatus==1)
          this.dealData = data;
        else if (dealstatus==2)
          this.wondealData = data;
        else if (dealstatus==3)
          this.lostdealData = data;
    });
  }

  private pipeline_list_arr = [];
  get_pipeline_list(){
    this.pipeline_service.get_pipeline_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.pipeline_list_arr = data.Items;
    }, error=> console.log('error=',error));
  }

  filter_by_pipeline(pipeline_uuid){
    this.static_data.pipeline_id =  pipeline_uuid;
    for (var i = 1; i <= 3; i++) {
      this.get_statistics_deal_v2_(i);
    }
  }

  filter_by_users(user_email){
    this.static_data.user_id = user_email;
    for (var i = 1; i <= 3; i++) {
      this.get_statistics_deal_v2_(i);
    }
  }

  private users_list = [];
  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.users_list = data['users'];
    });
  }


  toggleCurrencies1(x){
    // console.log(x);
    if (x==true)
    {
      this.toggleValue1 = false;
      this.showcurrencyClass1 = 'hidecurrency';
      this.closeCurrencies1 = 'Show currencies';
    }else
    {
      this.toggleValue1 = true;
      this.showcurrencyClass1 = 'showcurrency';
      this.closeCurrencies1 = 'Close currencies';
    }
  }

  toggleCurrencies2(x){
    // console.log(x);
    if (x==true)
    {
      this.toggleValue2 = false;
      this.showcurrencyClass2 = 'hidecurrency';
      this.closeCurrencies2 = 'Show currencies';
    }else
    {
      this.toggleValue2 = true;
      this.showcurrencyClass2 = 'showcurrency';
      this.closeCurrencies2 = 'Close currencies';
    }
  }

  toggleCurrencies3(x){
    // console.log(x);
    if (x==true)
    {
      this.toggleValue3 = false;
      this.showcurrencyClass3 = 'hidecurrency';
      this.closeCurrencies3 = 'Show currencies';
    }else
    {
      this.toggleValue3 = true;
      this.showcurrencyClass3 = 'showcurrency';
      this.closeCurrencies3 = 'Close currencies';
    }
  }

  // calendar
  public mainInput = {
      start: moment().startOf('month'),
      end: moment().endOf('month'),
      opens: "left"
  }
  
  public eventLog = '';
  private selectedDate(value: any, dateInput: any) {
      this.static_data.start_date = this._sharedComponent.dateToYMD(new Date(value.start));
      this.static_data.end_date = this._sharedComponent.dateToYMD(new Date(value.end));
      for (var i = 1; i <= 3; i++) {
        this.get_statistics_deal_v2_(i);
      }
      dateInput.start = value.start;
      dateInput.end = value.end;
  }

  private applyDate(value: any, dateInput: any) {
      dateInput.start = value.start;
      dateInput.end = value.end;
  }

  public calendarEventsHandler(e:any) {
      this.eventLog += '\nEvent Fired: ' + e.event.type;
  }
}

@Component({
  selector: 'app-follower',
  templateUrl: './follower.component.html',
  styleUrls: ['./follower.component.css']
})

export class FollowerComponent{

}