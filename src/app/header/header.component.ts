import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, EventEmitter } from '@angular/core';
import {Router} from "@angular/router";
// import services
import { StripeServices, UserService, Pipeline ,CompanyService} from '../service/user.service';
import { UserLoginService } from "../service/user-login.service";
import { CookieService, CookieOptionsArgs } from 'angular2-cookie/core';
import { SharedComponent }  from '../shared/_sharedComponent';
import { Baseurl } from '../service/baseUrl';

declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user_object = {};
  private company_list;
  private company_setting: boolean;
  
  base_class_obj = new Baseurl();
  private _sharedComponent = new SharedComponent();
  @Input() company: string;
  @Input() username: string;
  @Input() img_url: any = 'assets/images/blank_profile_male.jpg';
  @Input() company_logo: any = '';
  @Output() set_org_uuid = new EventEmitter<any>();  
  @ViewChild('changecompany') changecompany;

  constructor(public router: Router, public userService: UserLoginService, 
              public authUser: UserService, public pipeline_service: Pipeline,
              public compnayService:CompanyService, public stripe_services: StripeServices,
              private _cookieService: CookieService){
      this.userService.getUserParameters(this);
      this.get_organization();
      this.get_userProfile();
      this.get_organization_logo();
      this.get_company_list();
      // console.log('header component');
  }

  ngOnInit(){}
  callback(){}

  get_organization(){
    this.authUser.get_organization_(this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('show organiozation', data.json());
      this.company = data['response_organizations'].Item.company_name;
      this.get_billing_info(data['response_organizations'].Item.uuid);
      this.set_org_uuid.emit(data['response_organizations'].Item.uuid);
    },
      // The 2nd callback handles errors.
      (err) => console.error(err),
      // The 3rd callback handles the "complete" event.
      // () => console.log("observable complete")
    );
  }

  billing_data;
  private data = {trail_days:''}
  
  get_billing_info(uuid){
    this.stripe_services.get_billing_info_api(uuid).subscribe(data=>{
      // console.log(data)
      if(data.status == 'past_due'){
        this.router.navigate(['/auth/subscribe']);
      }
      this.billing_data = data;
    });
  }

   get_company_list(){
    this.authUser.get_company_data().subscribe(data=>{
       this.company_list = data;
       if(this.company_list==""){
         this.company_setting = false;
       }
       else{
         this.company_setting = true;
       }
    });
  }
  
  private organization_uuid

  open_new_tab(org){
    this.organization_uuid = org.uuid
    var data = {};
    this.authUser.get_organization_name(this.organization_uuid, data).subscribe(data=>{
      this.changecompany.close();
    });
    window.open(this.router.url);
  }

  get_userProfile(){
    this.authUser.get_user_profile_api(this.base_class_obj.subdomain).subscribe(data=>{
      if (data.userprofile!='') {     
        var profile_data = data.userprofile;
        if (profile_data.image_url == "removed"){
          this.img_url = 'assets/images/blank_profile_male.jpg';
        }else{
          this.img_url = profile_data.image_url || 'assets/images/blank_profile_male.jpg';
        }
      }
    });
  }

  get_organization_logo(){
    this.compnayService.getCompanyLogo(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        if(data['Items'].length != 0){
          
          if("image_url" in data['Items'][0]){
             this.company_logo = data['Items'][0]['image_url'];
             // console.log(this.company_logo);    
          }
        }else{
          this.company_logo = 'assets/images/Asset-1.png';
        }
      }
    });
  }

  // call back for getUserParameters
  callbackWithParam(result: any){
    for (let i = 0; i < result.length; i++){ 
      this.user_object[result[i].Name] = result[i].Value;
    }
    this.username = this.user_object["nickname"];
  }

  onLogout(){
    this.userService.logout();
    localStorage.removeItem('user_detail');
    this.router.navigate(['']);
    this._cookieService.remove('IsAdmin');
    // window.location.reload();
    localStorage.clear();
  }

  ngAfterViewInit(){
    // $(".nav li").on("click", function() {
    //   $(".nav li").removeClass("active");
    //   $(this).addClass("active");
    // });
  }
}