import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalComponent } from 'ng2-bs3-modal';
import { UserService, Pipeline, CompanyService } from '../service/user.service';
import { Baseurl } from '../service/baseUrl';

declare var toastr: any;
declare var $: any;
declare var google: any;

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.css']
})

export class CompanyEditComponent implements OnInit {
  @ViewChild('editCompanyPopup') editCompanyPopup_modal: BsModalComponent;
  @ViewChild('addPredefinedPopup') addLostReason_modal: BsModalComponent;
  @ViewChild('activityPopup') activityPopup_modal: BsModalComponent;
  @ViewChild('addFollowerPopup') add_follower_popup: BsModalComponent;
  @ViewChild('editfollower') editfollower: BsModalComponent;
  @ViewChild('addLocationPopup') addLocationPopup: BsModalComponent;

  base_class_obj = new Baseurl();
  
  private userSettings2 = { "showRecentSearch": false, "showCurrentLocation":false, 'inputString': '',
                                  "showSearchButton": false, "inputPlaceholderText": 'Enter Physical Location' };
  private addLocationForm: FormGroup;
  private addLostReasonForm: FormGroup;
  private addFollowerForm: FormGroup;
  private loader_image: boolean;
  private currencyOrganization: boolean;
  private organization_currency_field;
  private lost_reason_field_boolean=false;
  private lost_reason_field;
  private logoOrganizationUrl='';
  private active_activities='active';
  private currecy_active='active';
  private show_only: boolean;
  
  private orgUUID;
  private physical_location_list = [];
  constructor(public authUser: UserService, public compnayService: CompanyService, private pipeline_service: Pipeline, public fb: FormBuilder){
    this.get_organization();
    this.currencyOrganization=true;
    this.get_organizationCustomField(this.currencyOrganization);
    this.get_lost_reason();
    this.get_activity();
    this.get_organization_logo();
    this.get_followers_list();
    this.get_pipeline_list();
    this.get_activity_trigger_data();
    this.get_activity_trigger_status();
    this.initialize_location_form();
    this.initialize_lost_reason_form();
    this.initialize_add_follower_form();
  }

  initialize_location_form(){
    this.addLocationForm = this.fb.group({
      'name': [''],
      'address': [''],
      'phone_number': [''],
      'organization_uuid': ['']
    });
  }

  initialize_lost_reason_form(){
    this.addLostReasonForm = this.fb.group({
      'lost_reason': ['', Validators.compose([Validators.required])],
    });
  }

  initialize_add_follower_form(){
    this.addFollowerForm = this.fb.group({
      'follower_title': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit(){}
  ngAfterViewInit(){
    $('[data-toggle="tooltip"]').tooltip();
  }

  map: any;
  addMarker(LatLng, formatted_address){
    var lat = LatLng.lat
    var lng = LatLng.lng
    var mapCanvas = document.getElementById('map');
    var mapOptions = {
      center: new google.maps.LatLng(lat, lng),
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(mapCanvas, mapOptions)
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,  
      position: this.map.getCenter()
    });
    let content = "<p style='font-weight: bold;'>"+formatted_address+"</p>";
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }
  // @Output decoretor in header component.
  get_org_uuid(event){
    this.get_physical_location(event);
    this.orgUUID = event;
  }

  private address = { 'location':'', 'formatted_address':'' };
  autoCompleteCallback(e){
    if (e.response) {
      this.address.formatted_address = e.data.formatted_address;
      this.address.location = e.data.geometry.location;
    }else{
      this.address.formatted_address = '';
      this.address.location = '';
    }
  }

  create_location(){
    if (this.address.location == '') {
      alert('Enter Physical Location');
      return;
    }
    if (this.addLocationForm.valid) {
      this.addLocationForm.value.address = JSON.stringify(this.address);
      this.addLocationForm.value.organization_uuid = this.orgUUID;
      this.addLocationPopup.close();
      this.compnayService.create_location_api(this.addLocationForm.value).subscribe(res=>{
        if (res['ResponseMetadata'].HTTPStatusCode == 200) {
          this.get_physical_location(this.orgUUID);
          this.initialize_location_form();
        }
      });
    }
  }

  get_physical_location(orgUUID){
    this.compnayService.get_physical_location_api(orgUUID).subscribe(res=>{
      res.filter((x)=>{
        x.address = JSON.parse(x.address);
      });
      this.physical_location_list = res;
    });
  }

  edit_physical_location(data){
    this.addLocationPopup.open();
  }

  delete_physical_location(uuid){
    this.compnayService.delete_physical_location_api(uuid).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('Delete successfully');
        this.get_physical_location(this.orgUUID);
      }
    });
  }

  company_name;
  get_organization(){
    this.loader_image=true;
    this.authUser.get_organization_(this.base_class_obj.subdomain).subscribe(data=>{
      console.log('show organiozation', data);
      this.company_name = data['response_organizations'].Item.company_name;
      if(data!=''){
          this.loader_image=false;
        }
    },
      // The 2nd callback handles errors.
      (err) => console.error(err),
      // The 3rd callback handles the "complete" event.
      // () => console.log("observable complete")
    );
  }

  lost_reason_deleted(uuid){
    this.authUser.lost_reason_deleted_api(uuid).subscribe(data=>{
    if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('Delete  Successfully'); 
          this.get_lost_reason();

       }
    });
  }

  get_organizationCustomField(is_show){
    this.compnayService.get_organization_currency_field_api(is_show, this.base_class_obj.subdomain).subscribe(data=>{
    if (data['ResponseMetadata'].HTTPStatusCode == 200) {
      if(data['Items']!=[])
        {
          this.organization_currency_field = data['Items'];
        } 
        // console.log(data);
      }
    });
  }

  // update 'Important' and '  Show in Add new Dailog' fields value true/false
  update_currencyActivation(value, uuid){
    // console.log(value, uuid);
    this.compnayService.update_currency(value, uuid).subscribe(data=>{
      // console.log(data);
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('update successfully');
        this.get_organizationCustomField(this.currencyOrganization);
        // this.list_of_fields();
      }
    });
  }

  private currecy_deactive;
  activate_currency(is_show){
    this.currencyOrganization = is_show;
    if(this.currencyOrganization==false){
      this.currecy_deactive='active';
      this.currecy_active='';

    }
    else{
      this.currecy_active='active';
      this.currecy_deactive='';
    }
    this.get_organizationCustomField(this.currencyOrganization);
  }

  update_company(company_name){
    let data = { 'company_name': company_name }
    this.authUser.update_comany_name(data, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.editCompanyPopup_modal.close();
        this.get_organization();
      }
    });
  }

  private image_data:any = { 'image': "" }
  onChange(evt){
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload =this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  private base64textString: String="";
  _handleReaderLoaded(readerEvt) {
      var binaryString = readerEvt.target.result;
      this.base64textString = btoa(binaryString);
      this.image_data.image = this.base64textString;
      // console.log(this.image_data.image);
  }

  logo_update(){
    var result={};
    if (this.image_data.image!='') {
      for(var key in this.image_data) result[key] = this.image_data[key];
    }
    this.compnayService.updateCompanyLogo(result, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success("update successfully");
        this.get_organization_logo();
      }
    });
  }

  get_organization_logo(){
    this.compnayService.getCompanyLogo(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        if(data['Items'].length != 0){
          if("image_url" in data['Items'][0]){
             this.logoOrganizationUrl = data['Items'][0]['image_url'];
          }
        }else{
          this.logoOrganizationUrl = 'assets/images/Asset-1.png';
        }
      }
    });
  }

  private submit_lost: boolean = false;
  private edit_lost_reason_value: boolean = true;
  addPredefine(){
    this.submit_lost = true;
    if (this.addLostReasonForm.valid) {
      this.addLostReason_modal.close();
      if (this.edit_lost_reason_value) { // create new lost reason
        this.authUser.addPredefine(this.addLostReasonForm.value, this.base_class_obj.subdomain).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            this.get_lost_reason();
            this.initialize_lost_reason_form();
            this.submit_lost = false;
          }
        });
      }else{
        // update lost reason
        this.authUser.lost_reason_edit_api(this.edit_lost_uuid, this.addLostReasonForm.value).subscribe(data=>{
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
              toastr.success('Update Successfully');
              this.get_lost_reason();
              this.initialize_lost_reason_form();
           }
        });
      }
    }
  }

  private edit_lost_uuid;
  editLostRes(uuid, lost_reason){
    this.edit_lost_reason_value = false;
    this.edit_lost_uuid = uuid;
    this.addLostReasonForm.controls.lost_reason.patchValue(lost_reason);
    this.addLostReason_modal.open();
  }

  private list_of_followers = [];
  private followers_uuid;
  private follower_value: boolean = true;
  private submit_follower: boolean = false;
  add_followers(follower_title){
    this.submit_follower = true;
    if (this.addFollowerForm.valid) {
      this.add_follower_popup.close();
      if (this.follower_value) {
        // add follower method
        this.authUser.addFollower(this.addFollowerForm.value, this.base_class_obj.subdomain).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            this.submit_follower = false;
            this.get_followers_list();
          }
        });
      }else{
        // update follower method
        this.authUser.edit_follower(this.followers_uuid, this.addFollowerForm.value).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            toastr.success('Update Successfully');
            this.editfollower.dismiss();
            this.get_followers_list();
           }
        });
      }
    }
  }

  get_followers_list(){
    this.authUser.get_follower_list(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        if(data['Items']!=[]){
          this.list_of_followers = data['Items'];
        }
      }
    });
  }
  
  editfollowers(follower_title, uuid){
    this.follower_value = false;
    this.followers_uuid = uuid;
    this.add_follower_popup.open();
    this.addFollowerForm.controls.follower_title.patchValue(follower_title);
  }

  save_edit_follower(value){
    let data={'follower_title': value }
    this.authUser.edit_follower(this.followers_uuid,data).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('Update Successfully'); 
        this.editfollower.dismiss();
        this.get_followers_list();
       }
    });
  }

  delete_followers(uuid){
    this.authUser.delete_follower(uuid).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('Delete  Successfully'); 
          this.get_followers_list();
        }
     })
   }

  get_lost_reason(){
    this.show_only=false;
    this.authUser.get_lost_reason(this.base_class_obj.subdomain).subscribe(data=>{
    if (data['ResponseMetadata'].HTTPStatusCode == 200) {
      if(data['Items']!=[])
        {
          this.lost_reason_field = data['Items'];
          this.lost_reason_field_boolean = true;
          if(data['Count']==0){
          this.show_only=true;
          }
        }
      }
    });
  }

  private activity = { 'label': 'Call', 'tooltip_name': 'call', 'fa_fa_icon': 'fa fa-phone'};
  
  private label_name;
  private active_class;
  private active_already;
  private active_onclick;

  activity_data(tooltip_name, fa_icon){
    this.active_class=tooltip_name || 'Call';
    this.activity.tooltip_name = tooltip_name;
    this.activity.fa_fa_icon = fa_icon;
    this.active_already='';
    this.active_onclick='active';
  }

  private active_value;
  create_activity_icon(label_name){
    this.activity.label = label_name || 'Call';
    // console.log('create_activity_icon',this.activity)
    if(this.active_value){
      // console.log(this.active_value);
      this.compnayService.create_activity_api(this.activity, this.base_class_obj.subdomain)
      .subscribe(data=>{
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          this.label_name = null;
          toastr.success('Add Successfully');
          this.get_activity();
          this.activityPopup_modal.close();
        }
      });
    }
    else{
      let params = {'label': label_name }
      var activity_uuid=this.uuid;
      this.compnayService.active_deactive_activity_api(params, activity_uuid, this.base_class_obj.subdomain)
      .subscribe(data=>{
        // console.log('edit activity responce', data);
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('Edit Successfully');
          this.get_activity();
          this.activityPopup_modal.close();
          
        }
      });
      // console.log(this.active_value);
    }
  }

  private tooltip_name;
  private show_heading;
  private uuid;
  // open avtivity modal popup when we click on 'Edit' Button
  edit_activity_popup(label_name,tooltip_name,value,uuid_name){ // value return false
    this.uuid=uuid_name;
    this.tooltip_name=tooltip_name;
    this.active_value = value;
    this.label_name = label_name;
    this.activityPopup_modal.open();
    this.active_already='active';
    this.active_onclick=''; 
    this.show_heading='Edit activity type';
  }
  // open avtivity modal popup when we click on 'Add activity type' Button
  add_activity_popup(value){ // value return true
    this.active_value = value;
    this.label_name = '';
    this.activityPopup_modal.open();
    this.active_onclick='';
    this.active_already='';
    this.show_heading='Add activity type';
  }

  private list_of_activity_fields;
  private activate_value = true;
  private activity_icon_name = [];
  private selected_activity_type_uuid;
  get_activity(){
    this.compnayService.get_company_activity_fields_api(this.base_class_obj.subdomain).subscribe(data=>{
      var res = data.Items.filter((x)=>{ return x.is_activate === this.activate_value });
      this.activity_icon_name = data.Items.filter((x)=>{ return x.is_activate === true });
      this.selected_activity_type_uuid = this.activity_icon_name[0].uuid;
      this.list_of_activity_fields = res;
      console.log('activity responce', this.activity_icon_name);
    });
  }

  activate_deactivate_activity(activity_uuid, is_active){
    let params = {'is_activate': is_active }
    this.compnayService.active_deactive_activity_api(params, activity_uuid, this.base_class_obj.subdomain)
    .subscribe(data=>{
      // console.log('activity active deactive data responce', data);
      if (data.status != 400) {
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          this.activate_value = true;
          this.get_activity();
        }
      }
    });
  }

  // get 'Deactivate' users list
  private deactive_activity;
  activate_activity(value: boolean){
    this.activate_value = value;
    if(this.activate_value==false){
      this.deactive_activity='active';
      this.active_activities='';

    }
    else{
      this.active_activities='active';
      this.deactive_activity='';
    }
    this.get_activity()
    
  }

  private pipeline_list_arr = [];
  private selected_pipeline_uuid;
  get_pipeline_list(){
    this.loader_image = true;
    this.pipeline_service.get_pipeline_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      let pipeline_list = data.Items.filter((x)=>{ return x.count !=0 });
      this.selected_pipeline_uuid = data.Items[0].uuid;
      this.pipeline_list_arr = pipeline_list;
      this.get_pipeline_stage(this.selected_pipeline_uuid);
    }, (error)=> console.log('error=', error));
  }

  private stages_list = [];
  private selected_stage_uuid;
  get_pipeline_stage(pipeline_uuid){
    let filter_value = '?user_name=all';
    this.authUser.filter_pipeline_stage_deals(pipeline_uuid, filter_value, this.base_class_obj.subdomain)
    .subscribe(data=>{
      this.stages_list = data[0];
      this.selected_stage_uuid = this.stages_list[0].uuid;
      // console.log('pipeline stages list', data);
    }, (err)=> {});
  }

  private deal_activity_trigger_uuid;
  private is_active:boolean = false;
  get_activity_trigger_status(){
    this.compnayService.get_activity_trigger_status_api().subscribe(data=>{
      this.is_active = data.is_active;
      this.deal_activity_trigger_uuid = data.uuid;
    });
  }

  get_activity_trigger_data(){
    this.compnayService.get_activity_trigger_data_api(this.base_class_obj.subdomain).subscribe(data=>{
      console.log(data);
    });
  }

  update_activity_trigger_status(value){
    var data = { 'is_active': value};
    this.compnayService.update_activity_trigger_status_api(this.deal_activity_trigger_uuid, data).subscribe(data=>{});
  }

  create_activity_trigger(activityTriggerForm){
    var data = activityTriggerForm.value;
    console.log(data)
    this.compnayService.create_activity_trigger_api(this.base_class_obj.subdomain, data).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('Activity trigger created successfully');
      }
    });
  }

  private selected_activity_time = '1_hour';
  active_fields = [
    {tooltip_name:"Task" ,icon:"fa fa-clock-o"},
    {tooltip_name:"Email" ,icon:"fa fa-paper-plane"},
    {tooltip_name:"Meeting" ,icon:"fa fa-users"},
    {tooltip_name:"Deadline" ,icon:"fa fa-flag-checkered"},
    {tooltip_name:"flag" ,icon:"fa fa-flag"},
    {tooltip_name:"Lunch" ,icon:"fa fa-coffee"},
    {tooltip_name:"call" ,icon:"fa fa-phone"},
    {tooltip_name:"calender" ,icon:"fa fa-calendar"},
    {tooltip_name:"downloadarrow" ,icon:"fa fa-download"},
    {tooltip_name:"document" ,icon:"fa fa-file-text"},
    {tooltip_name:"smaartphone" ,icon:"fa fa-mobile"},
    {tooltip_name:"camera" ,icon:"fa fa-camera-retro"},
    {tooltip_name:"scissors" ,icon:"fa fa-scissors"},
    {tooltip_name:"tool" ,icon:"fa fa-wrench"},
    {tooltip_name:"bubble" ,icon:"fa fa-comment"},
    {tooltip_name:"uparrow" ,icon:"fa fa-cloud-download"},
    {tooltip_name:"checkbox" ,icon:"fa fa-sitemap"},
    {tooltip_name:"signpost" ,icon:"fa fa-map-signs"},
    {tooltip_name:"shuffle" ,icon:"fa fa-random"},
    {tooltip_name:"addressbook" ,icon:"fa fa-address-book"},
    {tooltip_name:"linegraph" ,icon:"fa fa-line-chart"},
    {tooltip_name:"picture" ,icon:"fa fa-picture-o"},
    {tooltip_name:"car" ,icon:"fa fa-car"},
    {tooltip_name:"world" ,icon:"fa fa-globe"},
    {tooltip_name:"search" ,icon:"fa fa-search"},
    {tooltip_name:"clip" ,icon:"fa fa-clipboard"},
    {tooltip_name:"sound" ,icon:"fa fa-volume-up"},
    {tooltip_name:"brush" ,icon:"fa fa-paint-brush"},
    {tooltip_name:"key" ,icon:"fa fa-key"},
    {tooltip_name:"padlock" , icon:"fa fa-lock"},
    {tooltip_name:"pricetag", icon:"fa fa-tag"},
    {tooltip_name:"suitcase" ,icon:"fa fa-suitcase"},
    {tooltip_name:"finish" ,icon:"fa fa-trophy"},
    {tooltip_name:"plane" ,icon:"fa fa-plane"},
    {tooltip_name:"loop" ,icon:"fa fa-refresh"},
    {tooltip_name:"wifi", icon:"fa fa-wifi"},
    {tooltip_name:"truck", icon:"fa fa-truck"},
    {tooltip_name:"cart" ,icon:"fa fa-shopping-cart"},
    {tooltip_name:"bell" ,icon:"fa fa-bell"},
    {tooltip_name:"presentations" ,icon:"fa fa-caret-square-o-right"}
  ]
}
