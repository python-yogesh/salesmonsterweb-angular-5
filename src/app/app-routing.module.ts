import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './auth/login.component';
import { SignupComponent } from './auth/signup.component';
import { ConfirmComponent } from './auth/confirm.component';
import { ForgetPasswordComponent } from './auth/forget-password.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompanyRegistrationComponent } from './auth/company-registration.component';
import { SettingComponent } from './setting/setting.component';
import { UserPermissionsComponent } from './setting/user-permissions/user-permissions.component';
import { AddUsersComponent } from './setting/add-users/add-users.component';
import { PipelinesComponent } from './pipelines/pipelines.component';
import { ListDealsComponent } from './dashboard/list-deals/list-deals.component';
import { ResendCodeComponent } from './auth/resend-code.component';
import { CustomizeFieldsComponent } from './setting/customize-fields/customize-fields.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { PeoplesComponent } from './contact/peoples/peoples.component';
import { OrganizationsComponent } from './contact/organizations/organizations.component';
// import { DealComponent } from './dashboard/deal/deal.component';
import { DealDetailsComponent } from './deal-details/deal-details.component';
import { ActivityComponent } from './activity/activity.component';
import { ActivityCalendarPickerComponent } from './activity/activity-calendar/activity-calendar-picker.component';
import { ActivityListComponent } from './activity/activity-list/activity-list.component';
import { StatisticsDashboardComponent, FollowerComponent } from './statistics-dashboard/statistics-dashboard.component';
import { FollowersComponent} from './dashboard/followers/followers.component';
import { PeopleComponent } from './dashboard/people/people.component';
import { MailComponent } from './dashboard/mail/mail.component';
import { MailDraftsComponent } from './dashboard/mail/mail-drafts/mail-drafts.component';
import { MailSendComponent } from './dashboard/mail/mail-send/mail-send.component';
import { ArchiveComponent } from './dashboard/mail/archive/archive.component';
import { ComposeMailComponent } from './dashboard/mail/compose-mail/compose-mail.component';
import { PaymentComponent } from './payment/payment.component';
import { ProductComponent, ProductDetailComponent } from './product/product.component';
import { BillingComponent } from './billing/billing.component';
import { DealDetailSectionComponent } from './shared/deal-detail-section/deal-detail-section.component';

const routes: Routes = [
    {path: '', component: WelcomeComponent, pathMatch: 'full'},
    // { path: 'welcome', component: WelcomeComponent,
    //     // children:[
    //     //     {path:'login', component: LoginComponent}
    //     // ]
    // },
    {path:'forgetPassword', component: ForgetPasswordComponent},
    {path:'dashboard/deals', component: ListDealsComponent},
    {path:'login', component: LoginComponent },
    {path:'signup', component: SignupComponent },
    {path:'dashboard', component: DashboardComponent },
    {path:'dashboard/setting', component: SettingComponent, },
    {path:'setting/users', component: UserPermissionsComponent },
    {path:'setting/add', component: AddUsersComponent },
    {path:'setting/pipelines', component: PipelinesComponent },
    {path:'confirm/:username', component: ConfirmComponent },
    {path:'company-registration', component: CompanyRegistrationComponent },
    {path:'resend-verification-code', component: ResendCodeComponent },
    {path:'setting/customize-fields', component: CustomizeFieldsComponent },
    {path:'setting/company/edit', component: CompanyEditComponent },
    {path:'contacts/persons', component: PeoplesComponent },
    {path:'contacts/organizations', component: OrganizationsComponent },
    {path:'statistics/dashboard', component: StatisticsDashboardComponent },
    {path:'followers', component: FollowerComponent },
    {path:'people/:id', component: PeopleComponent },
    {path:'dashboard/mail', component: MailComponent },
    {path:'products', component: ProductComponent},
    {path:'setting/billing', component: BillingComponent },
    
    {path:'dashboard/followers/:id/:deal_id/:name/:email',component:FollowersComponent},
    {path:'dashboard/deal/:dealuuid/:stage_name/:pipeline_id', component: DealDetailsComponent },
    // {path:'dashboard/deal/:dealuuid/:stage_name/:pipeline_id', component: DealComponent },
    {path:'dashboard/activity', component: ActivityComponent,
        // children:[
        //     { path: '', redirectTo: 'calendar', pathMatch: 'full' },
        //     { path: 'calendar', component: ActivityCalendarPickerComponent },
        //     { path: 'list', component: ActivityListComponent },
        // ]
    },
    {path:'dashboard/mail/mail-drafts', component: MailDraftsComponent },
    {path:'dashboard/mail/mail-send', component: MailSendComponent },
    {path:'dashboard/mail/archive', component: ArchiveComponent },
    {path:'dashboard/mail/compose-mail',component: ComposeMailComponent},
    {path:'product/details/:uuid',component: ProductDetailComponent},
    {path:'auth/subscribe', component: PaymentComponent },
    {path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }