import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Pipe } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';  // replaces previous Http service
import { NgSelectModule} from '@ng-select/ng-select';
import { DndModule } from 'ng2-dnd';
import { BsModalModule } from 'ng2-bs3-modal';
import { PopoverModule } from "ng2-popover";
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { InlineEditorModule } from 'ng2-inline-editor';
import { CalendarModule } from 'angular-calendar';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DemoUtilsModule } from './shared/activity-calendar-utils/module';

import { TooltipModule } from "ngx-tooltip";
import { TypeaheadModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Daterangepicker } from 'ng2-daterangepicker';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import { QuillEditorModule } from 'ngx-quill-editor';
import { CookieService } from 'angular2-cookie/services/cookies.service';
// import component
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { WelcomeComponent } from './welcome/welcome.component'
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfirmComponent } from './auth/confirm.component'
import { ChangePasswordComponent } from './private/change-password/change-password.component';
import { ForgetPasswordComponent } from './auth/forget-password.component';
import { LoginComponent } from './auth/login.component';
import { SignupComponent } from './auth/signup.component';
import { CompanyRegistrationComponent } from './auth/company-registration.component';
import { SettingComponent } from './setting/setting.component';
import { HeaderComponent } from './header/header.component';
import { ListDealsComponent } from './dashboard/list-deals/list-deals.component';
import { AddDealsComponent, DemoNumber, RoundPipe } from './add-deals/add-deals.component';
import { UserPermissionsComponent } from './setting/user-permissions/user-permissions.component';
import { AddUsersComponent } from './setting/add-users/add-users.component';
import { SideBarComponent } from './setting/side-bar/side-bar.component';
import { PipelinesComponent } from './pipelines/pipelines.component';
import { TooltipButtonsComponent } from './shared/tooltip-buttons/tooltip-buttons.component';
import { ResendCodeComponent } from './auth/resend-code.component';
import { CustomizeFieldsComponent } from './setting/customize-fields/customize-fields.component';
import { ObjectValuesPipe, OrderBy } from './dashboard/list-deals/list-deals.component';
import { ActivityComponent } from './activity/activity.component';
import { ActivityCalendarPickerComponent } from './activity/activity-calendar/activity-calendar-picker.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { PeoplesComponent } from './contact/peoples/peoples.component';
import { OrganizationsComponent } from './contact/organizations/organizations.component';
import { ActivityListComponent } from './activity/activity-list/activity-list.component';
import { ModalPopupComponent } from './activity/modal-popup/modal-popup.component';
import { StatisticsDashboardComponent, FollowerComponent } from './statistics-dashboard/statistics-dashboard.component';
import { FollowersComponent } from './dashboard/followers/followers.component';
import { PeopleComponent } from './dashboard/people/people.component';
import { MailComponent } from './dashboard/mail/mail.component';
import { MailDraftsComponent } from './dashboard/mail/mail-drafts/mail-drafts.component';
import { MailSendComponent } from './dashboard/mail/mail-send/mail-send.component';
import { ArchiveComponent } from './dashboard/mail/archive/archive.component';
import { ComposeMailComponent } from './dashboard/mail/compose-mail/compose-mail.component';
import { PaymentComponent } from './payment/payment.component';
import { ProductComponent, ProductDetailComponent } from './product/product.component';
import { DealDetailSectionComponent } from './shared/deal-detail-section/deal-detail-section.component';

// import services
import { AwsUtil } from "./service/aws.service";
import { CognitoUtil } from './service/cognito.service';
import { UserRegistrationService } from './service/user-registration.service';
import { UserLoginService } from './service/user-login.service';
import { DynamoDBService } from './service/ddb.service';
import { UserService, Pipeline, CustomFieldsService, PersonService, CompanyService, DealService,
        ActivityEventClass, StripeServices,ProductService} from './service/user.service';

import { BillingComponent } from './billing/billing.component';
import { OrganizationSectionComponent } from './organization-section/organization-section.component';
import { PersonSectionComponent } from './shared/person-section/person-section.component';
import { DealDetailsComponent } from './deal-details/deal-details.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    DashboardComponent,
    ConfirmComponent,
    ChangePasswordComponent,
    ForgetPasswordComponent,
    LoginComponent,
    SignupComponent,
    CompanyRegistrationComponent,
    SettingComponent,
    HeaderComponent,
    ListDealsComponent,
    AddDealsComponent, DemoNumber, RoundPipe,
    UserPermissionsComponent,
    AddUsersComponent,
    SideBarComponent,
    PipelinesComponent,
    TooltipButtonsComponent,
    ResendCodeComponent,
    CustomizeFieldsComponent,
    ObjectValuesPipe, OrderBy, CompanyEditComponent, PeoplesComponent, 
    OrganizationsComponent, ActivityComponent,ProductComponent,
    ActivityCalendarPickerComponent, ActivityListComponent, ModalPopupComponent ,StatisticsDashboardComponent, 
    PeopleComponent, MailComponent, FollowersComponent, MailDraftsComponent, MailSendComponent, ArchiveComponent, ComposeMailComponent, 
    PaymentComponent, ProductComponent, BillingComponent, FollowerComponent,
    DealDetailSectionComponent, OrganizationSectionComponent, PersonSectionComponent, DealDetailsComponent, ProductDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,ReactiveFormsModule, HttpClientModule, PopoverModule,
    HttpModule, NgSelectModule, BsModalModule, QuillEditorModule,
    AppRoutingModule, Ng2AutoCompleteModule, NguiAutoCompleteModule, Daterangepicker, MyDateRangePickerModule,
    DndModule.forRoot(), BrowserAnimationsModule, NgbModule.forRoot(),
    CalendarModule.forRoot(),
    DemoUtilsModule, InlineEditorModule, TypeaheadModule.forRoot(), Ng4GeoautocompleteModule.forRoot(),
    TooltipModule
  ],
  providers: [
    UserLoginService, AwsUtil, DynamoDBService, UserRegistrationService, CognitoUtil, 
    UserService, Pipeline, CustomFieldsService, PersonService, CookieService,
    CompanyService, DealService, ActivityEventClass, StripeServices, ProductService
  ],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}