import { Component, AfterViewInit, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { BsModalComponent } from 'ng2-bs3-modal';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/filter';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, Validators, FormBuilder, NgForm,FormArray } from '@angular/forms';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { Observable } from 'rxjs/Observable';
import { IMyDrpOptions } from 'mydaterangepicker';
//import external classes;
import { SharedComponent } from '../shared/_sharedComponent';
import { DealDetailSectionComponent } from '../shared/deal-detail-section/deal-detail-section.component';
import { OrganizationSectionComponent } from '../organization-section/organization-section.component';
import { PersonSectionComponent } from '../shared/person-section/person-section.component';
import { CustomFieldsService, UserService, CompanyService, DealService, ActivityEventClass, Pipeline,
         PersonService, ProductService } from '../service/user.service';
import { CognitoUtil } from "../service/cognito.service";
import { UserLoginService } from "../service/user-login.service";
import { Baseurl } from '../service/baseUrl';

declare var toastr: any;
declare var $: any;
declare var google: any;
declare var moment: any;

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-deal-details',
  templateUrl: './deal-details.component.html',
  styleUrls: ['./deal-details.component.css']
})
export class DealDetailsComponent implements OnInit {
  myDateRangePickerOptions: IMyDrpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
  };
  
  @ViewChild('addActivity') addActivity_modal: BsModalComponent;
  @ViewChild('takeNoteModalPopup') take_note_modal: BsModalComponent;
  @ViewChild('ownerPopover') input_ownerPopover;
  @ViewChild('dealTitle') input_dealTitle;
  @ViewChild('currencyPopover') currency_popover;

  @ViewChild('deal_detail_section') deal_detail_section: DealDetailSectionComponent;
  @ViewChild('org_section') org_section: OrganizationSectionComponent;
  @ViewChild('person_section') person_section: PersonSectionComponent;

  base_class_obj = new Baseurl();
  edit_activityForm: FormGroup;
  private model: any;
  private _sharedComponent = new SharedComponent();
  private userPermission=false;
  private fa_icon_list: any;
  private activity_list
  private deal_uuid_params;
  private owner_name;
  private pipeline_id;
  private pipeline_name;
  private stage_name;
  private activity_status:boolean = false;
  data = { type: '' , is_type: 'text', label: '', multiple_value: [], is_textbox: false, is_show: false, is_important: false }
  private disabled:boolean = false;
  constructor(private router: Router,private activatedRoute: ActivatedRoute,public authUser: UserService,
      private cognito_user: CognitoUtil, public custom_field_service: CustomFieldsService, 
      private company_services : CompanyService, private deal_services: DealService, private pipeline_services: Pipeline,
      private activity_events: ActivityEventClass,public formBuilder: FormBuilder, private person_services: PersonService,
      private product_services: ProductService, private user_login_service: UserLoginService){

      toastr.options = { "progressBar": true }
      this.activatedRoute.params.subscribe((params: Params) => {
        this.deal_id = params['dealuuid'];
        this.deal_uuid_params = params['dealuuid'];
        this.pipeline_id = params['pipeline_id'];
        // this.pipeline_name = params['pipelinename'];
        this.active_stages = params['stage_name'];
      });

      this.owner_id = this.cognito_user.getCurrentUser().getUsername();
      this.selected_owner_id = this.cognito_user.getCurrentUser().getUsername();
      this.user_login_service.getUserParameters(this);
      this.get_pipeline_list();
      this.get_deal_custom_fields();
      this.get_user_list();
      this.get_takenotes();
      this.get_created_activity();
      this.get_lost_reason();
      this.get_fa_icon_listing();
      this.get_userPermission('export_data_from_list_by_user');
      this.get_activity_list();
      this.get_currency();
      this.setDate_dataRange();
      this.get_participants_list();
      this.get_logs();
      // form
      this.edit_activityForm = this.formBuilder.group({
        'activity_tooltip_name': [''],
        'activity_fa_fa_icon': [''],
        'activity_field_uuid': [''],
        'activityTitle': [''],
        'activity_date': [''],
        'activity_start_time': [],
        'activity_end_time': [],
        'activity_editor': [''],
        'owner_id': [''],
        'deal_title': ['', Validators.compose([Validators.required])],
        'person_name': [''],
        'contact_organization_name': [''],
        'start_at': [''],
        'end_at': [''],
        'owner_name':[''],
        'activity_complete_status': [this.activity_status]
      });
      this.activity_date = this._sharedComponent.dateToYMD(new Date());
  }

  public user_object = {};
  callback(){}
  // call back for getUserParameters
  callbackWithParam(result: any){
    for (let i = 0; i < result.length; i++){ 
      this.user_object[result[i].Name] = result[i].Value;
    }
    console.log(this.user_object);
  }

  ngOnInit() {}

  ngAfterViewInit(){
    
  }

  private log_data = [];
  get_logs(){
    this.deal_services.get_log_api(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      // console.log(data);
      this.log_data = data.Items;
    });
  }

  setDate_dataRange(): void {
    // Set today date using the patchValue function
    let date = new Date();
    this.model = {  beginDate: {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()},
                    endDate: {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()+1}
                  };              
  }

  private deal_id;
  private deal;
  private user_list_data;
  private owner_id;
  private selected_owner_id;
  private deal_fields: any;
  private edit_notes;
  private take_notes: any = [];
  private activities_status_false;
  private activities_status_true;
  private activity_editor;
  private activity_placeholder_name;
  private activityTitle;
  private lost_reason_listing;
  private fa_fa_icon_list;
  private activity_fa_fa_icon;
  private previous_activity;
  private activity_length;
  private activity_date;
  private activity_field_uuid;
  private start_at_time;
  private end_at_time;
  private activity_id;
  private activity_modal_value: boolean = true;
  private active_class;
  private validation_field: boolean;
  private list_text ="Text";
  private select;
  public followers_name_ :any;

  activityM(symbol, fa_fa_icon, activity_uuid){
    this.active_class = symbol;
    this.edit_activityForm.controls['activity_tooltip_name'].setValue(symbol);
    this.activityTitle = symbol;
    this.edit_activityForm.controls.activity_fa_fa_icon.setValue(fa_fa_icon);
    this.activity_fa_fa_icon = fa_fa_icon;
    this.activity_placeholder_name = symbol;
    this.previous_activity = symbol;
    this.activity_field_uuid = activity_uuid;
  }

  keyPress_onActivity_inputField(event,activity) {
    if (event.target.value.length > 0) {
      this.activity_placeholder_name = event.target.value;
     // this.activityTitle = event.target.value;
    }else{
      this.activity_placeholder_name = this.active_class;
    }
  }
 
  focusOut_(){
    $('.ngui-auto-complete').css("display", "none");
    if(this.selected_deal_data=="" || this.selected_deal_data==undefined) {
      this.edit_activityForm.controls.deal_title.setValue('');
      this.validation_field=true;
    }else{
      let dd = this.selected_deal_data['deal_title'] || this.selected_deal_data;
      this.edit_activityForm.controls.deal_title.setValue(dd);
    }
  }

  focusIn_(){
    $('.contact-person .ngui-auto-complete').css("display", "block");
    this.validation_field=false;
  }

  valueChanged_deal_obj(event){
    if (event!=undefined && event['uuid']) {
      this.deal_id = event.uuid;
      this.edit_activityForm.controls.deal_title.setValue(event.deal_title);
    }
  }
 
  observableSourceDealName = (keyword: any): Observable<any[]> => {
    let url = 'v1/deals_search_filter/';
    if (keyword) {
      let persone = this.authUser.search_api(url, keyword, this.base_class_obj.subdomain);
      return persone;
    }
  }
  // participant code section
  // person search
  observableSource = (keyword: any): Observable<any[]> => {
    let url = 'v1/participant_deals/'+this.deal_id
    if (keyword) {
      let person = this.authUser.serach_product_participant_api(url, keyword, this.base_class_obj.subdomain);
      person.subscribe(data=>{
        if (data.length!=0) {
          $('.addPersonButtonRow').css('display', 'block');
        }else{ // return []
          $('.addPersonButtonRow').css('display', 'block');
        }
      });
      return person;
    }else{
      return Observable.of([]);
    }
  }

  dynamic_arr_participant(is_arr){
    if(Array.isArray(is_arr)){
        var d = []
        if (is_arr[0].email) {
          return is_arr[0].email +', '+ is_arr[0].selF 
        }
        if (is_arr[0].phone) {
          return is_arr[0].phone +', '+ is_arr[0].selF 
        }
        for(let i in is_arr){
          d.push(is_arr[i].id)
        }
        return d;
    }else{
      return is_arr;
    }
  }

  focusFunction_person(){
    $('.contact-person .ngui-auto-complete').css("display", "block");
  }

  public selected_participant;
  valueChanged_contact_participant(event){
    let data = {'contact_person_uuid': event.uuid}
    if (event.uuid!=undefined) {
      this.selected_participant = null;
      this.deal_services.add_new_participant(this.deal_id, data, this.base_class_obj.subdomain).subscribe(data=>{
        $('.addPersonButtonRow').css('display', 'none');
        this.get_participants_list();
      });
    }
    // this.dealForm.controls['contact_organization_uuid'].setValue(event.contact_organization_name, {onlySelf: true});
    // this.selected_deal_title = event.contact_organization_name + ' deal';
  }

  delete_participant_in_deal(parti_uuid){
    let value = confirm('Are you sure you want to unlink the main contact person?');
    this.deal_services.delete_participant(parti_uuid, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.get_participants_list();
      }
    });
  }

  add_participants(person_name){
    let data  = {'person_name': person_name.value }
    this.deal_services.add_new_participant(this.deal_id, data, this.base_class_obj.subdomain).subscribe(data=>{
      this.selected_participant = null;
      $('.addPersonButtonRow').css('display', 'none');
      this.get_participants_list();
    });
  }

  private participant_list;
  private participant_value;
  get_participants_list(){
    this.deal_services.get_participants_list_api(this.deal_id,this.base_class_obj.subdomain).subscribe(data=>{
      this.participant_list = data.deals_participents;
      if (this.participant_list.length==0) {
        this.participant_value = true;
      }else{
        this.participant_value = false;
      }
      // console.log('get_participants_list', this.participant_list);
    });
  }
  // participant code section end

  selected_deal_data;
  private all_follower;
  private followers_data = [];
  private data_not_available: boolean;
  private read_only: boolean;
  private stages_arr;
  private stage_count;

  private allCurrency;
  get_currency(){
    this.company_services.get_organization_currency_field_api(true, this.base_class_obj.subdomain).subscribe(data=>{
      this.allCurrency = data['Items'];
    });
  }

  private active_green_pipeline_stage = [];
  private stage_index;
  get_active_green_pipeline_stage(stage_uuid){
    this.pipeline_services.active_green_pipeline_stage(this.pipeline_id, this.deal_id, this.base_class_obj.subdomain)
      .subscribe(data=>{
      // console.log('get_active_green_pipeline_stage', data);
      this.pipeline_name = data.pipeline_data.pname;
      this.active_green_pipeline_stage = [];
      this.active_green_pipeline_stage = data.pipeline_stages_data;
      this.green_stage_index(stage_uuid);
    });
  }

  green_stage_index(stage_uuid){
    this.active_green_pipeline_stage.forEach((x, index)=>{
      let check_stage_name = x.stage_response;
      if (check_stage_name.uuid == stage_uuid) {
        this.stage_name = check_stage_name.sname;
        this.stage_index = index;
      }
    });
  }

  private active_stages;
  active_stage(stage){
    this.active_stages = stage.sname;
    var data = {'uuid': this.deal_id} // move in other stage uuid
    this.green_stage_index(stage.uuid);
    this.authUser.move_pipeline_deal(data, stage.uuid, this.base_class_obj.subdomain).subscribe(data=>{
      // this.get_active_green_pipeline_stage(stage.uuid);
    });
  }

  get_stage_time(datetime_diff){
    if (datetime_diff.day!=0) {
      return datetime_diff.day+' day';
    }else if(datetime_diff.hour!=0){
      return  datetime_diff.hour+' hour';
    }else if(datetime_diff.minute!=0){
      return  datetime_diff.minute+' minute';
    }else if(datetime_diff.second!=0){
      return datetime_diff.second+' second';
    }else{
      return 'This deal has not been in this stage yet';
    }
  }

  private followers: Array<any> = [];
  private list = [];
  private value:any = {};
  private show_select;
  private stage_time;

  follower_data(id,name,email){
    this.router.navigate(["/dashboard/followers",id,this.deal_id,name,email]);
  }

  
  selected_follower(value){
    if (value!=null) {
      let data={'follower_id': value.id, 'follower_name': value.text}
      console.log(data);
      this.authUser.post_add_follower(this.deal_id, this.base_class_obj.subdomain, data).subscribe(data=>{
        this.get_added_follower();
      });
    }
  }
  
  private selected_follower_owner = [];
  get_added_follower(){
   this.all_follower = false;
   this.authUser.get_added_followers(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      this.followers_data = data.Items;
      this.selected_follower_owner = this.followers_data.filter(x=>{
        return this.user_object["email"] == x.follower_id;
      });
      if(data.Count == this.user_length){
         this.all_follower = true;
      }
      this.get_not_added_followers();
    });
  }
  
  messageMapping: { [k: string]: string} = { '=0': 'No Follower', '=1': 'FOLLOWER', 'other': 'FOLLOWERS' };

  get_not_added_followers(){
    this.authUser.get_not_added_followers(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      var arr_object = [];
      data.forEach((x) => {
        arr_object.push({
          id: x.email,
          text: x.nickname
        });
      });
      this.followers = arr_object;
    });
  }

  delete_follower(uuid){
    this.authUser.delete_followers(uuid).subscribe(data=>{
      this.get_added_follower();
      this.get_not_added_followers();
    })
  }
  /* create new activity */ 
  create_new_activity(activityForm, update_value){
    if(this.edit_activityForm.invalid){
        this.validation_field = true;
    }
    var start_at = this.edit_activityForm.value.activity_start_time;
    var end_at = this.edit_activityForm.value.activity_end_time;
    if (start_at >= end_at) {
      alert('End time should be greater than start time');
      return;
    }
    this.edit_activityForm.controls.start_at.setValue(start_at);
    this.edit_activityForm.controls.end_at.setValue(end_at);
    var tooltip_name = this.edit_activityForm.value.activity_tooltip_name;
    var title = this.edit_activityForm.value.activityTitle;
    this.edit_activityForm.controls.activityTitle.setValue(title || tooltip_name);
    this.edit_activityForm.controls.owner_name.setValue(this.owner_name);
    this.edit_activityForm.value.activity_start_time = this._sharedComponent.dateToHMS(this.edit_activityForm.value.activity_start_time);
    this.edit_activityForm.value.activity_end_time = this._sharedComponent.dateToHMS(this.edit_activityForm.value.activity_end_time);
    var data = this.edit_activityForm.value;
    data["local_timezone"] = Intl.DateTimeFormat().resolvedOptions().timeZone;
    console.log(data);
    this.activity_placeholder_name = this.previous_activity;
    if (update_value) {
      if(this.edit_activityForm.valid){
        this.authUser.add_activity(this.deal_id, data, this.base_class_obj.subdomain).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            this.reset_fields(activityForm);
            this.get_created_activity();
            this.get_activity_list();
            this.get_fa_icon_listing();
            this.activity_status = false;
            this.edit_activityForm.controls.activity_complete_status.setValue(this.activity_status);
            this.addActivity_modal.close();
            toastr.success('Added successfully');
          }
        },(err) => console.error(err));
      }
    }
    else{
      if(this.edit_activityForm.valid){
        this.deal_services.edit_activity_new_api(this.activity_id, this.deal_id, data, this.base_class_obj.subdomain).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            this.reset_fields(activityForm);
            this.get_created_activity();
            this.get_activity_list();
            this.get_fa_icon_listing();
            this.activity_status = false;
            this.edit_activityForm.controls.activity_complete_status.setValue(this.activity_status);
            this.addActivity_modal.close();
            toastr.success('update successfully');
          }
        });
      }
    }
  }

  markAllDone(){
    this.edit_activityForm.controls.activity_complete_status.setValue(this.activity_status);
  }

  // get activated fa fa icons listing
  get_fa_icon_listing(){
    this.company_services.get_activity_fa_fa_icon_api(this.base_class_obj.subdomain).subscribe(data=>{
      var fa_fa_icon_list = data.Items.filter((x)=>{ return x.is_activate === true });
      this.fa_icon_list = fa_fa_icon_list;
      this.edit_activityForm.controls['activity_tooltip_name'].setValue(fa_fa_icon_list[0].tooltip_name);
      this.activity_fa_fa_icon = fa_fa_icon_list[0].fa_fa_icon;
      this.activity_placeholder_name = fa_fa_icon_list[0].tooltip_name;
      this.previous_activity = fa_fa_icon_list[0].tooltip_name;
      // this.activityTitle = fa_fa_icon_list[0].tooltip_name;
      this.activity_field_uuid = fa_fa_icon_list[0].uuid;
      this.active_class=this.activity_placeholder_name;
      // this.event_obj.start = startOfDay(new Date());
      // this.event_obj.end = endOfDay(new Date());
    });
  }

  get_userPermission(key){
    // key = 'export_data_from_list_by_user';
    this.company_services.get_userPermissionApi(key, this.base_class_obj.subdomain).subscribe(data=>{
      if(data['userPermission']){
        this.userPermission = data['userPermission'];
      }
    });
  }

  // get created activity listing
  private addActivity_length;
  private activity_data_list;
  get_created_activity(){
    this.authUser.get_all_activity_listing_api(this.deal_uuid_params).subscribe(data=>{
      this.activity_data_list = data;
      this.activities_status_false = data.filter((x)=> { return x.activity_complete_status === false });
      this.addActivity_length = this.activities_status_false.length;
      this.activities_status_true = data.filter((x)=> { return x.activity_complete_status === true });
      this.activity_length = this.activities_status_true.length;
    },(err) => console.error(err) );
  }

  get_one_activity(activity_uuid, tooltip_na){
    this.active_class=tooltip_na;
    this.activity_id = activity_uuid;
    var activity, obj;
    this.events = [ this.event_obj ]; // re-initialize
    for (var i = 0; i < this.activity_data_list.length; i++) {
      this.addEvent_(this.activity_data_list[i]);
    }
    this.activity_modal_value = false;
    var activity = this.activity_data_list.filter((x)=>{ return x.uuid == activity_uuid });
    obj = {};
    obj = activity[0];
    this.activity_status = obj.activity_complete_status;
    this.selected_deal_data = obj.deal_title;
    this.edit_activityForm.controls.deal_title.setValue(obj.deal_title);
    this.owner_name = obj.owner_name;
    this.edit_activityForm.patchValue(obj);
    console.log(this.edit_activityForm.value);
    this.events = this.events.filter((x)=> { return x.meta.id != activity_uuid });
    this.viewDate = new Date(obj.activity_date);
    this.activityTitle = obj.activityTitle + ' ' + obj.activity_start_time + ' To ' + obj.activity_end_time;
    this.event_obj.title = this.activityTitle;
    this.event_obj.start = new Date(obj.start_at);
    this.event_obj.end = new Date(obj.end_at);
  }

  delete_activity(activity_uuid){
    this.deal_services.delete_activity_api(activity_uuid, this.base_class_obj.subdomain).subscribe(data=>{
    });
  }

  // click on check box set activity on true/false
  change_activity_status(activity_uuid, value: boolean){
    // console.log(activity_uuid, value)
    var data = { 'activity_complete_status': value };
    this.deal_services.change_activity_status_api(activity_uuid, data).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.get_created_activity();
      }
    })
  }

  // reset activity form value
  reset_fields(activityForm){
    activityForm.form.controls.activityTitle.reset();
    this.activity_date = this._sharedComponent.dateToYMD(new Date());
    this.viewDate = new Date();
    activityForm.form.controls.activity_editor.reset();
    this.addActivity_modal.close();
    this.get_fa_icon_listing();
    this.event_obj.title = 'A draggable and create new event';
    this.owner_id = this.owner_id_active;
  }
  
  private show_owner_name;
  private deals_data;
  private is_show_field;
  private is_important_field;
  private only_false;
  private sidebar_value;
  private field_key_value;

  schedule_activity(){
    this.activity_status = false;
    this.edit_activityForm.controls.activity_complete_status.setValue(this.activity_status);
    this.activity_modal_value = true;
    this.addActivity_modal.open('lg');
    this.active_class = this.activity_placeholder_name;
    this.event_obj.start = new Date();
    this.event_obj.end = this._sharedComponent.addHours();
    this.event_obj.title = 'A draggable and create new event';
    this.validation_field = false;
    this.selected_deal_data = this.deal.deal_title;
    this.owner_id_active;
    this.owner_id = this.owner_id_active;
  }

  publishDealEvents(){
    this.get_deal_custom_fields();
  }

  publishOrgEvents(){
    this.get_deal_custom_fields();
  }
  publishPersonEvents(){
    this.get_deal_custom_fields();
  }

  get_deal_custom_fields(){
    this.authUser.get_deal(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      var deal_object = data['Item'];
      this.deal_detail_section.list_of_fields(deal_object);
      this.org_section.list_of_fields(deal_object);
      this.person_section.list_of_fields(deal_object);
    }, error=> console.log('error=',error));
  }

  private deal_title;
  private person_name;
  private contact_organization_name;
  private deal_age;
  private product_list = [];
  private product_qty: number = 0;
  // get created deal data
  get_pipeline_list(){
    this.authUser.get_deal(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      this.deal = data['Item'];
      // console.log('deal detail', this.deal);
      var json_object = data['Item'];
      var current_date = new Date();
      var today_date = moment(this._sharedComponent.dateToYMD(current_date));
      var created_date = moment(this._sharedComponent.dateToYMD(new Date(json_object.created_date)));
      this.deal_age = today_date.diff(created_date, "days");
      this.get_active_green_pipeline_stage(json_object.stage_uuid);
      this.product_list = json_object.is_product ? json_object.deal_product_response : [];
      // console.log(this.product_list);
      this.product_qty = 0;
      this.product_list.forEach((x)=>{
        this.product_qty = this.product_qty + x.qty;
      });
      // this.list_of_fields();
      // put value inactivity form
      this.edit_activityForm.controls.deal_title.setValue(this.deal["deal_title"]);
      this.selected_deal_data = this.deal.deal_title;
      this.person_name = this.deal['contact_person_uuid'].person_name;
      this.contact_organization_name = this.deal['contact_organization_uuid'].contact_organization_name;
      // end
      // console.log('DEAL DETAILS data',this.deal);
    }, error=> console.log('error=',error));
  }

  update_deal(updateDealForm){
    let deal = updateDealForm.value;
    if ('uuid' in deal){
      this.authUser.update_deal_api(deal['uuid'],deal, this.base_class_obj.subdomain).subscribe(data=>{
        toastr.success('Update successfully');
        this.input_dealTitle.hide();
        this.get_pipeline_list();
      })
    }
  }

  lost_deal(lostDeal){
    // console.log(lostDeal.value);
    if(lostDeal.value.lost_reason_comment == ''){
      lostDeal.value.lost_reason_comment = ' ';
    }

    this.authUser.won_lost_api(this.deal_id, lostDeal.value, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.get_pipeline_list();
        $('#lostModal').modal('toggle');
        toastr.success('Lost Deal successfully');
        lostDeal.controls.lost_reason.reset();
        lostDeal.controls.lost_reason_comment.reset();
      }
    },(err) => console.error(err) );
  }

  update_reopen(lostDeal){
    var reopenDeal = {deal_reopen:lostDeal};
    this.authUser.won_lost_api(this.deal_id, reopenDeal, this.base_class_obj.subdomain).subscribe(data=>{
       this.get_pipeline_list();
       toastr.success('Reopen Deal successfully');
    },(err) => console.error(err) );
  }

  update_won(wonDeal){
    var won = {'deal_won': wonDeal};
    this.authUser.won_lost_api(this.deal_id, won, this.base_class_obj.subdomain).subscribe(data=>{
       this.get_pipeline_list();
       toastr.success('Won Deal successfully');
    },(err) => console.error(err) );
  }

  update_owner(wonDeal){
    this.authUser.won_lost_api(this.deal_id, wonDeal.value, this.base_class_obj.subdomain).subscribe(data=>{
       this.get_pipeline_list();
       this.input_ownerPopover.hide();
       toastr.success('Owner Update successfully');
    },(err) => console.error(err) );
  }

  private text_field: boolean
  private when_add_field: boolean;
  private add_new = true;
  private edit_value = true;
  private tableName;
  private field_name;
  private temp;
  private message_show = false;
  private add_new_fld;
  private isimpotant_false;
  private icon_down = false;
  private show_setting_icon = true;
  private filter_data;
  private message;

  update_currency(deal_apikey4, deal_apikey5, deal_uuid){
    var data = {'deal_value': deal_apikey4, 'deal_currency': deal_apikey5};
    this.deal_services.update_currency_api(deal_uuid, data, this.base_class_obj.subdomain).subscribe(data=>{
      this.currency_popover.hide();
      if(data['ResponseMetadata'].HTTPStatusCode==200){
        toastr.success('update currency successfully');
      }
    });
  }
  update_close_date(CloseDateForm){
    
  }
  // end

  // get all users listing created by master admin
  private owner_id_active;
  private owner_nickname;
  private user_length;

  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      debugger
      this.user_length = data.users.length;
      this.user_list_data = data.users;
      this.owner_name = data.users[0].nickname;
      this.owner_id_active=data.users[0].email;
      this.get_added_follower();
    },(err) => console.error(err) );
  }

  get_owner_name(event){
    this.owner_name=event.currentTarget.selectedOptions[0].text;
  }

  add_takeNote(takeNote, btn_take_note){
    if(takeNote.value.notes=='' || takeNote.value.notes==null){
      return;
    }else{
      btn_take_note.disabled = true;
    }
    this.authUser.add_takenote(this.deal_id, takeNote.value, this.base_class_obj.subdomain).subscribe(data=>{
      btn_take_note.disabled = false;
      toastr.success('Added successfully');
      takeNote.reset();
      this.get_takenotes();
    },(err) => {
      btn_take_note.disabled = false;
      console.error(err);
    });
  }

  delete_take_note(uuid, indx){
   this.authUser.delete_take_note_api(uuid).subscribe(data=>{
      toastr.success('Deleted successfully');
      this.take_notes.splice(this.take_notes.indexOf(indx), 1);
      // this.get_takenotes();
    },(err) => {
      console.error(err);
    });
  }

  private edit_notes_uuid;
  // click on edit button
  edit_take_note(note){
    this.edit_notes = note.notes;
    this.edit_notes_uuid = note.uuid;
    this.take_note_modal.open();
  }

  update_take_note_data(notes_data){
    var data = {'notes': notes_data};
    if(data.notes=='' || data.notes==null){
      return;
    }else{
      this.take_note_modal.close();
    }
    this.authUser.update_take_note_api(this.edit_notes_uuid, data, this.base_class_obj.subdomain).subscribe(res=>{
      toastr.success('Update successfully');
      this.get_takenotes();
    });
  }

  get_takenotes(){
    this.authUser.get_takenotes(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      this.take_notes = data;
      console.log(data);
    },(err) => console.error(err) );
  }
  
  // get lost reason list in company
  get_lost_reason(){
    this.authUser.get_lost_reason(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['Items'].length<1) {
        this.other_reason_visibility_textbox = false;
      }else{
        this.lost_reason_listing = data['Items'];
        this.reason_select_option = true;
      }
    });
  }

  private reason_select_option: boolean;
  private other_reason_visibility_textbox: boolean;
  private lost_reason: any;
  private other_reason_text;
  reason_on_change(event){
    this.lost_reason = event.target.value;
    if (event.target.value == 'Other...') {
      this.other_reason_text = 'Other'
      this.other_reason_visibility_textbox = true;
    }else{
      this.other_reason_text = '';
      this.other_reason_visibility_textbox = false;
    }
  }

  // calendar funcatinality
  view: string = 'day';
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  private event_obj = {
    start: new Date(), end: this._sharedComponent.addHours(),
    title: 'A draggable and create new event', color: colors.blue, draggable: true, meta: { id: 1 }
  };

  // PUSH ALL SET TIME EVENT IN ARRAY
  addEvent_(obj): void {
    this.events.push({
      title: obj.activityTitle + '<br>' + obj.activity_start_time + ' --> ' + obj.activity_end_time,
      start: new Date(obj.start_at),
      end: new Date(obj.end_at), 
      color: colors.red, 
      draggable: false, 
      meta: { id: obj.uuid }
    });
    this.refresh.next();
  }
   
  get_activity_list(){
    this.activity_events.get_activities_listing_api(this.base_class_obj.subdomain).subscribe(data=>{
      //this.activity_data_list = data;
      this.events = [ this.event_obj ];
      for (let i = 0; i < data.length; ++i) {
        let obj = data[i];
        this.addEvent_(obj);
      }
    });
  }

  events: CalendarEvent[] = [ this.event_obj ];
  // create draggable_container on every date
  draggable_container(start_at, end_at){
    this.event_obj.start = new Date(start_at);
    this.event_obj.end = new Date(end_at);
  }

  change_activity_date(event){ // event return 2017-09-09
    this.viewDate = new Date(event.target.value); // Sun Sep 10 2017 05:30:00 GMT+0530 (IST)
    let start_at = event.target.value+' '+this.start_at_time;
    let end_at = event.target.value+' '+this.end_at_time;
    this.draggable_container(start_at, end_at);
  }
 
  next_pre(){
    this.activity_date = this._sharedComponent.dateToYMD(new Date(this.viewDate)); // return yyyy-mm-dd
    let start_at = this.activity_date+' '+this.start_at_time;
    let end_at = this.activity_date+' '+this.end_at_time;
    this.draggable_container(start_at, end_at);
  }

  change_start_time(event){
    var time = String(event)
    this.start_at_time = this._sharedComponent.dateToHMS_24(new Date(time));
    this.refresh.next();
  }

  change_end_time(event){
    var time = String(event)
    this.end_at_time = this._sharedComponent.dateToHMS_24(new Date(time));
    this.refresh.next();
  }

  eventTimesChanged(event){
    event.event.start = event.newStart;
    event.event.end = event.newEnd;
    this.refresh.next();
  }

  deleteDeal(uuid){
    var confirmBox = confirm("Are you sure you want to delete this deal!");
    if (confirmBox == true) {
        let data = {};
        this.authUser.delete_deal_api(uuid, data,this.base_class_obj.subdomain).subscribe(data=>{
          if(!data['error']){
            toastr.success('Delete Deal successfully');
            this.get_pipeline_list();
          }
        });
        //--- /pipelines/deals/{deal_id}/delete
    }else{
       // console.log("You pressed Cancel!");
    }
  }

  // product code section start
  observableSource_product = (keyword: any): Observable<any[]> => {
    let url = 'v1/products/search/'+this.deal.deal_currency;
    if (keyword) {
      let product = this.authUser.serach_product_participant_api(url, keyword ,this.base_class_obj.subdomain);
      product.subscribe(data=>{
        if (data.length!=0) {
          $('.addProductButtonRow').css('display', 'block');
        }else{ // return []
          $('.addProductButtonRow').css('display', 'block');
        }
      });
      return product;
    }else{
      return Observable.of([])
    }
  }

  private selected_product;
  valueChanged_product(event){
    if (event.uuid) {
      this.selected_product = null;
      $('.addProductButtonRow').css('display', 'none');
      var product = {"is_product": true, "product_uuid": event.uuid}
      this.add_existing_product(product);
    }
  }

  add_as_new_product(product_name){
    let product = {"is_product": false, "product_name": product_name };
    // console.log(product);
    this.product_services.add_product_in_deal_detail(this.deal_id, product, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        $('.addProductButtonRow').css('display', 'none');
        this.selected_product = null;
        this.get_pipeline_list();
      }
    });
  }

  add_existing_product(product){
    this.product_services.add_product_in_deal_detail(this.deal_id, product, this.base_class_obj.subdomain).subscribe(data=>{
      // console.log(data);
      this.get_pipeline_list();
    });
  }
  
  delete_product(product){
    this.product_services.delete_product_deal_detail_api(this.deal_id, product.uuid, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('Product delete successfully');
        this.get_pipeline_list();
      }
    });
  }

  update_product_data(product){
    this.product_services.update_product_deal_detail_api(product.uuid, product, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('product update successfully');
        this.get_pipeline_list();
      }
    });
  }

  check_input_value(product){
    if(product.discount<0){
      product.discount = 0;
    }else if(product.qty<0){
      product.qty = 0;
    }else if(product.price<0){
      product.price = 0;
    }
  }
  // product code section end
}