import { Component, OnInit, Output, EventEmitter, AfterViewInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { ActivityEventClass, DealService, CompanyService } from '../../service/user.service';
import { SharedComponent } from '../../shared/_sharedComponent';
import { ModalPopupComponent } from '../modal-popup/modal-popup.component';
import { Baseurl } from '../../service/baseUrl';

declare var $: any;
declare var toastr: any;

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector:'activity-calendar-picker',
  templateUrl:'activity-calendar-picker.component.html',
  styleUrls:['./activity-calendar-picker.component.css']
})

export class ActivityCalendarPickerComponent{
  @ViewChild('ActivityModalPopup_template')
  activity_modal_temp: ModalPopupComponent;
  base_class_obj = new Baseurl();
  constructor(public activity_events: ActivityEventClass, public deal_services: DealService,
              public company_services: CompanyService,){
    this.get_activity_list();
    toastr.options = {"progressBar": true}
  }

  view_activity: string = 'month';
  viewDate_activity: Date = new Date();
  activeDayIsOpen: boolean = false;
  private _sharedComponent = new SharedComponent();

  events_activity: CalendarEvent[] = [];
  refresh_activity: Subject<any> = new Subject();

  get_activity_list(){
    this.activity_events.get_activities_listing_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.get_activity_events(data);
    });
  }

  get_activity_events(data){
    this.events_activity = [];
    for (var i =0; i < data.length; i++) {
      var obj = data[i];
      this.addEvent(obj);
    }
  }

  addEvent(data): void{
    this.events_activity.push({
      title: data.activityTitle + ', ' + data.activity_start_time + ' TO ' + data.activity_end_time,
      start: new Date(data.start_at),
      end: new Date(data.end_at),
      color: colors.red,
      draggable: true,
      meta: { id: data.uuid }
    });
    this.refresh_activity.next();
  }

  eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.update_activity_events(event);
    this.refresh_activity.next();
  }

  update_activity_events(event){
    let data = {
      'activity_start_time': this._sharedComponent.dateToHMS(event.start),
      'activity_end_time': this._sharedComponent.dateToHMS(event.end),
      'start_at': event.start,
      'end_at': event.end
    };
    // console.log(data);    
    let activity_id = event.meta.id;
    this.deal_services.edit_activity_api(activity_id, data).subscribe(data=>{
      this.get_activity_list();
      if (data['ResponseMetadata'].HTTPStatusCode==200) { 
        toastr.success('event updated');
      }
    })
  }

  dayClicked({ date, events_activity }: { date: Date; events_activity: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate_activity)) {
      if(
        (isSameDay(this.viewDate_activity, date) && this.activeDayIsOpen === true) || events_activity.length === 0){
          this.activeDayIsOpen = false;
      }else{
        this.activeDayIsOpen = true;
        this.viewDate_activity = date;
      }
    }
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.activity_modal_temp.get_one_activity(event.meta.id,event.meta.tooltip_name);
    this.activity_modal_temp.open_();
  }

}