import { Component, Input, OnInit, Output, EventEmitter, AfterViewInit, AfterViewChecked, AfterContentInit, ViewChild } from '@angular/core';
import { startOfDay, endOfDay } from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/observable/of';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { BsModalComponent } from 'ng2-bs3-modal';
import { SharedComponent } from '../../shared/_sharedComponent';
import { UserService, CompanyService, ActivityEventClass, DealService } from '../../service/user.service';
import { CognitoUtil } from "../../service/cognito.service";
import { Baseurl } from '../../service/baseUrl';

declare var toastr: any;
declare var $: any;

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-modal-popup',
  templateUrl: './modal-popup.component.html',
  styleUrls: ['./modal-popup.component.css']
})

export class ModalPopupComponent implements OnInit {
  @ViewChild('addActivity') addActivity_modal: BsModalComponent;
  base_class_obj = new Baseurl();

  private activity_date;
  private fa_icon_list: any;
  private owner_id;
  private selected_owner_id;
  private user_list_data;
  private person_name;
  private contact_organization_name;
  private deal_data;
  private active_class;
  private validation_field :boolean;
  private previous_activity;
  private activityTitle;
  private activity_fa_fa_icon;
  private activity_placeholder_name;
  private activity_field_uuid;
  private activity_status:boolean = false;
  private _sharedComponent = new SharedComponent();
  private activity_form: FormGroup;
  @Input() deal_uuid_modal: any
  @Input() deal_object_modal: any
  @Output() sendActivityEvent = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder, private company_services: CompanyService, 
              private cognito_user: CognitoUtil,
              private authUser: UserService, private activity_events: ActivityEventClass,
              public deal_services: DealService, public http: Http){
    
      // form
      this.activity_form = this.formBuilder.group({
          'activity_tooltip_name': [''],
          'activity_fa_fa_icon': [''],
          'activity_field_uuid': [''],
          'activityTitle': [''],
          'activity_date': [''],
          'activity_start_time': [],
          'activity_end_time': [],
          'activity_editor': [''],
          'owner_id': [''],
          'deal_title': ['', Validators.compose([Validators.required])],
          'person_name': [''],
          'contact_organization_name': [''],
          'start_at': [''],
          'end_at': [''],
          'owner_name':[''],
          'activity_complete_status': [this.activity_status]
      });
      this.activity_date = this._sharedComponent.dateToYMD(new Date());
      this.owner_id = this.cognito_user.getCurrentUser().getUsername();
      this.selected_owner_id = this.cognito_user.getCurrentUser().getUsername();
      this.initialize_methods();
  }

  initialize_methods(){
    this.get_user_list();
    this.get_activity_list();
    this.get_fa_icon_listing();
  }
 
  ngOnInit(){}

  observableSourceDealName = (keyword: any): Observable<any[]> => {
    let url = 'v1/deals_search_filter/';
    if (keyword) {
      let persone = this.authUser.search_api(url, keyword, this.base_class_obj.subdomain);
      return persone;
    }
  }
  
  valueChanged_deal_obj(event){
    if (event!=undefined && event['uuid']) {
      this.deal_uuid_modal = event.uuid;
      this.activity_form.controls.deal_title.setValue(event.deal_title);
    }
  }
  
  focusOut_(){
    $('.ngui-auto-complete').css("display", "none");
    if(this.selected_deal_data=="" || this.selected_deal_data==undefined) {
      this.activity_form.controls.deal_title.setValue('');
      this.validation_field = true;
    }else{
      let dd = this.selected_deal_data['deal_title'] || this.selected_deal_data;
      this.activity_form.controls.deal_title.setValue(dd);
    }
  }
  
  focusIn_(){
    $('.contact-person .ngui-auto-complete').css("display", "block");
    this.validation_field = false;
  }

  private selected_deal_data;
  put_deals_data(){
    // "deal_object_modal" getting to dashboard page click on (schedule new activity).
    if (this.deal_object_modal!=undefined) {
      this.deal_uuid_modal = this.deal_object_modal["uuid"];
      this.activity_form.controls.deal_title.setValue(this.deal_object_modal["deal_title"]);
      this.selected_deal_data = this.deal_object_modal["deal_title"];
      this.person_name = this.deal_object_modal["apikey1"];
      this.contact_organization_name = this.deal_object_modal["apikey2"];
    }
  }

  open_(){
    this.addActivity_modal.open('lg');
    this.activity_status = false;
    this.put_deals_data();
    this.validation_field = false;
    this.active_class = this.activity_placeholder_name;
    this.activity_modal_value = true;
    this.activity_modal_event.start = new Date();
    this.activity_modal_event.end = this._sharedComponent.addHours();
    this.activity_modal_event.title = 'A draggable and create new event',
    this.viewDate = new Date();
    this.events = [ this.activity_modal_event ];
    for (var i = 0; i < this.activity_list.length; i++) {
      this.addEvent(this.activity_list[i]);
    }
  }

  keyPress_onActivity_inputField(event,activity) {
    if (event.target.value.length > 0) {
      this.activity_placeholder_name = event.target.value;
     // this.activityTitle = event.target.value;
    }else{
      this.activity_placeholder_name = this.active_class;
    }
  }

  activityM(symbol, fa_fa_icon, activity_uuid){
    this.active_class = symbol;
    this.activity_form.controls['activity_tooltip_name'].setValue(symbol);
    // this.activityTitle = symbol;
    this.activity_form.controls.activity_fa_fa_icon.setValue(fa_fa_icon);
    this.activity_placeholder_name = symbol;
    this.previous_activity = symbol;
    this.activity_field_uuid = activity_uuid;
  }

  get_fa_icon_listing(){
    this.company_services.get_activity_fa_fa_icon_api(this.base_class_obj.subdomain).subscribe(data=>{
      var fa_fa_icon_list = data.Items.filter((x)=>{ return x.is_activate === true });
      this.fa_icon_list = fa_fa_icon_list;
      this.activity_form.controls['activity_tooltip_name'].setValue(fa_fa_icon_list[0].tooltip_name);
      this.activity_fa_fa_icon = fa_fa_icon_list[0].fa_fa_icon;
      this.activity_placeholder_name = fa_fa_icon_list[0].tooltip_name;
      this.previous_activity = fa_fa_icon_list[0].tooltip_name;
      this.activity_field_uuid = fa_fa_icon_list[0].uuid;
    });
  }

  create_new_activity(activityForm){
    if(this.activity_form.invalid){
        this.validation_field = true;
    }
    this.activity_form.controls.owner_name.setValue(this.owner_name);
    var start_at = this.activity_form.value.activity_start_time;
    var end_at = this.activity_form.value.activity_end_time;
    if (start_at >= end_at) {
      alert('End time should be greater than start time');
      return;
    }
    var tooltip_name = this.activity_form.value.activity_tooltip_name;
    var title = this.activity_form.value.activityTitle;
    this.activity_form.controls.activityTitle.setValue(title || tooltip_name);
    this.activity_form.controls.start_at.setValue(start_at);
    this.activity_form.controls.end_at.setValue(end_at);
    this.activity_form.value.activity_start_time = this._sharedComponent.dateToHMS(this.activity_form.value.activity_start_time);
    this.activity_form.value.activity_end_time = this._sharedComponent.dateToHMS(this.activity_form.value.activity_end_time);
    // console.log(this.activity_form.value);
    var data = this.activity_form.value;
    data["local_timezone"] = Intl.DateTimeFormat().resolvedOptions().timeZone;
    console.log(data)
    // console.log('model',data)
    //this.activity_modal_value, //return true either false when we add/edit activity
    if (this.activity_modal_value) {
      if(this.activity_form.valid){
          this.authUser.add_activity(this.deal_uuid_modal, data, this.base_class_obj.subdomain).subscribe(data=>{
           if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            this.reset_fields(activityForm);
            this.sendActivityEvent.emit();
            this.get_activity_list();
            this.get_fa_icon_listing();
            this.activity_status = false;
            this.activity_form.controls.activity_complete_status.setValue(this.activity_status);
            toastr.success('Added successfully');
          }
        },(err) => console.error(err));
      }
    }
    else{
      // this section work on update activity data
      if(this.activity_form.valid){
        this.deal_services.edit_activity_new_api(this.activity_id, this.deal_uuid_modal, data, this.base_class_obj.subdomain).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200) {
            this.reset_fields(activityForm);
            this.get_activity_list();
            this.sendActivityEvent.emit();
            this.activity_status = false;
            this.activity_form.controls.activity_complete_status.setValue(this.activity_status);
            this.get_fa_icon_listing();   
            toastr.success('update successfully');
          }
        });
      }
    }
  }

  markAllDone(){
    this.activity_form.controls.activity_complete_status.setValue(this.activity_status);
  }
   
  get_owner_name(event){
     this.owner_name=event.currentTarget.selectedOptions[0].text;
  }
  // reset activity form value
  reset_fields(activityForm){
    this.get_fa_icon_listing();
    activityForm.form.controls.activityTitle.reset();
    this.activity_date = this._sharedComponent.dateToYMD(new Date());
    this.viewDate = new Date();
    activityForm.form.controls.activity_editor.reset();
    this.addActivity_modal.close();
    this.owner_id = this.cognito_user.getCurrentUser().getUsername();
  }

  private activity_id;
  get_one_activity(activity_uuid,tooltip_na){
    this.active_class = tooltip_na;
    this.activity_id = activity_uuid;
    var activity, obj;
    this.addActivity_modal.open('lg');
    this.validation_field=false;
    this.events = [ this.activity_modal_event ]; // re-initialize
    for (var i = 0; i < this.activity_list.length; i++) {
      this.addEvent(this.activity_list[i]);
    }
    this.activity_modal_value = false;
    activity = this.activity_list.filter((x)=>{ return x.uuid == activity_uuid });
    obj = {};
    obj = activity[0];
    this.activity_status = obj.activity_complete_status;
    this.selected_deal_data = obj.deal_title;
    this.owner_name = obj.owner_name;
    this.activity_form.controls.deal_title.setValue(obj.deal_title);
    this.activity_form.patchValue(obj);
    let event = this.events.filter((x)=> { return x.meta.id != activity_uuid });
    this.events = event;
    this.viewDate = new Date(obj.activity_date);
    let activity_title = obj.activityTitle + ' ' + obj.activity_start_time + ' To ' + obj.activity_end_time;
    this.activity_modal_event.title = activity_title;
    this.activity_modal_event.start = new Date(obj.start_at);
    this.activity_modal_event.end = new Date(obj.end_at);
  }
        
  // PUSH ALL SET TIME EVENT IN ARRAY
  private addEvent(obj): void {
    this.events.push({
      title: obj.activityTitle + '<br>' + obj.activity_start_time + ' --> ' + obj.activity_end_time,
      start: new Date(obj.start_at),
      end: new Date(obj.end_at),
      color: colors.red,
      draggable: false,
      meta: { id: obj.uuid }
    });
    this.refresh_.next();
  }

  private activity_list = [];
  // get all created activity list and push data in calendar
  get_activity_list(){
    this.activity_events.get_activities_listing_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.activity_list = data;
      // console.log('activity data',data);
      // this.get_activity_events(data); // this method work for calendar(month, week, day)
      this.events = [ this.activity_modal_event ];
      for (var i = 0; i < data.length; i++) {
        this.addEvent(data[i]);
      }
    });
  }

  private activity_modal_value = true;  
  private owner_name;
  private owner_id_active;
  
  // get users listing
  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.user_list_data = data['users'];
      this.owner_name=data['users'][0].nickname;
      this.owner_id_active=data['users'][0].email;
    });
  }

  // calendar functionality
  view_: string = 'day';
  viewDate: Date = new Date();
  private start_at_time;
  private end_at_time;
  private activity_modal_event = {
    start: new Date(),
    end: this._sharedComponent.addHours(),
    title: 'A draggable and create new event',
    color: colors.blue, 
    draggable: true,
    meta: { id: 1 }
  };

  events: CalendarEvent[] = [ this.activity_modal_event ];

  refresh_: Subject<any> = new Subject();

  eventTimesChanged_(event){
    event.event.start = event.newStart;
    event.event.end = event.newEnd;
    this.refresh_.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
  }

  draggable_container(start_at, end_at){
    this.activity_modal_event.start = new Date(start_at);
    this.activity_modal_event.end = new Date(end_at);
  }

  change_activity_date(event){ // event return 2017-09-09
    this.viewDate = new Date(event.target.value); // Sun Sep 10 2017 05:30:00 GMT+0530 (IST)
    let start_at = event.target.value+' '+this.start_at_time;
    let end_at = event.target.value+' '+this.end_at_time;
    this.draggable_container(start_at, end_at);
  }

  next_pre(){
    this.activity_date = this._sharedComponent.dateToYMD(new Date(this.viewDate)); // return yyyy-mm-dd
    let start_at = this.activity_date+' '+this.start_at_time;
    let end_at = this.activity_date+' '+this.end_at_time;
    this.draggable_container(start_at, end_at);
  }

  change_start_time(event){
    var time = String(event)
    this.start_at_time = this._sharedComponent.dateToHMS_24(new Date(time));
    this.refresh_.next();
  }

  change_end_time(event){
    var time = String(event)
    this.end_at_time = this._sharedComponent.dateToHMS_24(new Date(time));
    this.refresh_.next();
  }
}