import { Component, ElementRef, Input, OnInit, Output, EventEmitter, AfterViewInit, AfterViewChecked, AfterContentInit, ViewChild } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';

import { UserService, CompanyService, ActivityEventClass, DealService, } from '../service/user.service';
import { SharedComponent } from '../shared/_sharedComponent';
import { ModalPopupComponent } from './modal-popup/modal-popup.component';
import { Baseurl } from '../service/baseUrl';

declare var $: any;
declare var toastr: any;

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css'],
})

export class ActivityComponent implements OnInit {
  private user_list_data;  
  private selected_owner_id;
  activity_form: FormGroup;
  private _sharedComponent = new SharedComponent();
  tooltip_name:any;

  @ViewChild('my_modal_popup') my_modal_popup: ModalPopupComponent;
  
  base_class_obj = new Baseurl();
  constructor(private company_services: CompanyService, private formBuilder: FormBuilder,
              public deal_services: DealService, private authUser: UserService, 
              private activity_events: ActivityEventClass, private el: ElementRef) {
        toastr.options = {"progressBar": true}
        this.initialize_methods();
  }
  
  initialize_methods(){
    this.get_activity_list();
    this.get_fa_icon_listing();
  }

  ngOnInit(){}
  ngAfterViewInit(){
    this.el.nativeElement
    $(".btnactive button").click(function () {
      $(".btnactive button").removeClass("active");
      $(this).addClass("active");
    });
    $(".btn-active button").click(function () {
      $(".btn-active button").removeClass("active");
      $(this).addClass("active");
    });
  }

  private fa_fa_icon_list;
  get_fa_icon_listing(){
    this.company_services.get_activity_fa_fa_icon_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.fa_fa_icon_list = data.Items.filter((x)=>{ return x.is_activate === true });
    });
  }

  // work on click on 'Add activity' button
  open_activity_modal(){
    this.my_modal_popup.open_();
  }

  private activity_list = [];
  // get all created activity list and push data in calendar
  get_activity_list(){
    this.activity_events.get_activities_listing_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.activity_list = data;
      // console.log('activity data',data);
      this.get_activity_events(data); // this method work for calendar(month, week, day)
    });
  }
 
  showEvents() {
    this.get_activity_list();
  }

  // ---------- <!-- activity calendar(month, week, day) --> -------------
  view_activity: string = 'month';
  viewDate_activity: Date = new Date();
  activeDayIsOpen: boolean = false;

  events_activity: CalendarEvent[] = [];
  refresh_activity: Subject<any> = new Subject();

  get_activity_events(data){
    this.events_activity = [];
    for (var i =0; i < data.length; i++) {
      var obj = data[i];
      this.addEvent_activity(obj);
    }
  }

  addEvent_activity(data): void{
    this.events_activity.push({
      title: data.activityTitle + ', ' + data.activity_start_time + ' TO ' + data.activity_end_time,
      start: new Date(data.start_at),
      end: new Date(data.end_at),
      color: colors.red,
      draggable: true,
      meta: { 'id': data.uuid, 'tooltip_name': data.activity_tooltip_name}
    });
    this.refresh_activity.next();
  }
  
  private for_icon;
  private for_active = 'active';
  private active_class;

  filter_activities(tooltip_n){
    this.for_active='';
    this.active_class ='active';
    this.for_icon = tooltip_n;
    this.get_activity_events(this.activity_list);
    var filter_icon = this.events_activity.filter((i)=>{ return i.meta.tooltip_name == tooltip_n });
    this.events_activity = filter_icon;
  }

  for_all(){
    this.get_activity_events(this.activity_list);
    this.for_active = 'active';
    this.active_class = '';
  }

  eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.update_activity_events(event);
    this.refresh_activity.next();
  }
  
  update_activity_events(event){
    let data = {
      'activity_start_time': this._sharedComponent.dateToHMS(event.start),
      'activity_end_time': this._sharedComponent.dateToHMS(event.end),
      'start_at': event.start,
      'end_at': event.end,
      'activity_date': this._sharedComponent.dateToYMD(new Date(event.end)),
    };
   
    let activity_id = event.meta.id;
    this.deal_services.edit_activity_api(activity_id, data).subscribe(data=>{
      this.get_activity_list();
      this.my_modal_popup.get_activity_list(); // call modalComponent method
      if (data['ResponseMetadata'].HTTPStatusCode==200) { 
        toastr.success('event updated');
      }
    })
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate_activity)) {
      if(
        (isSameDay(this.viewDate_activity, date) && this.activeDayIsOpen === true) || events.length === 0){
          this.activeDayIsOpen = false;
      }else{
        this.activeDayIsOpen = true;
        this.viewDate_activity = date;
      }
    }
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.my_modal_popup.get_one_activity(event.meta.id,event.meta.tooltip_name);
    // this.activity_id = event.meta.id;
    // this.activity_modal_temp.open_();
  }
}