import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserLoginService } from '../../service/user-login.service';
import { ValidationService } from '../../shared/validationServices';

declare var jQuery: any;
declare var toastr: any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls:['./change-password.component.css']
})

export class ChangePasswordComponent implements OnInit {
  myform: FormGroup;
  newPassword: string;
  oldPassword: string;
  errorMessage: string;
  confirm_password: string;

  constructor(public userLoginService: UserLoginService, public formBuilder: FormBuilder,){
    toastr.options = { "progressBar": true}
    this.createForm();
  }
  createForm() {
    this.myform = this.formBuilder.group({
      oldPassword: ['', Validators.compose([Validators.required])],
      newPassword: ['', Validators.compose([Validators.required])],
      confirm_password: ['',  Validators.compose([Validators.required])],
    }, {'validator': ValidationService.isMatching});
  }

  ngOnInit(){}

  change_password(){
    // console.log(this.myform.valid);
    if (this.myform.valid) {
      this.userLoginService.changePassword(this.oldPassword, this.newPassword, this);
    }
  }

  cognitoCallback(message: string, result: string){
    message!=null ? toastr.success(message) : toastr.success('Password changed successfully');
    if (message==null) {
      this.createForm();
    }
  }
}