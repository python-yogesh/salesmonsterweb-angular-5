import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { CustomFieldsService, UserService, CompanyService, DealService, ActivityEventClass, Pipeline } from '../service/user.service';
import { SharedComponent } from '../shared/_sharedComponent';
import { Baseurl } from '../service/baseUrl';

import io from 'socket.io-client';

declare var toastr: any;

@Component({
  selector: 'app-organization-section',
  templateUrl: './organization-section.component.html',
  styleUrls: ['./organization-section.component.css']
})

export class OrganizationSectionComponent implements OnInit {
	base_class_obj = new Baseurl();
  
	private _sharedComponent = new SharedComponent();
	@Output() sendOrgEvent = new EventEmitter<any>();
  @Output() sendMapPointer = new EventEmitter<any>();
	private arr_time;
  constructor(public authUser: UserService, public custom_field_service: CustomFieldsService,
  	public deal_services: DealService,) { 
  	this.view_list();
  	this.arr_time = this._sharedComponent.timeDuration;
  }

  ngOnInit() {}
  
  private list=[];
  view_list(){
  	var listing = this._sharedComponent.Listing;
     listing.forEach((list_) => {
      this.list.push({
        icon_:list_.icon,
        id: list_.Text,
        text: `${list_.icon}&nbsp&nbsp<span class="label-component">${list_.Text}</span></br>${list_.Description}`,
      });
    });
  }

  private org_edit_uuid = '';
  update_org_field(tableName, org_data){
    // console.log(org_data);
    var data = { 'label': org_data.label, 'is_show': org_data.is_show, 'is_important': org_data.is_important}
    this.custom_field_service.update_details(tableName, org_data, org_data.uuid, this.base_class_obj.subdomain).subscribe(data=>{
      toastr.success('update successfully');
      this.org_edit_uuid = '';
    });
  }

  delete_custom_field(table_name, field_uuid){
    var value = confirm('Are you sure you wish to remove this?');
    if (value) {
      this.custom_field_service.delete_(table_name, field_uuid).subscribe(data=>{
        // console.log(data);
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          this.sendOrgEvent.emit();
          toastr.success('deleted');
        }
      });
    }
  }

  private errorMsg;
  private error_message_txt;
  create_custom_field_for_org(tableName, org_data){
    // console.log(org_data);
    this.custom_field_service.create_field_api(tableName, org_data, this.base_class_obj.subdomain).subscribe(data=>{
      if(data['response'] == "field name already exist"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else if(data['response'] == "it is reserved word"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else{
        this.errorMsg = false;
        this.sendOrgEvent.emit();
        toastr.success('created');
      }
    });
  }

  private org_field_api_key = '';
  private org_add_field_dev;

  cancel_edit(){ this.org_edit_uuid = ''; }
  show_org_fields(org_field){
    this.org_field_api_key = org_field.field_api_key;
  }

  isBigEnough(element) {
    return (element.is_deleted==true); 
  }

  private org_rename_css;
  private org_input_filed_css = 'none';
  org_rename_btn(rename_btn, input_field){
    this.org_input_filed_css = input_field;
    this.org_rename_css = rename_btn;
  }
  // update org name
  save_contact_org(contact_org_name, contact_org_uuid){
    var data = {"fieldName":"contact_organization_name", "fieldValue": contact_org_name.value};
    this.partial_method_for_contact_org_person('contacts_organizations', contact_org_uuid, data);
  }

  // update org name
  update_contact_org_address(contact_org_uuid){
    var data = {"fieldName":"address", "fieldValue": JSON.stringify(this.map_address_data)};
    this.partial_method_for_contact_org_person('contacts_organizations', contact_org_uuid, data);
  }

  partial_method_for_contact_org_person(table_name, uuid, data){
    this.deal_services.update_contact_orgnization(table_name, uuid, data)
      .subscribe(data=>{
        this.sendOrgEvent.emit();
        if (data['ResponseMetadata'].HTTPStatusCode==200) {
          this.org_rename_btn('block', 'none');
          toastr.success('update successfully');
        }
    });
  }

  dynamic_address(ss){
    try{
      var data = JSON.parse(ss);
      if (data['formatted_address']) {
        this.map_address_data = data;
        return data['formatted_address'];
      }
      return ss;
    } catch (e) { return ss; }
  }
  
  private org_address;

  public userSettings2: any = {
    showRecentSearch: false, showSearchButton: false, inputString: '' , "showCurrentLocation":false,
  };

  private map_address_data = { 'location':'', 'formatted_address':'' };
  autoCompleteCallback(e){
    this.map_address_data.formatted_address = e.data.formatted_address;
    this.map_address_data.location = e.data.geometry.location;
  }
  
  put_map_pointer(){
    
  }

  private contacts_organizations_fields_obj = []; // form variable
  private contacts_organizations_fields;
  private deal;
  list_of_fields(deal_obj){
  	this.deal = deal_obj;
    this.org_address = this.dynamic_address(this.deal.contact_organization_uuid.address);
    this.userSettings2.inputString  = this.org_address;
    this.org_field_api_key = '';
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('custom fields', data)
      this.org_add_field_dev = false;
      // org custom fields data
      this.contacts_organizations_fields = data.contacts_organizations_fields.filter(this.isBigEnough);
      var contacts_organizations_fields = data.contacts_organizations_fields.filter(this.isBigEnough);
      var contact_org_arr = [];
      for (var j in contacts_organizations_fields){
        var value = deal_obj.contact_organization_uuid[contacts_organizations_fields[j].field_api_key] != undefined ? deal_obj.contact_organization_uuid[contacts_organizations_fields[j].field_api_key] : '';
        let obj = {
          'field_api_key': contacts_organizations_fields[j].field_api_key,
          'is_type': contacts_organizations_fields[j].is_type,
          'label': contacts_organizations_fields[j].label,
          'multiple_value': contacts_organizations_fields[j].multiple_value,
          'value': value,
          'org_uuid': deal_obj.contact_organization_uuid.uuid
        }
        contact_org_arr.push(obj);
      }
      // console.log('contact_org_arr', contact_org_arr)
      this.contacts_organizations_fields_obj = contact_org_arr;
    });
  }

  org_data = {  type: '' , is_type: '', label: '', multiple_value: [], is_textbox: false, 
                is_show: false, is_important: false }

  field_type_for_org(event){
    var seleted_text =  event.id;
    this.org_data.is_type = event.id;
    var is_type = this._sharedComponent.field_type(event);
    if (is_type == 'multiple_option' || is_type == 'single_option') {
       this.org_data.is_textbox = true;
    }else{
      this.org_data.is_textbox = false;
    }
    this.org_data.type = is_type;
  }

  edit_btn_org(org_uuid, org){
    org.is_important = org.is_important;
    org.is_show = org.is_show;
  }

  // update multiple person and organisatoin fields.
  update_all_custom_field_value(table_name, org_form_data_value, uuid){
    let data = org_form_data_value;
    this.deal_services.update_all_single_field_value_org_api(table_name, uuid, this.base_class_obj.subdomain, data).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode==200) {
        toastr.success('update successfully');
        this.sendOrgEvent.emit();
      }
    });
  }

  // update single person and org fields.
  update_single_org_custom_field_value(table_name, org_form_data_value, uuid){
    // uuid => person_uuid, org_uuid, deal_uuid
    let data = org_form_data_value;
    this.deal_services.update_single_field_value_org(table_name, uuid, this.base_class_obj.subdomain, data)
      .subscribe(data=>{
        this.sendOrgEvent.emit();
        toastr.success('success');
    });
  }

}