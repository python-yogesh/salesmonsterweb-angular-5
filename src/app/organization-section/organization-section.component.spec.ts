import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationSectionComponent } from './organization-section.component';

describe('OrganizationSectionComponent', () => {
  let component: OrganizationSectionComponent;
  let fixture: ComponentFixture<OrganizationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
