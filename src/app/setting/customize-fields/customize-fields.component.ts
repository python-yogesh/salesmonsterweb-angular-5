import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BsModalComponent } from 'ng2-bs3-modal';
import { CustomFieldsService , UserService} from '../../service/user.service';
import { Baseurl } from '../../service/baseUrl';
import {JqueryComponent} from '../../shared/jqueryComponent';

declare var toastr: any;
declare var $: any;
@Component({
  selector: 'app-customize-fields',
  templateUrl: './customize-fields.component.html',
  styleUrls: ['./customize-fields.component.css']
})

export class CustomizeFieldsComponent implements OnInit {
  custom_field: any;
  contacts_persons_fields: any;
  contacts_organizations_fields: any;
  products_fields: any;
  data = { type: '' , is_type: '', label: '', multiple_value: [], is_textbox: false }
  uuid;
  updateFieldName: string;
  arrayOfLines: any = '';
  textarea_list_arr: boolean;
  textarea: boolean;
  errorMsg: boolean = false;
  error_message_txt: string;
  multiple_list_array;
  tableName = "deals_fields";
  private loader_image:boolean;
  base_class_obj = new Baseurl();
  
  constructor(public u_service: UserService, public custom_field_service: CustomFieldsService,) { 
  	this.list_of_fields();
    toastr.options = { "progressBar": true }

  }
  @ViewChild('parentModal')
  parentModal_modal: BsModalComponent;
  
  @ViewChild('childModal')
  childModal_modal: BsModalComponent;
  
  @ViewChild('updateModal')
  updateModal_modal: BsModalComponent;

  ngOnInit(){}

  list_of_fields(){
    this.loader_image=true;
  	this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
   
  		// console.log("----------------------------------------------------------");
      // console.log(data);
      this.custom_field = data['deal_fields'];
      this.contacts_persons_fields = data['contacts_persons_fields'];
      this.contacts_organizations_fields = data['contacts_organizations_fields'];
      this.products_fields= data['products_fields'];
      if(data!=''){
          this.loader_image=false;
        }
  	});
  }
  
  // update 'Important' and '	Show in Add new Dailog' fields value true/false
  update(value, uuid){
  	// console.log(value, uuid);
  	this.custom_field_service.update_details(this.tableName,value, uuid, this.base_class_obj.subdomain).subscribe(data=>{
  		// console.log(data);
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('update successfully');
        this.list_of_fields();
      }
  	});
  }

  update_popup(fields_data){
    this.multiple_list_array = fields_data.multiple_value;
    this.textarea = fields_data.is_textbox;
    this.updateFieldName = fields_data.label;
    this.uuid = fields_data.uuid;
    this.updateModal_modal.open();
    // console.log(fields_data);
  }
  // update in db
  update_field(){
    if (this.textarea) {
      var arr_list = [];
      var data = []
      arr_list.push(this.arrayOfLines);
      var result = arr_list[0].split("\n");
      // We iterate the array in the code
      for(let r of result) {
        data.push(r.trim());
      }
      var filter_textbox_data = data.filter(function(i){ return i != ''; });
      var final_data = this.multiple_list_array.concat(filter_textbox_data)
    }

    // console.log('final_data', final_data);
    var d = { label: this.updateFieldName, multiple_value: final_data }
    // console.log(d);
    this.custom_field_service.update_details(this.tableName,d, this.uuid, this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('update field response', data)
      if(data['response'] == "it is reserved word"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else{
        this.updateFieldName = '';  
        this.list_of_fields();
        toastr.success('update successfully');
        this.arrayOfLines = '';
        this.textarea = false;
        this.errorMsg = false;
        this.updateModal_modal.close();
      }
    });
  }

  delete(uuid){
    var r = confirm('Are you sure you wish to remove this?');
    // console.log(r);
    if (r == true) {
      this.custom_field_service.delete_(this.tableName, uuid).subscribe(data=>{
        // console.log(data);
        this.list_of_fields();
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('deleted');
        }
      });
    }
  }

  fieldName: any = '';
  is_value: boolean;
  set_data(is_type, type, value){
    this.is_value = value;
    this.fieldName = '';
    this.childModal_modal.open();
    this.data.type = type;
    this.data.is_type = is_type;
    // console.log(this.data);
  }
  get_table_name(tableName){
   this.tableName = tableName;
  }
  // save field data
  create_field(name){
    // console.log("show tablename",this.tableName);
    if (this.arrayOfLines == '' && this.is_value ) {
        this.errorMsg = true;
        this.error_message_txt = 'At least one option needs to be entered in save this field';
        return
    }
    if (this.is_value) {
      var arr_list = [];
      var data = []
      arr_list.push(this.arrayOfLines);
      var result = arr_list[0].split("\n");
      // We iterate the array in the code
      for(let r of result) {
        data.push(r.trim());
      }
      var r = data.filter(function(i){ return i != ''; })
      // console.log('r:', r)
      this.data.multiple_value = r;
      this.data.is_textbox = true;
    }
    this.data.label = name;
    // console.log("show data",this.data);
    this.custom_field_service.create_field_api(this.tableName,this.data, this.base_class_obj.subdomain).subscribe(data=>{
      var res = data;
      if(res['response'] == "field name already exist"){
        this.errorMsg = true;
        this.error_message_txt = res['response'];
      }else if(res['response'] == "it is reserved word"){
        this.errorMsg = true;
        this.error_message_txt = res['response'];
      }else{
        this.errorMsg = false;
        this.list_of_fields();
        this.childModal_modal.close();
        this.parentModal_modal.close();
        this.arrayOfLines = ''
        toastr.success('created');
      }
    });
  }
  
  dismissed(){
    this.errorMsg = false;
    this.textarea = false;
  }

  text_box(){
    this.textarea = true;
  }
  
  soft_delete_value(index: number): any{
    this.multiple_list_array.splice(index, 1);
  }
  saveEditable(value, i) { this.multiple_list_array[i] = value; }
}