import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizeFieldsComponent } from './customize-fields.component';

describe('CustomizeFieldsComponent', () => {
  let component: CustomizeFieldsComponent;
  let fixture: ComponentFixture<CustomizeFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizeFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizeFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
