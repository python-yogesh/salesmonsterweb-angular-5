import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { BsModalComponent } from 'ng2-bs3-modal';
import { UserService, CompanyService, StripeServices } from '../service/user.service';
import { Callback, CognitoUtil } from "../service/cognito.service";
import { UserLoginService } from "../service/user-login.service";

import { Baseurl } from '../service/baseUrl';

declare var toastr: any;
declare var $: any;

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})

export class SettingComponent implements OnInit, Callback {
  @ViewChild('myModal')
  @ViewChild('fileInput') el:ElementRef;
  base_class_obj = new Baseurl();
  modal: BsModalComponent;
  private company_form: FormGroup;
  private user_object = {};
  private cognito_user_name: string;
  private submit:boolean = false;  
  private selectedCurrency = '';
  private selectedLocale = 'aa_ER';
  private selectedLanguage = 'Deutsch';

  private allCurrency;

  private image_data:any = { 'image': "" }
  private img_url = 'assets/images/blank_profile_male.jpg';
  private remove_pic_btn: boolean = true;
  private loader_image: boolean;

  constructor(public compnayService: CompanyService,public router: Router, public userService: UserLoginService, public cognito_user: CognitoUtil,
              public authUser: UserService, public stripe_services: StripeServices,
              public formBuilder: FormBuilder){
    
    toastr.options = { "progressBar": true }
    // this.authUser.getJSON().subscribe(data=>{
    // });
    // this.authUser.getJSON().subscribe(data =>{
    //   console.log(data);
    // }, error => console.log(error));
    this.get_userProfile();
    this.get_cognito_user_profile();
    this.get_currency();
    this.get_selectCurrency();
    this.get_company_list();
    this.initialize_company_form();
  }

  ngOnInit(){}
  callback(){}

  // call back for getUserParameters
  callbackWithParam(result: any){
    for (var i = 0; i < result.length; i++) {
      this.user_object[result[i].Name] = result[i].Value;
    }
    this.cognito_user_name = this.user_object["nickname"];
  }

  get_cognito_user_profile(){
    this.userService.getUserParameters(this); // get user profile data via cognito
  }

  onChange(evt){
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload =this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  private base64textString: String="";
  _handleReaderLoaded(readerEvt) {
      var binaryString = readerEvt.target.result;
      this.base64textString = btoa(binaryString);
      this.image_data.image = this.base64textString;
  }

  get_userProfile(){
    this.loader_image = true;
    this.authUser.get_user_profile_api(this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('user profile data', data);
      if (data.userprofile!='') {
        this.loader_image = false;
        var profile_data = data.userprofile;
        if (profile_data.image_url == "removed"){
          this.img_url = 'assets/images/blank_profile_male.jpg';
          this.remove_pic_btn = true;
        }else if (profile_data['image_url']){ 
          this.remove_pic_btn = false;
          this.img_url = profile_data.image_url || 'assets/images/blank_profile_male.jpg';
        }else{
          this.remove_pic_btn = true;
        }
        this.selectedLocale = profile_data.locale || this.selectedLocale;
        this.selectedLanguage = profile_data.languages || this.selectedLanguage;
        this.selectedCurrency = profile_data.default_currency || this.selectedCurrency;
      }
    });
  }

  // Method for get all currency of a organization
  get_currency(){
    this.compnayService.get_organization_currency_field_api(true, this.base_class_obj.subdomain).subscribe(data=>{
        //allCurrency variable
        this.allCurrency = data['Items'];
    });
  }

  initialize_company_form(){
    this.company_form = this.formBuilder.group({
      'company_name': ['', Validators.compose([Validators.required])],
    });
  }

  // create company.
  save_company_data(){
    this.agent_ = 'agent';
    this.submit = true;
    if (this.company_form.valid) {
      $('#company_model').modal('toggle');
      this.authUser.company_setting(this.agent_, this.company_form.value).subscribe(data=>{
        if (data.error) {
          toastr.error(data.error);
          return;
        }
        if (data.organization_response['ResponseMetadata'].HTTPStatusCode == 200) {
          this.initialize_company_form();
          this.get_company_list();
          this.create_stripe_trail(data.organization_json.Item);
          toastr.success('Company Create Successfully');
        }
      });
    }
  }

  create_stripe_trail(data){
    var obj = { 'username': this.cognito_user.getCurrentUser().getUsername(), 
      "organization_uuid" : data.uuid, "organization_name" : data.company_name
    }
    this.stripe_services.create_trail_stripe_api(obj).subscribe(data=>{
      if (data.status_code==200){
        // alert('success ');
        // localStorage.setItem('customer_id', data.customer)
      }
    },(err)=>{
      alert(err);
    });
  }

  //get company list
  private company_list;
  private agent_;
  
  get_company_list(){
    this.authUser.get_company_data().subscribe(data=>{
       this.company_list = data;
    });
  }

  // Method for get selected currency of a organization
  get_selectCurrency(){
    this.authUser.get_user_profile_api(this.base_class_obj.subdomain).subscribe(data=>{
        //allCurrency variable
        if ("default_currency" in data['userprofile']){
            this.selectedCurrency = data['userprofile']['default_currency'];    
        }
        else 
        {
          this.selectedCurrency = 'USD';
        }
    });
  }


  
  update_user(username, email, formData){
    this.loader_image = true;
    var result={};
    for(var key in formData.value) result[key] = formData.value[key];
    if (this.image_data.image!='') {
      for(var key in this.image_data) result[key] = this.image_data[key];
    }
    // console.log('result', result);
    this.authUser.update_profile(result, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.get_userProfile();
        toastr.success('update profile successfully');
      }
    });
    // update cognito user profile data
    this.userService.updateUserParameter(username.value, email.value, this);
  }

  update_currency(formData){
    this.loader_image = true;
    var result={};
    for(var key in formData.value) result[key] = formData.value[key];
    // console.log('result', result);
    this.authUser.update_currency_api(result, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        toastr.success('Currency Update Successfully');
        this.get_userProfile();
      }
    });
    
  }

  removePic(){
    this.authUser.delete_profile_image().subscribe(data=>{
      toastr.success('Profile Deleted');
      this.get_userProfile();
    });
  }
  
  // call back for update user data, method updateUserParameter()
  cognitoCallback(message: string, result: any){
    message!=null ? toastr.success(message) : '';
    this.get_cognito_user_profile();
    // this.router.navigate(['/dashboard']);
  }

  ngAfterViewInit(){
    $(document).ready(function() {
      $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
      });
    });
  }
  
  close() { this.modal.close(); }
  open() { this.modal.open('sm'); }

  locale =[ 
            { value: 'aa_ER', name: 'Afar (Eritrea)' },
            { value: 'aa_ET', name: 'Afar (Ethiopia)' },
            { value: 'sq_AL', name: 'Albanian (Albania)' },
            { value: 'am_ET', name: 'Amharic (Ethiopia)' },
            { value: 'ar_DZ', name: 'Arabic (Algeria)' },
            { value: 'ar_BH', name: 'Arabic (Bahrain)' },
            { value: 'ar_EG', name: 'Arabic (Egypt)' },
            { value: 'ar_IN', name: 'Arabic (India)' },
            { value: 'ar_IQ', name: 'Arabic (Iraq)' },
            { value: 'ar_JO', name: 'Arabic (Jordan)' },
            { value: 'ar_KW', name: 'Arabic (Kuwait)' },
            { value: 'ar_LB', name: 'Arabic (Lebanon)' },
            { value: 'ar_LY', name: 'Arabic (Libyan Arab Jamahiriya)' },
            { value: 'ar_MA', name: 'Arabic (Morocco)' },
            { value: 'ar_OM', name: 'Arabic (Oman)' },
            { value: 'ar_QA', name: 'Arabic (Qatar)' },
            { value: 'ar_SA', name: 'Arabic (Saudi Arabia)' },
            { value: 'ar_SD', name: 'Arabic (Sudan)' },
            { value: 'ar_SY', name: 'Arabic (Syrian Arab Republic)' },
            { value: 'ar_TN', name: 'Arabic (Tunisia)' },
            { value: 'ar_AE', name: 'Arabic (United Arab Emirates)' },
            { value: 'ar_YE', name: 'Arabic (Yemen)' },
            { value: 'an_ES', name: 'Aragonese (Spain)' },
            { value: 'hy_AM', name: 'Armenian (Armenia)' },
            { value: 'as_IN', name: 'Assamese (India)' },
            { value: 'eu_FR', name: 'Basque (France)' },
            { value: 'eu_ES', name: 'Basque (Spain)' },
            { value: 'be_BY', name: 'Belarusian (Belarus)' },
            { value: 'bn_BD', name: 'Bengali (Bangladesh)' },
            { value: 'bn_IN', name: 'Bengali (India)' },
            { value: 'bs_BA', name: 'Bosnian (Bosnia and Herzegovina)' },
            { value: 'br_FR', name: 'Breton (France)' },
            { value: 'bg_BG', name: 'Bulgarian (Bulgaria)' },
            { value: 'ca_AD', name: 'Catalan; Valencian (Andorra)' },
            { value: 'ca_FR', name: 'Catalan; Valencian (France)' },
            { value: 'ca_IT', name: 'Catalan; Valencian (Italy)' },
            { value: 'ca_ES', name: 'Catalan; Valencian (Spain)' },
            { value: 'zh_CN', name: 'Chinese (China)' },
            { value: 'zh_HK', name: 'Chinese (Hong Kong)' },
            { value: 'zh_SG', name: 'Chinese (Singapore)' },
            { value: 'zh_TW', name: 'Chinese (Taiwan)' },
            { value: 'kw_GB', name: 'Cornish (United Kingdom)' },
            { value: 'hr_HR', name: 'Croatian (Croatia)' },
            { value: 'cs_CZ', name: 'Czech (Czech Republic)' },
            { value: 'da_DK', name: 'Danish (Denmark)' },
            { value: 'nl_BE', name: 'Dutch (Belgium)' },
            { value: 'nl_NL', name: 'Dutch (Netherlands)' },
            { value: 'dz_BT', name: 'Dzongkha (Bhutan)' },
            { value: 'en_AU', name: 'English (Australia)' },
            { value: 'en_BW', name: 'English (Botswana)' },
            { value: 'en_CA', name: 'English (Canada)' },
            { value: 'en_DK', name: 'English (Denmark)' },
            { value: 'en_HK', name: 'English (Hong Kong)' },
            { value: 'en_IN', name: 'English (India)' },
            { value: 'en_IE', name: 'English (Ireland)' },
            { value: 'en_NZ', name: 'English (New Zealand)' },
            { value: 'en_NG', name: 'English (Nigeria)' },
            { value: 'en_PH', name: 'English (Philippines)' },
            { value: 'en_SG', name: 'English (Singapore)' },
            { value: 'en_ZA', name: 'English (South Africa)' },
            { value: 'en_GB', name: 'English (United Kingdom)' },
            { value: 'en_US', name: 'English (United States)' },
            { value: 'en_ZW', name: 'English (Zimbabwe)' },
            { value: 'et_EE', name: 'Estonian (Estonia)' },
            { value: 'fo_FO', name: 'Faroese (Faroe Islands)' },
            { value: 'fi_FI', name: 'Finnish (Finland)' },
            { value: 'fr_BE', name: 'French (Belgium)' },
            { value: 'fr_CA', name: 'French (Canada)' },
            { value: 'fr_FR', name: 'French (France)' },
            { value: 'fr_LU', name: 'French (Luxembourg)' },
            { value: 'fr_CH', name: 'French (Switzerland)' },
            { value: 'gl_ES', name: 'Galician (Spain)' },
            { value: 'ka_GE', name: 'Georgian (Georgia)' },
            { value: 'de_AT', name: 'German (Austria)' },
            { value: 'de_BE', name: 'German (Belgium)' },
            { value: 'de_DE', name: 'German (Germany)' },
            { value: 'de_LI', name: 'German (Liechtenstein)' },
            { value: 'de_LU', name: 'German (Luxembourg)' },
            { value: 'de_CH', name: 'German (Switzerland)' },
            { value: 'el_CY', name: 'Greek, Modern (Cyprus)' },
            { value: 'el_GR', name: 'Greek, Modern (Greece)' },
            { value: 'gu_IN', name: 'Gujarati (India)' },
            { value: 'ha_NG', name: 'Hausa (Nigeria)' },
            { value: 'he_IL', name: 'Hebrew (modern) (Israel)' },
            { value: 'hi_IN', name: 'Hindi (India)' },
            { value: 'hu_HU', name: 'Hungarian (Hungary)' },
            { value: 'is_IS', name: 'Icelandic (Iceland)' },
            { value: 'ig_NG', name: 'Igbo (Nigeria)' },
            { value: 'id_ID', name: 'Indonesian (Indonesia)' },
            { value: 'iu_CA', name: 'Inuktitut (Canada)' },
            { value: 'ik_CA', name: 'Inupiaq (Canada)' },
            { value: 'ga_IE', name: 'Irish (Ireland)' },
            { value: 'it_IT', name: 'Italian (Italy)' },
            { value: 'it_CH', name: 'Italian (Switzerland)' },
            { value: 'ja_JP', name: 'Japanese (Japan)' },
            { value: 'kl_GL', name: 'Kalaallisut, Greenlandic (Greenland)' },
            { value: 'kn_IN', name: 'Kannada (India)' },
            { value: 'ks_IN', name: 'Kashmiri (India)' },
            { value: 'kk_KZ', name: 'Kazakh (Kazakstan)' },
            { value: 'km_KH', name: 'Khmer (Cambodia)' },
            { value: 'rw_RW', name: 'Kinyarwanda (Rwanda)' },
            { value: 'ky_KG', name: 'Kirghiz, Kyrgyz (Kyrgyzstan)' },
            { value: 'ko_KR', name: 'Korean (Korea, Republic of)' },
            { value: 'ku_TR', name: 'Kurdish (Turkey)' },
            { value: 'lo_LA', name: 'Lao (Lao Peoples Democratic Republic)' },
            { value: 'lv_LV', name: 'Latvian (Latvia)' },
            { value: 'li_BE', name: 'Limburgish, Limburgan, Limburger (Belgium)' },
            { value: 'li_NL', name: 'Limburgish, Limburgan, Limburger (Netherlands)' },
            { value: 'lt_LT', name: 'Lithuanian (Lithuania)' },
            { value: 'lg_UG', name: 'Luganda (Uganda)' },
            { value: 'mk_MK', name: 'Macedonian (Macedonia)' },
            { value: 'mg_MG', name: 'Malagasy (Madagascar)' },
            { value: 'ms_MY', name: 'Malay (Malaysia)' },
            { value: 'ml_IN', name: 'Malayalam (India)' },
            { value: 'mt_MT', name: 'Maltese (Malta)' },
            { value: 'gv_GB', name: 'Manx (United Kingdom)' },
            { value: 'mr_IN', name: 'Marathi (Marāṭhī) (India)' },
            { value: 'mn_MN', name: 'Mongolian (Mongolia)' },
            { value: 'mi_NZ', name: 'Māori (New Zealand)' },
            { value: 'ne_NP', name: 'Nepali (Nepal)' },
            { value: 'se_NO', name: 'Northern Sami (Norway)' },
            { value: 'nb_NO', name: 'Norwegian Bokmål (Norway)' },
            { value: 'nn_NO', name: 'Norwegian Nynorsk (Norway)' },
            { value: 'oc_FR', name: 'Occitan (France)' },
            { value: 'or_IN', name: 'Oriya (India)' },
            { value: 'om_ET', name: 'Oromo (Ethiopia)' },
            { value: 'om_KE', name: 'Oromo (Kenya)' },
            { value: 'pa_IN', name: 'Panjabi, Punjabi (India)' },
            { value: 'pa_PK', name: 'Panjabi, Punjabi (Pakistan)' },
            { value: 'fa_IR', name: 'Persian (Iran, Islamic Republic of)' },
            { value: 'pl_PL', name: 'Polish (Poland)' },
            { value: 'pt_BR', name: 'Portuguese (Brazil)' },
            { value: 'pt_PT', name: 'Portuguese (Portugal)' },
            { value: 'ro_RO', name: 'Romanian, Moldavian, Moldovan (Romania)' },
            { value: 'ru_RU', name: 'Russian (Russian Federation)' },
            { value: 'ru_UA', name: 'Russian (Ukraine)' },
            { value: 'sa_IN', name: 'Sanskrit (Saṁskṛta) (India)' },
            { value: 'sc_IT', name: 'Sardinian (Italy)' },
            { value: 'gd_GB', name: 'Scottish Gaelic; Gaelic (United Kingdom)' },
            { value: 'sr_ME', name: 'Serbian (Montenegro)' },
            { value: 'sr_RS', name: 'Serbian (Serbia)' },
            { value: 'si_LK', name: 'Sinhala, Sinhalese (Sri Lanka)' },
            { value: 'sk_SK', name: 'Slovak (Slovakia)' },
            { value: 'sl_SI', name: 'Slovene (Slovenia)' },
            { value: 'so_DJ', name: 'Somali (Djibouti)' },
            { value: 'so_ET', name: 'Somali (Ethiopia)' },
            { value: 'so_KE', name: 'Somali (Kenya)' },
            { value: 'so_SO', name: 'Somali (Somalia)' },
            { value: 'nr_ZA', name: 'South Ndebele (South Africa)' },
            { value: 'st_ZA', name: 'Southern Sotho (South Africa)' },
            { value: 'es_AR', name: 'Spanish; Castilian (Argentina)' },
            { value: 'es_BO', name: 'Spanish; Castilian (Bolivia)' },
            { value: 'es_CL', name: 'Spanish; Castilian (Chile)' },
            { value: 'es_CO', name: 'Spanish; Castilian (Colombia)' },
            { value: 'es_CR', name: 'Spanish; Castilian (Costa Rica)' },
            { value: 'es_DO', name: 'Spanish; Castilian (Dominican Republic)' },
            { value: 'es_EC', name: 'Spanish; Castilian (Ecuador)' },
            { value: 'es_SV', name: 'Spanish; Castilian (El Salvador)' },
            { value: 'es_GT', name: 'Spanish; Castilian (Guatemala)' },
            { value: 'es_HN', name: 'Spanish; Castilian (Honduras)' },
            { value: 'es_MX', name: 'Spanish; Castilian (Mexico)' },
            { value: 'es_NI', name: 'Spanish; Castilian (Nicaragua)' },
            { value: 'es_PA', name: 'Spanish; Castilian (Panama)' },
            { value: 'es_PY', name: 'Spanish; Castilian (Paraguay)' },
            { value: 'es_PE', name: 'Spanish; Castilian (Peru)' },
            { value: 'es_PR', name: 'Spanish; Castilian (Puerto Rico)' },
            { value: 'es_ES', name: 'Spanish; Castilian (Spain)' },
            { value: 'es_US', name: 'Spanish; Castilian (United States)' },
            { value: 'es_UY', name: 'Spanish; Castilian (Uruguay)' },
            { value: 'es_VE', name: 'Spanish; Castilian (Venezuela)' },
            { value: 'ss_ZA', name: 'Swati (South Africa)' },
            { value: 'sv_FI', name: 'Swedish (Finland)' },
            { value: 'sv_SE', name: 'Swedish (Sweden)' },
            { value: 'tl_PH', name: 'Tagalog (Philippines)' },
            { value: 'ta_IN', name: 'Tamil (India)' },
            { value: 'tt_RU', name: 'Tatar (Russian Federation)' },
            { value: 'te_IN', name: 'Telugu (India)' },
            { value: 'th_TH', name: 'Thai (Thailand)' },
            { value: 'ti_ER', name: 'Tigrinya (Eritrea)' },
            { value: 'ti_ET', name: 'Tigrinya (Ethiopia)' },
            { value: 'ts_ZA', name: 'Tsonga (South Africa)' },
            { value: 'tn_ZA', name: 'Tswana (South Africa)' },
            { value: 'tr_CY', name: 'Turkish (Cyprus)' },
            { value: 'tr_TR', name: 'Turkish (Turkey)' },
            { value: 'tk_TM', name: 'Turkmen (Turkmenistan)' },
            { value: 'ug_CN', name: 'Uighur, Uyghur (China)' },
            { value: 'uk_UA', name: 'Ukrainian (Ukraine)' },
            { value: 'ur_PK', name: 'Urdu (Pakistan)' },
            { value: 'uz_UZ', name: 'Uzbek (Uzbekistan)' },
            { value: 've_ZA', name: 'Venda (South Africa)' },
            { value: 'vi_VN', name: 'Vietnamese (Vietnam)' },
            { value: 'wa_BE', name: 'Walloon (Belgium)' },
            { value: 'cy_GB', name: 'Welsh (United Kingdom)' },
            { value: 'fy_DE', name: 'Western Frisian (Germany)' },
            { value: 'fy_NL', name: 'Western Frisian (Netherlands)' },
            { value: 'wo_SN', name: 'Wolof (Senegal)' },
            { value: 'xh_ZA', name: 'Xhosa (South Africa)' },
            { value: 'yi_US', name: 'Yiddish (United States)' },
            { value: 'yo_NG', name: 'Yoruba (Nigeria)' },
            { value: 'zu_ZA', name: 'Zulu (South Africa)' }
          ]
}