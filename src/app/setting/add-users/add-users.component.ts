import { Component, OnInit , AfterViewInit, AfterViewChecked } from '@angular/core';
import {
    FormsModule, FormGroup,FormArray, FormControl, Validators, FormBuilder
} from '@angular/forms';

import { UserService } from '../../service/user.service';
import { Baseurl } from '../../service/baseUrl';

declare var AWS: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent {
  myForm: FormGroup;
  submitted = false;
  private email_validation_msg;
  private admin_id = AWS.config.credentials.params.IdentityId;
  base_class_obj = new Baseurl();
  constructor(public authUser: UserService, public formBuilder: FormBuilder){
    toastr.options = { "progressBar": true }
    // this.initializeForm();
    this.myForm = this.formBuilder.group({
      itemRows: this.formBuilder.array([this.initializeForm()])
    });
  }

  ngAfterViewChecked(){
    $(".phone").intlTelInput();
  }

  ngAfterViewInit(){
    $(".phone").intlTelInput();
  }

  initializeForm() {
    return this.formBuilder.group({
      'email': ["", Validators.compose([Validators.required, Validators.pattern(this.check_valid_email())] ) ],
      'nickname': ["", Validators.compose([Validators.required])],
      'phone_number': [""],
     });
  }

  check_valid_email(){
    var pureEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pureEmail;
  }

  focus_out(i: number, event, email_value: boolean){
      this.email_validation_msg = false;
      // var email_ele = document.getElementsByClassName('index_');
      // for (var  p = 0; p < email_ele.length; p++) {
      //   var mail = $(email_ele[p]).val();
      // }
      var mail = event.target.value;
      if (mail.length==0) {
         $('.index_idx_'+i).css('display', 'none');
      }
      if(email_value) {
        this.authUser.check_existing_email_address(mail, this.base_class_obj.subdomain).subscribe(data=>{
          if(data['emailExist']){ // true
            $('.index_idx_'+i).css('display', 'block');
          }
          else{
            $('.index_idx_'+i).css('display', 'none');
          }
          if(data['emailExist']==false){
            this.email_validation_msg=true;
          }
        });
      }
  }

  save_user(){
    var valid_number = true;
    var cusid_ele = document.getElementsByClassName('phone');
    for (var j = 0; j < cusid_ele.length; ++j) {
        var phoneNumber = $(cusid_ele[j]).intlTelInput("getNumber");
        this.myForm.controls.itemRows.value[j].phone_number = phoneNumber;
        var valid_num = $(cusid_ele[j]).intlTelInput("isValidNumber");
        if(valid_num==false){
          var valid_number = false;
        }
    }
     if (this.myForm.valid){
      let data =  this.myForm.controls.itemRows.value;
      if (this.email_validation_msg) {
        this.authUser.create_admin_user(data, this.base_class_obj.subdomain).subscribe(data=>{
          if (data.status==200) {
            toastr.success('Add successfully');
            this.initializeForm();
            this.reset_data();
          }
        });
      }
     } else{
        toastr.info('All field required');
      }
  }


  check_phone_number(j){ // working on focusout
    var phone_ = document.getElementsByClassName('phone');
    var isValid_num = $(phone_[j]).intlTelInput("isValidNumber");
    if(isValid_num){
      $('.invalid_'+j).css('display', 'none');
    }
    else{
      $('.invalid_'+j).css('display', 'block');
    }
  }

  addNewRow() {
    const control = <FormArray>this.myForm.controls['itemRows'];
    control.push(this.initializeForm());
    this.ngAfterViewChecked();
  }

  deleteRow(index: number) {
    const control = <FormArray>this.myForm.controls['itemRows'];
    control.removeAt(index);
  }
  
  reset_data(){
    this.myForm.reset();
  }

  private phone_number;
  private for_false;
  private phone_validation_msg = false;
}