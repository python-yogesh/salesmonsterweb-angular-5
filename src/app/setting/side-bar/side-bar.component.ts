import { Component, OnInit } from '@angular/core';
import { CookieService } from 'angular2-cookie/core';
import { CognitoUtil } from "../../service/cognito.service";
import { UserService } from '../../service/user.service';
import { Baseurl } from '../../service/baseUrl';


@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})

export class SideBarComponent implements OnInit {
  private current_user;
  private organization;
  private current_user_group;
  base_class_obj = new Baseurl();

  constructor(public authUser: UserService, private _cookieService: CookieService, 
              public cognitoUtil: CognitoUtil){
      this.getCurrentUserParameters();
      this.getCurrentUser();
  }

  ngOnInit(){}

  getCurrentUserParameters(){
    // this.current_user = this.cognitoUtil.get_CurrentUser_by_session_token();
  }

  getCurrentUser(){
    this.organization=this.authUser.get_organization_(this.base_class_obj.subdomain).subscribe(data=>{ 
      this.current_user_group = data['response_organizations_users']['group'];
    });
  }
}