import { Component, OnInit } from '@angular/core';
// import services
import { UserService } from '../../service/user.service';
import { UserLoginService } from "../../service/user-login.service";
import { Baseurl } from "../../service/baseUrl";
import { JqueryComponent } from '../../shared/jqueryComponent';

declare var toastr: any;

@Component({
  selector: 'app-user-permissions',
  templateUrl: './user-permissions.component.html',
  styleUrls: ['./user-permissions.component.css']
})

export class UserPermissionsComponent implements OnInit{
  private user_object = {};
  private user_list_data;
  private user_enable = true;
  private loader_image:boolean;
  private active_user_activities='active';
  base_class_obj = new Baseurl();

  constructor(public userService: UserLoginService, public authUser: UserService){
    // this.userService.getUserParameters(this);
    this.get_user_list();
    this.get_users_permissions();
  }
  ngOnInit(){
    toastr.options = { "progressBar": true }
  }
  
  // callback(){}
  // call back for getUserParameters
  // callbackWithParam(result: any){
  //   for (let i = 0; i < result.length; i++){
  //     this.user_object[result[i].Name] = result[i].Value;
  //   }
  // }
  // yes/no change into table
  is_admin(user){
    user.is_admin = user.is_admin == 'yes' ? 'no' : 'yes';
    this.saveEditable(user);
  }

  initDatatable(){
    let jq = new JqueryComponent();
    jq.initializeJs();
  }

  // get all users listing created by master admin
  get_user_list(){
    this.loader_image=true;
    var subdomain = false;
    this.authUser.get_user_list_api(subdomain).subscribe(data=>{
      // The 1st callback handles the data emitted by the observable.
      this.user_list_data = data['users'].filter((x)=>{ return x.enabled == this.user_enable });
      // console.log(this.user_list_data);
      if(data!=''){
          this.loader_image=false;
        }
    },
      // The 2nd callback handles errors.
      (err) => console.error(err),
      // The 3rd callback handles the "complete" event.
      () => console.log("observable complete")
    );
  }

  // update single column data for user table.
  saveEditable(user){
    // console.log(user);
    this.authUser.update_editable_users(user, user.id).subscribe(data=>{
      if (data.status == 200){
        toastr.success('update successfully')
      }
    });
  }

  deactivate_user(email, enabled_value){
    let params = {'enabled': enabled_value, 'email': email }
    this.authUser.active_deactive_users(params).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.user_enable = true;
        this.get_user_list();
      }
      // console.log('user active deactive data',data);
    });
  }
  
  private deactive_activity;
  // get 'Deactivate' users list
  activate_user(value: boolean){
    this.user_enable = value;
    if(this.user_enable==false){
      this.deactive_activity='active';
      this.active_user_activities='';
    }
    else{
      this.active_user_activities='active';
      this.deactive_activity='';
    }
    this.get_user_list();
  }
  
  // get user-permission function
  private userPermissionField;
  get_users_permissions(){
    this.authUser.get_users_permissions_api(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          this.userPermissionField = data['Items'];
          // console.log(this.userPermissionField);
      }
    });
  }

  edit_users_permissions_fields(uuid,userPermissionStatus){
    let data = {};
    data['is_activate'] = userPermissionStatus;
    this.authUser.edit_users_permissions_fields_api(uuid,data).subscribe(data=>{
      if (data.status == 200){
         this.get_users_permissions();
      }
    });
  }
}