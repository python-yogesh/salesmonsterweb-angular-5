import {Component} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
// import {CookieService} from 'angular2-cookie/core';

import { UserService } from '../service/user.service';
import { UserLoginService } from "../service/user-login.service";

declare var toastr: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})

export class WelcomeComponent {
  subDomain: any;
  constructor(public userService: UserLoginService, public authUser: UserService,
              public router: Router, private route: ActivatedRoute){
    toastr.options = { "progressBar": true }
  }

  comp;
  ngOnInit(){
    this.comp = this.route.component;
    this.userService.isAuthenticated(this);
  }

  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }else{
      toastr.success('Welcome on salesmonster');
      localStorage.clear();
    }
  }
}