import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CustomFieldsService, UserService, CompanyService, DealService, ActivityEventClass } from '../../service/user.service'; 
import { Baseurl } from '../../service/baseUrl'; 
@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {
  base_class_obj = new Baseurl();
  constructor(private activatedRoute: ActivatedRoute,private authuserservice: UserService) {
    this.activatedRoute.params.subscribe(params => {
      this.follower_id = +params['id'];
      this.deal_id = +params['deal_id'];
      this.follower_name_ = params['name'];
      this.follower_email = params['email'];
    });
    this.get_not_added_followers();
    this.get_added_follower();
  }

  private follower_id;
  private bttn = false;
  private not_follow;
  private deal_id;
  private added_data;
  private follower_name;
  private follower_name_;
  private follower_email;
  id:number;

  ngOnInit() {}

  get_followers_detail(){

  }

  hide_show_div(){
    if(this.bttn == false){
      this.bttn = true;
    }
    else{
      this.bttn = false;
    }
  }

  get_not_added_followers(){
    this.authuserservice.get_not_added_followers(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      this.not_follow = data;
    })
  }

  get_added_follower(){
   this.authuserservice.get_added_followers(this.deal_id, this.base_class_obj.subdomain).subscribe(data=>{
      this.added_data = data.Items;
    })
  }

  selected_follower(value){
    this.follower_id = value.target.value;
    this.follower_name = value.currentTarget.selectedOptions[0].text;
    let data={'follower_id':this.follower_id,'follower_name':this.follower_name}
    this.authuserservice.post_add_follower(this.deal_id, this.base_class_obj.subdomain, data).subscribe(data=>{
      this.get_added_follower();
      this.bttn = false;
   })  
  }

  delete_follower(uuid){
    this.authuserservice.delete_followers(uuid).subscribe(data=>{
      this.get_added_follower();
      this.get_not_added_followers();
    })
  }
}
