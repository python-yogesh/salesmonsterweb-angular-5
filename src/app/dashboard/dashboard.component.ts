import {Component, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { BsModalComponent } from 'ng2-bs3-modal';
import { Router } from "@angular/router";
import { FormGroup, Validators, FormBuilder, NgForm } from '@angular/forms';
import { CookieService, CookieOptionsArgs } from 'angular2-cookie/core';
//import external classes;
import { SharedComponent } from '../shared/_sharedComponent';
// import services
import { UserLoginService } from "../service/user-login.service";
import { UserService, Pipeline, DealService, CompanyService } from '../service/user.service';
import { Baseurl } from '../service/baseUrl';
import { JqueryComponent } from '../shared/jqueryComponent';
import { ModalPopupComponent } from '../activity/modal-popup/modal-popup.component';
import { AddDealsComponent } from '../add-deals/add-deals.component';

declare var toastr: any;
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  @ViewChild('addActivity') addActivity_modal: BsModalComponent;

  @ViewChild('lost_reason_modal') lost_reason_modal_popup: BsModalComponent;

  @ViewChild('my_modal_popup') my_modal_popup: ModalPopupComponent;

  @ViewChild('add_deal_form_obj') add_deal_form_obje: AddDealsComponent;

  private isActive: boolean;
  private pipeline_list_arr;
  public input_pipeline_obj: any;
  private pipeline_name: string;
  private pipeline_uuid: any;
  private loader_image:boolean;
  private _sharedComponent = new SharedComponent();
  public deal_uuid_;
  public deal_object;
  private user_list_data;
  applicationServerPublicKey = 'BMAdL-n9FodZJRlXUiV4NjOcWQlK9n1xRby4P_iL2GyZ2pYPUL-qWrbUxZ_ge4w_yvZzbx9xh3j2aOwCR5XH1tQ';

  base_class_obj = new Baseurl();
  constructor(private router: Router, private userService: UserLoginService, 
              private _cookieService: CookieService, private authUser: UserService,
              private pipeline_service: Pipeline,
              private deal_services: DealService, private company_services: CompanyService,
              private formBuilder: FormBuilder){
      this.get_organization();
      // this.userService.isAuthenticated(this);
      this.initialize_methods();
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
        this.router.navigate(['']);
    }
  }
  
  get_organization(){
    this.authUser.get_organization_(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['organization']){ // return false don't have a org
        this.router.navigate(['/dashboard']);
      }else{
        this.router.navigate(['/company-registration']);
      }
    });
  }

  initialize_methods(){
    toastr.options = { "progressBar": true }
    // toastr.success('Welcome')
    this.get_pipeline_list();
    this.get_lost_reason();
    this.get_user_list();
  }

  // get users listing
  private owner_name;
  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.user_list_data = data['users'];
    },(err)=>{});
  }

  showEvents() {
    // console.log('emit pipelines')
    this.get_pipeline_list();
  }
    
  private activity_overdue_title = 'Loading...';
  private activity_planned_title;
  private activities_list;
  private activity_data_loading;
  private activity_planned_hide_show;
  private count = 0;
 
  get_popover_px_1(v){
    var data;
    if (window.screen.availWidth == 1366) { 
      data = [ {id: 1, value: 64.6}, {id: 2, value: 26.5}, {id: 3, value: 14.6}, {id: 4, value: 8.5}, {id: 5, value: 4.7}, {id: 6, value: 2.1}, {id: 7, value: 0.4} ]
    } else {
      data = [ {id: 1, value: 67.7}, {id: 2, value: 30.1}, {id: 3, value: 17.6}, {id: 4, value: 11.5}, {id: 5, value: 7.5}, {id: 6, value: 5.1}, {id: 7, value: 3.4} ]
    }    
    var dd = data.filter(x=>{
      if (x.id == v) { return x.value; }
    });
    return dd;
  }

  get_pixel_(indx, stage_percentage, init_value){
    let final_value = init_value + (indx*stage_percentage);
    return final_value;
  }

  private LeftPer: any = 0;
  
  // get created activity listing in pop over
  get_activities_list(deal_uuid, myPopover, deal_data, stage, indx){
    this.deal_object = deal_data;
    this.deal_uuid_ = deal_uuid;
    this.activity_data_loading = false;
    this.activity_planned_hide_show = false;
    this.activity_overdue_title = 'Loading...';
    var stage_percentage = 100/this.stages_arr.length;
    let pixel = this.get_popover_px_1(this.stages_arr.length);
    var left_percentage = this.get_pixel_(indx, stage_percentage, pixel[0].value);
    
    if (this.last_stage_uuid == stage.uuid) {
      // $('.content-popover .arrow').css('left','86%');
    }
    
    this.authUser.get_activity(deal_uuid, this.base_class_obj.subdomain).subscribe(data=>{
      $(".popover").css('left', left_percentage+'%');
      
      // activity_overdue_count response
      if (data[0].activity_overdue_count!=0) {
        this.activity_overdue_title = data[0].activity_name +' '+ data[0].activity_overdue_count;
      }
      // activity_planned_count response
      if (data[1].activity_planned_count!=0) {
        this.activity_planned_hide_show = true;
        this.activity_planned_title = data[1].activity_name +' '+ data[1].activity_planned_count;
        if (data[0].activity_overdue_count==0) {
          this.activity_planned_hide_show = false;
          this.activity_overdue_title = data[1].activity_name +' '+ data[1].activity_planned_count;
        }
      }

      if (data[1].activity_planned_count==0 && data[0].activity_overdue_count==0) {
        this.activity_overdue_title = 'You have no activities scheduled for this deal.';
      }else{
        this.activity_data_loading = true;
        this.activities_list = data;
      }
      // console.log('get activity listing',data);
    });
  }

  // click on check box set activity on true/false
  change_activity_status(activity_uuid, value: boolean, myPopover){
    // console.log(activity_uuid, value)
    var data = { 'activity_complete_status': value };
    this.deal_services.change_activity_status_api(activity_uuid, data).subscribe(data=>{
      // console.log(data);
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        myPopover.hide();
        this.get_pipeline_list();
      }
    })
  }

  // get activated fa fa icons listing
  private activity_fa_fa_icon;
  private activity_placeholder_name;
  private previous_activity;
  private activity_field_uuid;

  // click on single activity
  private start_at_time;
  private end_at_time;
  private activity_id;

  get_one_activity(activity_uuid,tooltip_name){
    this.my_modal_popup.get_one_activity(activity_uuid,tooltip_name);
  }

  schedule_activity(){
    this.my_modal_popup.open_();
  }

  save(string){
    // let aa = string.charAt(0).toUpperCase() + string.slice(1);
    // return aa.replace(/[/_/]/g, ' ');
  }

  myFunction(){ $('#myModal').modal('show'); }

  ngAfterViewInit(){
    let jq = new JqueryComponent();
    jq.initializeJs();
    
    
  }

  replaceLineBreak(str) { return str ? String(str).replace(/<[^>]+>/gm, '') : '' }

  // demo_(){
  //   this.authUser.get_subject();
  // }

  ngOnInit(){ 
    // this.authUser.NewSub.subscribe(data=> console.log('Subscribe',data));
    // toastr.success('Welcome')
  }
  
  countChange_dashboard(event){
    // console.log('countChange dashboard', event);
    this.get_pipeline_stage(event);
  }

  get_pipeline_list(){
    this.loader_image = true;
    this.pipeline_service.get_pipeline_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      let pipeline_list = data.Items.filter((x)=>{ return x.count !=0 });
      this.add_deal_form_obje.pipeline_list_for_add_deal(pipeline_list);
      this.pipeline_list_arr = pipeline_list;
      this.pipeline_uuid = data.Items[0].uuid;
      this.get_pipeline_stage(this.pipeline_list_arr[0]);
      if(data!=''){
        this.loader_image=false;
      }
    }, error=> console.log('error=',error));
  }

  private stages_arr;
  get_pipeline_stage(pipeline_obj){
    this.loader_image = true;
    this.pipeline_service.before_call_pipeline(pipeline_obj.uuid, this.base_class_obj.subdomain).subscribe(res=>{});
    // console.log(pipeline_obj);
    // this._cookieService.putObject('pipeline', pipeline_obj);
    this.input_pipeline_obj = pipeline_obj;
    this.pipeline_name = pipeline_obj.pname;
    this.get_pileline_stages_data(pipeline_obj.uuid);
    // console.log(pipeline_obj.uuid)              
    this.add_deal_form_obje.show_pipeline(pipeline_obj);
  }

  private total_currency;
  private set_currency;
  private total_deals;
  private widthPer;
  private widthNew;
  private pipeline_id;
  private last_stage_uuid;

  get_pileline_stages_data(pipeline_uuid){
    let filter_value = '?user_name='+this.filter_user;
    this.partial_method(pipeline_uuid, filter_value);
  }

  // partial_method for filter users, pipeline stages
  partial_method(pipeline_uuid, filter_value){
    this.loader_image = true;
    this.authUser.filter_pipeline_stage_deals(pipeline_uuid, filter_value, this.base_class_obj.subdomain)
    .subscribe(data=>{
      console.log('stages data',data)
      this.stages_arr = data[0];
      var len = data[0].length-1
      
      this.last_stage_uuid = data[0][len].uuid;
      this.pipeline_id = pipeline_uuid;
      this.total_currency = data[1].currency_total_value;
      this.set_currency = data[2].current_currency;
      this.total_deals = data[3].total_deal_count;
      this.widthPer = 100/data[4].stage_count+"%";
      this.widthNew = 100/data[4].stage_count-0.1+"%";
      this.loader_image = false;
      // console.log('pipeline stages list', data);
    }, (err)=> {});
  }
  
  // pipeline stages deal list by pipeline_uuid
  // working on when we click on pipiline dropdown
  
 
  private user_name = "Everyone";
  private filter_user = 'all';
  get_stages_data_by_users_filter(user_obj){
    if (user_obj == 'all') {
      this.filter_user = user_obj;
      this.user_name = "Everyone"
    }else{
      this.filter_user = user_obj.email;
      this.user_name = user_obj.nickname;
    }
    let filter_value = '?user_name='+this.filter_user;
    this.partial_method(this.input_pipeline_obj.uuid, filter_value);
  }

  // Filter Won, Lost, Open, Rotten deals
  filter_deal_data(deal_filterObj){
    this.filter_user = deal_filterObj.dealStatusValue;
    this.user_name = deal_filterObj.fiterTextShow;
    let filter_value = '?filter='+this.filter_user;
    this.partial_method(this.input_pipeline_obj.uuid, filter_value);
  }

  // drag drop data in stages
  move_drop_data(event, stage_uuid){ // event is a stage data
   this.loader_image = true;
    var data = {'uuid': event.dragData.uuid} // move in other stage uuid
    this.authUser.move_pipeline_deal(data, stage_uuid, this.base_class_obj.subdomain).subscribe(data=>{
      this.get_pileline_stages_data(event.dragData.pipeline_uuid);
    });
  }

  lost_deal(lostDeal){
    this.authUser.won_lost_api(this.deal_uuid, lostDeal.value, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.get_pipeline_list();
        this.lost_reason_modal_popup.close();
        toastr.success('Lost Deal successfully');
        lostDeal.controls.lost_reason.reset();
        lostDeal.controls.lost_reason_comment.reset();
      }
    },(err) => console.error(err) );
  }

  deleteDeal(deal_uuid){
    let data = {};
    this.authUser.delete_deal_api(deal_uuid, data,this.base_class_obj.subdomain).subscribe(data=>{
      if(!data['error']){
        toastr.success('Delete Deal successfully');
        this.get_pipeline_list();
      }
    });
  }

  update_won(deal_uuid){
    var won = {'deal_won': 2};
    this.authUser.won_lost_api(deal_uuid, won, this.base_class_obj.subdomain).subscribe(data=>{
       this.get_pipeline_list();
       toastr.success('Won Deal successfully');
    },(err) => console.error(err) );
  }


  send_to_deal(dealuuid,stage_n){
    this.router.navigate(["/dashboard/deal", dealuuid, stage_n.stage, this.pipeline_id]);
  }

  private reason_select_option: boolean;
  private other_reason_visibility_textbox: boolean;
  private lost_reason: any;
  private other_reason_text;
  private lost_reason_listing;
  
  // get lost reason listing
  get_lost_reason(){
    this.authUser.get_lost_reason(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['Items'].length<1) {
        this.other_reason_visibility_textbox = false;
      }else{
        this.lost_reason_listing = data['Items'];
        this.reason_select_option = true;
      }
    });
  }

  reason_on_change(event){
    this.lost_reason = event.target.value;
    if (event.target.value == 'Other...') {
      this.other_reason_text = 'Other'
      this.other_reason_visibility_textbox = true;
    }else{
      this.other_reason_text = '';
      this.other_reason_visibility_textbox = false;
    }
  }

  removeItem(item: any, list: Array<any>) {
      let index = list.map((e) => {
          return e.name
      }).indexOf(item.name);
      list.splice(index, 1);
  }
  
  private drop_data_in_footer;
  private deal_uuid;

  hideFooter(){ this.isActive = false; }

  mouse_leave_hide_footer(){
    this.isActive = false;
    $("div").removeClass("dnd-drag-over");
  }

  drop_deleted_deal(value){
    this.deal_uuid = this.drop_data_in_footer.uuid;
    if (value == 'delete'){
      // exicute block on delete condition
      this.deleteDeal(this.deal_uuid);
    } else if (value == 'lost') {
      this.lost_reason_modal_popup.open('sm');
      // exicute block on lost condition
    } else {
      // exicute block on won condition
      this.update_won(this.deal_uuid);
    }
  }
  
  private drag_footer(event): void {
    this.drop_data_in_footer = event;
    this.isActive = true;
  }
}