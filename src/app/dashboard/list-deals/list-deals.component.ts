import { Component, OnInit, AfterViewInit, ViewChild, Pipe, Renderer, PipeTransform, TemplateRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalComponent } from 'ng2-bs3-modal';
import { BrowserModule, DomSanitizer, SafeHtml } from '@angular/platform-browser'
import { UserService, Pipeline, CustomFieldsService} from '../../service/user.service';
import { Baseurl } from '../../service/baseUrl';
import { JqueryComponent } from '../../shared/jqueryComponent';

declare var toastr: any;
declare var $: any;

@Pipe({
  name: 'objectValues'
})
export class ObjectValuesPipe implements PipeTransform {
  transform(obj: any) {
  let result = [];
  for (var key in obj) {
    result.push(Object.keys(obj[key]))
  }
    return result;
  }
}

@Pipe({name: 'orderBy'})
export class OrderBy implements PipeTransform {
  transform(input:any, args:string[]) : any {
    return input;
  }
}

@Component({
  selector: 'app-list-deals',
  templateUrl: './list-deals.component.html',
  styleUrls: ['./list-deals.component.css']
})

export class ListDealsComponent implements OnInit {
  dealForm: FormGroup;
  custom_field: any;
  private deal_data;
  private loadTable: boolean;
  private loader_image: boolean;
  private user_list_data;
  formShowHide: boolean = false;
  base_class_obj = new Baseurl();
  
  @ViewChild('addDealPopup')
  addDealPopup_modal: BsModalComponent;
  
  @ViewChild('html_content') data_html: TemplateRef<any>;

  constructor(public authUser: UserService, public pipeline_service: Pipeline,  
              public formBuilder: FormBuilder,public custom_field_service: CustomFieldsService,
              private sanitizer: DomSanitizer, private elRef: ElementRef,  private renderer:Renderer){
    this.dealForm = this.formBuilder.group({
      'customer_name': ['', Validators.compose([Validators.required])],
      'customer_organization': ['', Validators.compose([Validators.required])],
      'deal_title': ['', Validators.compose([Validators.required])],
      'deal_value': ['', Validators.compose([Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')])],
      'currency': ['', Validators.compose([Validators.required])],
      'expected_close_date': ['', Validators.compose([Validators.required])],
      'pipeline_uuid': [''],
    });
    this.get_user_list();
    this.get_deals_list('all');
    this.list_of_fields();
  }
  
  ngOnInit(){}
  ngAfterViewInit(){}

  private deal_count;
  private currency_total;
  private currency;  
  private user_name = "Everyone";
  private filter_user = 'all';

  get_deals_list(user_obj){
    if (user_obj == 'all') {
      this.filter_user = user_obj;
      this.user_name = "Everyone"
    }else{
      this.filter_user = user_obj.email;
      this.user_name = user_obj.nickname;
    }
    // console.log(this.user_name);
    let filter_value = '?user_name='+this.filter_user;
    this.authUser.get_deal_list_v2(this.base_class_obj.subdomain, filter_value).subscribe(data=>{
      // console.log(data.json());
      this.deal_data = data;
      this.deal_count = data[2]['deal_count'];
      this.currency_total = data[3]['currency_total'];
      this.currency = data[4]['currency'];
      this.loadTable = true;
    });
  }

  filter_get_deals_list(deal_filterObj){
    this.filter_user = deal_filterObj.dealStatusValue;
    this.user_name = deal_filterObj.fiterTextShow;
    let filter_value = '?filter='+this.filter_user;
    this.authUser.get_deal_list_v2(this.base_class_obj.subdomain, filter_value).subscribe(data=>{
        this.deal_data = data;
        this.deal_count = data[2]['deal_count'];
        this.currency_total = data[3]['currency_total'];
        this.currency = data[4]['currency'];
        this.loadTable = true;
    });
  }

  /*** get users listing **/
  private owner_name;
  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.user_list_data = data['users'];
    });
  }
  /****/

  deal_list_change(event){
    this.deal_data = event;
  }
  // list of 'isActive' true Custome fields
  list_of_fields(){
    this.loader_image=true;
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
    // console.log('isActive true custom field listing response', data);
    this.custom_field = data['deal_fields'];
      for(let key of this.custom_field){
        if (key.is_checked){
            this.checkbox_arr.push({
                'uuid': key.uuid,
                'is_checked': key.is_checked
          });
        }
      }
      this.initialize_fields_data();
      if(data!=''){
          this.loader_image=false;
        }

    });
  }
  private ff = true;
  private search_data;
  initialize_fields_data(){
    this.search_data = this.custom_field;
  }
  
  dynamic_arr(obj){
    if(Array.isArray(obj)){
        // console.log(obj);
        var d = []
        for(let i in obj){
          d.push(obj[i].id);
        }
        return d;
    }else{
      return obj;
    }
  }
  
  checkbox_arr = [];
  disable_save_btn: boolean = false;
  checkbox(check, uuid){
    this.disable_save_btn = true;
    if(check.checked){
        this.checkbox_arr.push({
          'uuid': uuid,
          'is_checked': check.checked
        });
    }
    else{
      for(var i = this.checkbox_arr.length - 1; i >= 0; i--) {
        if(this.checkbox_arr[i].uuid === uuid) {
            this.checkbox_arr[i].is_checked = false;
            break;
        }
      }
    }

    this.checkbox_arr.forEach(x=>{
      if (x.is_checked==true){
        this.disable_save_btn = false;
      }
    });
  }

  // update check box vlue true/false
  update_checkbox(){
    this.custom_field_service.checkbox_active('deals_fields',this.checkbox_arr).subscribe(data=>{
      this.checkbox_arr = [];
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.list_of_fields();
        this.get_deals_list('all');
      }
    });
  }
  
  showHide(formShowHide){ $("#toggle").toggle("slide"); }

  filter_fields(event){
    this.initialize_fields_data();
    let val = event.target.value;
    if (val && val.trim() != ''){
      this.search_data = this.search_data.filter((item) => {
        let name: any = item;
        return (name.label.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  saveData(deal_obj, deal_value){
    if (deal_value.name == 'title'){
      deal_obj.deal_title = deal_value.value;

    }else if(deal_value.name=='deal-value'){
      deal_obj.deal_value = deal_value.value;

    }else if(deal_value.name=='organization'){
      deal_obj.organization_name = deal_value.value;

    }else if(deal_value.name=='contact-persno'){
      deal_obj.person_name = deal_value.value;
      
    }else if(deal_value.name=='close-date'){
      deal_obj.close_date = deal_value.value;
    }
    // console.log(deal_obj);
  }
}