import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { DynamoDBService } from "./ddb.service";
import { CognitoCallback, CognitoUtil, LoggedInCallback, CognitoCallback_Login, CallbackForgotPassAgent, 
    CallbackNewPassword, CognitoCallback_Confirm_Code, Callback } from "./cognito.service";
import { AuthenticationDetails, CognitoUser, CognitoUserSession, CognitoUserAttribute } from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";
import * as STS from "aws-sdk/clients/sts";
import * as Cognitoidentityserviceprovider from "aws-sdk/clients/cognitoidentityserviceprovider";
@Injectable()
export class UserLoginService {

    private onLoginSuccess = (callback: CognitoCallback_Login, session: CognitoUserSession) => {
        console.log("In authenticateUser onSuccess callback");
        AWS.config.credentials = this.cognitoUtil.buildCognitoCreds(session.getIdToken().getJwtToken());
        
        callback.cognitoCallback_login(null, session);
    }

    private onLoginError = (callback: CognitoCallback_Login, err) => {
        callback.cognitoCallback_login(err.message, null);
    }

    constructor(public ddb: DynamoDBService, public cognitoUtil: CognitoUtil) {
        // this.verifyUserAttribute();
    }

    authenticate(username: string, password: string, callback: CognitoCallback_Login) {
        console.log("UserLoginService: starting the authentication");

        let authenticationData = {
            Username: username,
            Password: password,
        };
        let authenticationDetails = new AuthenticationDetails(authenticationData);

        let userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        console.log("UserLoginService: Params set...Authenticating the user");
        let cognitoUser = new CognitoUser(userData);
        console.log("UserLoginService: config is " + AWS.config);
        cognitoUser.authenticateUser(authenticationDetails, {
            newPasswordRequired: (userAttributes, requiredAttributes) => callback.cognitoCallback_new_password(),
            onSuccess: result => this.onLoginSuccess(callback, result),
            onFailure: err => this.onLoginError(callback, err)
        });
    }

    forgotPassword(username: string, callback: CognitoCallback) {
      let userData = {
          Username: username,
          Pool: this.cognitoUtil.getUserPool()
      };

      let cognitoUser = new CognitoUser(userData);

      cognitoUser.forgotPassword({
          onSuccess: function () {

          },
          onFailure: function (err) {
              callback.cognitoCallback(err.message, null);
          },
          inputVerificationCode() {
              callback.cognitoCallback(null, null);
          }
      });
    }

    confirmNewPassword(email: string, verificationCode: string, password: string, callback: CognitoCallback_Confirm_Code) {
      let userData = {
          Username: email,
          Pool: this.cognitoUtil.getUserPool()
      };

      let cognitoUser = new CognitoUser(userData);

      cognitoUser.confirmPassword(verificationCode, password, {
          onSuccess: function () {
              callback.cognitoCallback_confirm_code(null, null);
          },
          onFailure: function (err) {
              callback.cognitoCallback_confirm_code(err.message, null);
          }
      });
    }

    logout() {
        console.log("UserLoginService: Logging out");
        this.ddb.writeLogEntry("logout");
        this.cognitoUtil.getCurrentUser().signOut();
    }

    isAuthenticated(callback: LoggedInCallback) {
        if (callback == null)
            throw("UserLoginService: Callback in isAuthenticated() cannot be null");
        let cognitoUser = this.cognitoUtil.getCurrentUser();
        if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    console.log("UserLoginService: Couldn't get the session: " + err, err.stack);
                    callback.isLoggedIn(err, false);
                }
                else {
                    console.log("UserLoginService: Session is " + session.isValid());
                    callback.isLoggedIn(err, session.isValid());
                }
            });
        } else {
            console.log("UserLoginService: can't retrieve the current user");
            callback.isLoggedIn("Can't retrieve the CurrentUser", false);
        }
    }

    getUserAttributeVerificationCode(){
      var _token = this.cognitoUtil.get_Access_Token();
      console.log(_token)
      var cognitoidentityserviceprovider = new Cognitoidentityserviceprovider();
      console.log(cognitoidentityserviceprovider);
      var params = {
        AccessToken: _token, /* required */
        AttributeName: 'email', /* required */
      };
      
      cognitoidentityserviceprovider.getUserAttributeVerificationCode(params, function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else     console.log(data);           // successful response
      });
    }

    verifyUserAttribute(Verificationcode, callback: CallbackForgotPassAgent){
      var return_response;
      var _token = this.cognitoUtil.get_Access_Token();
      var params = {
        AccessToken: _token,
        AttributeName: 'email',
        Code: Verificationcode
      };
      var cognitoidentityserviceprovider = new Cognitoidentityserviceprovider();
      cognitoidentityserviceprovider.verifyUserAttribute(params, function(err, data) {
        callback.cognitoCallback_forgot_pass_agent(err);
        // if (err) console.log(err, err.stack); // an error occurred
        // else console.log(data);           // successful response
      });
    }

    newPassword(username: string, password: string, newPass, callback: CallbackNewPassword): void {
        // Get these details and call
        //cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
        let authenticationData = {
            Username: username,
            Password: password,
        };
        let authenticationDetails = new AuthenticationDetails(authenticationData);

        let userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        console.log("UserLoginService: Params set...Authenticating the user");
        let cognitoUser = new CognitoUser(userData);
        console.log("UserLoginService: config is " + AWS.config);
        cognitoUser.authenticateUser(authenticationDetails, {
            newPasswordRequired: function (userAttributes, requiredAttributes) {
                // User was signed up by an admin and must provide new
                // password and required attributes, if any, to complete
                // authentication.

                // the api doesn't accept this field back
                delete userAttributes.email_verified;
                cognitoUser.completeNewPasswordChallenge(newPass, requiredAttributes, {
                    onSuccess: function (result) {
                        callback.callback_login_new_password(null, userAttributes);
                    },
                    onFailure: function (err) {
                        callback.callback_login_new_password(err, null);
                    }
                });
            },
            onSuccess: function (result) {
                callback.callback_login_new_password(null, result);
            },
            onFailure: function (err) {
                callback.callback_login_new_password(err, null);
            }
        });
    }

    resendCode(username: string, callback: CognitoCallback_Confirm_Code): void {
        let userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        let cognitoUser = new CognitoUser(userData);

        cognitoUser.resendConfirmationCode(function (err, result) {
            if (err) {
                callback.cognitoCallback_confirm_code(err.message, null);
            } else {
                callback.cognitoCallback_confirm_code(null, result);
            }
        });
    }

    // get parameters authenticated user
    getUserParameters(callback: Callback){
        let cognitoUser = this.cognitoUtil.getCurrentUser();
        if (cognitoUser!=null) {
          cognitoUser.getSession(function (err, session) {
            if (err) {
              console.log("UserParametersService: Couldn't retrieve the user");
            }else{
              cognitoUser.getUserAttributes(function (err, result) {
                if (err) {
                    console.log("UserParametersService: in getParameters: " + err);
                } else {
                    callback.callbackWithParam(result);
                }
              });
            }
          });
        }else{
          callback.callbackWithParam(null)
        }
    }

    updateUserParameter(username, email, callback: CognitoCallback){
        let cognitoUser = this.cognitoUtil.getCurrentUser();
        if (cognitoUser!=null) {
          cognitoUser.getSession(function (err, session) {
            if (err) {
              console.log("UserParametersService: Couldn't retrieve the user");
            }
            else{
              let attributeList = [];
              // let dataEmail = { Name: 'email', Value: email };
              let dataNickname = { Name: 'nickname', Value: username };
              // attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail));
              attributeList.push(new CognitoUserAttribute(dataNickname));
              cognitoUser.updateAttributes(attributeList, function(error, result) {
                if (error) { // return err
                  callback.cognitoCallback(err, null);
                }else{ // return SUCCESS
                  callback.cognitoCallback(null, result);
                }
              });
            };
          });
        }
    }

    // after login change password authorized user
    changePassword(oldPassword: string, newPassword: string, callback: CognitoCallback){
      let cognitoUser = this.cognitoUtil.getCurrentUser();
      if (cognitoUser != null) {
        cognitoUser.getSession(function (err, session) {
          if (err) {
              console.log("UserLoginService: Couldn't get the session: " + err, err.stack);
          }
          else{
            cognitoUser.changePassword(oldPassword, newPassword, function(err, result) {
              if (err) {
                callback.cognitoCallback(err.message, null); 
              }else{
                callback.cognitoCallback(null, result);
              }
            });
          }
        });
      }
    }
}