import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
// import services
import { CognitoUtil } from './cognito.service';

const baseUrl: any = 'https://77qvzo6w8c.execute-api.us-west-2.amazonaws.com/dev/'; // preetam
const new_url: any = 'https://zemkxckfml.execute-api.us-west-2.amazonaws.com/dev/'; // vinod

// const baseUrl: any = 'https://mzqk52jaf0.execute-api.us-west-2.amazonaws.com/api/'; 
// const new_url: any = 'https://4uy372dhb8.execute-api.us-west-2.amazonaws.com/api/'; // vinod
var cogito_util = new CognitoUtil();

function globle_header() {
  var token = cogito_util.get_session_Token();
  var _headers = new HttpHeaders().set('Content-Type', 'application/json');
  var headers = _headers.append('Authorization', token);
  return headers;
}

@Injectable()
export class UserService {
  public NewSub:BehaviorSubject<any> = new BehaviorSubject<any>('');
  constructor(private http: HttpClient){}

  get_subject(){
    let data = { name:'avi', lastname:'jain' }
    this.NewSub.next(data);
  }

  public getJSON(): Observable<any> {
    return this.http.get("src/data/student.json");
  }
  // get single company details object using 'user_id'
  get_organization_(subdomain){
    return this.http.get(baseUrl+'v1/get-organization/'+subdomain, { headers: globle_header() });
  }

  getExistingOrgList(){
    return this.http.get(new_url+'v2/get/organization-type', { headers: globle_header() });
  }

  check_existing_email_address(email, subdomain){
    return this.http.get(baseUrl+'v1/is-user-emailexist/'+email+'/'+subdomain, { headers: globle_header() });
  }

  check_existing_phone_number(phone_number){
    return this.http.get(baseUrl+'is-user-phonenumberexist/'+phone_number, { headers: globle_header() });
  }

  create_company_(data){
    return this.http.post(new_url+'v2/create/organization', data, { headers: globle_header() });
  }
  // create a new deals
  add_deal(pipeline_uuid, data, subdomain){
    return this.http.post(new_url+'v1/pipelines/'+pipeline_uuid+'/deals/create/'+subdomain, data, { headers: globle_header() });
  }

  // filter api for deal listing
  get_deal_list_v2(subdomain, filter_value){
    return this.http.get(new_url+'v1/pipelines/organizations/deals/'+subdomain+filter_value, { headers: globle_header() });
  }

  // get all deals list using user_id
  get_deal(deal_id, subdomain){
    return this.http.get(new_url+'v1/get-deal/'+deal_id+'/'+subdomain, { headers: globle_header() });
  }

  // update deal using user_id
  update_deal_api(deal_id, data, subdomain){
    return this.http.put(baseUrl+'v1/pipelines/deals/'+deal_id+'/update/'+subdomain, data, {headers: globle_header() });
  }

  // delete deal using user_id
  delete_deal_api(deal_id,data,subdomain){
    return this.http.put(baseUrl+'v1/pipelines/deals/'+deal_id+'/delete/'+subdomain,data, { headers: globle_header() });
  }

  // won_lost using user_id
  won_lost_api(deal_id, data, subdomain){
    return this.http.put(baseUrl+'v1/pipelines/deals/'+deal_id+'/won_lost/'+subdomain, data, { headers: globle_header() });
  }
  
  // add note api
  add_takenote(deal_id, data, subdomain){
    return this.http.post(baseUrl+'v1/create/take_notes/'+deal_id+'/'+subdomain, data, { headers: globle_header() });
  }

  delete_take_note_api(uuid){
    return this.http.put(new_url+'v1/delete/take-notes/'+uuid, {}, { headers: globle_header() });
  }

  update_take_note_api(uuid, data, subdomain){
    return this.http.put(baseUrl+'v1/take_notes/edit/'+uuid+'/'+subdomain, data, { headers: globle_header() });
  }

  get_takenotes(deal_id, subdomain){
    return this.http.get(baseUrl+'v1/take_notes/'+deal_id+'/'+subdomain, { headers: globle_header() });
  }

  /* activity  */
  add_activity(deal_id,data, subdomain){
    return this.http.post(baseUrl+'v1/create/activity/'+deal_id+'/'+subdomain, data, { headers: globle_header() });
  }

  get_activity(deal_id, subdomain){
    return this.http.get(baseUrl+'v1/activitySingle/'+deal_id+'/'+subdomain, { headers: globle_header() });
  }

  get_all_activity_listing_api(deal_id): Observable<any>{
    return this.http.get(baseUrl+'v1/activity_bydeal/'+deal_id, { headers: globle_header() });
  }

  create_admin_user(data, subdomain): Observable<any> {
    return this.http.post(baseUrl+'v1/create/users/'+subdomain, data, { headers: globle_header() });
  }

  get_organization_name(uuid, data): Observable<any>{
    return this.http.put(baseUrl+'v1/organization/activate/'+uuid,data,{ headers: globle_header() });
  }

  // get all normal users list in users & permission page **
  get_user_list_api(subdomain): Observable<any> {
    return this.http.get(baseUrl+'v1/users/'+subdomain, { headers: globle_header() });
  }
  
  /*p  get all user permission  field*/
  get_users_permissions_api(subdomain): Observable<any> {
    return this.http.get(baseUrl+'v1/get_users_permissions_fields/'+subdomain, { headers: globle_header() });
  }

  // update info for user & permission table users list
  edit_users_permissions_fields_api(uuid, data): Observable<any>{
    return this.http.put(baseUrl+'users_permissions_fields/edit/'+uuid, data, { headers: globle_header() });
  }

  // update info for user & permission table users list
  update_editable_users(data, user_id): Observable<any>{
    return this.http.put(baseUrl+'userupdate/'+user_id, data, { headers: globle_header() });
  }

  filter_pipeline_stage_deals(pipeline_uuid, filter_value, subdomain): Observable<any>{
    // filter_value = '?user_name='+'all' , filter_value = '?filter='+'all'
    // console.log(new_url+'v1/pipelines/'+pipeline_uuid+'/deals/'+subdomain+filter_value);
    return this.http.get(new_url+'v1/pipelines/'+pipeline_uuid+'/deals/'+subdomain+filter_value, { headers: globle_header() });
  }

  move_pipeline_deal(move_data, stage_uuid, subdomain): Observable<any>{
    return this.http.put(baseUrl+'v1/pipelines/deals/stages/'+stage_uuid+'/move_seentime/'+subdomain, move_data, { headers: globle_header() });
  }

  update_profile(data, subdomain): Observable<any>{
    return this.http.put(new_url+'v2/user-profile/update/'+subdomain, data, { headers: globle_header() });
  }
  
  // this api call into setting component.
  update_currency_api(data, subdomain): Observable<any>{
    return this.http.put(baseUrl+'v1/user-currency/update/'+subdomain, data, { headers: globle_header() });
  }
  
  get_user_profile_api(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/user-profile/'+subdomain, { headers: globle_header() });
  }

  delete_profile_image(): Observable<any>{
    return this.http.put(baseUrl+'v1/user-profile-picture/delete/false', {}, { headers: globle_header() });
  }

  active_deactive_users(data){
    return this.http.post(baseUrl+'user-enable', data, { headers: globle_header() });
  }

  update_comany_name(data, subdomain){
    return this.http.put(baseUrl+'v1/organization/update/'+subdomain, data, { headers: globle_header() });
  }

  addPredefine(data, subdomain){
    return this.http.post(baseUrl+'v1/create/lost_reason_field/'+subdomain, data, { headers: globle_header() });
  }

  get_lost_reason(subdomain){
    return this.http.get(baseUrl+'v1/lost_reason_field/'+subdomain, { headers: globle_header() });
  }

  lost_reason_deleted_api(uuid): Observable<any>{
    return this.http.put(baseUrl+'v1/lost_reason_field/delete/'+uuid,'', { headers: globle_header() });
  }

  lost_reason_edit_api(uuid,data): Observable<any>{
    return this.http.put(baseUrl+'v1/lost_reason_field/edit/'+uuid,data,{ headers: globle_header() });
  }
 
  // add followers
  addFollower(data, subdomain): Observable<any>{
    return this.http.post(baseUrl+'v1/create/followers_deals_field/'+subdomain, data, { headers: globle_header() });
  }

  //list of follower
  get_follower_list(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/followers_deals_field/'+subdomain,{ headers: globle_header() });
  }

   //edit follower
  edit_follower(uuid,data): Observable<any>{
    return this.http.put(baseUrl+'v1/followers_deals_field/edit/'+uuid, data, { headers:globle_header() });
  }

  //delete follower
  delete_follower(uuid): Observable<any>{
    return this.http.put(baseUrl+'v1/followers_deals_field/delete/'+uuid,'',{ headers: globle_header() });
  }

  get_followers_detail(uuid): Observable<any>{
    return this.http.get(baseUrl+''+uuid, { headers: globle_header() });
  }

  get_added_followers(deal_id, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/followers_deals/get_added_users/'+deal_id+'/'+subdomain,{ headers: globle_header() });
  }

  get_not_added_followers(deal_id, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/followers_deals/users/'+deal_id+'/'+subdomain, { headers: globle_header() });
  }

  post_add_follower(deal_id, subdomain, data): Observable<any>{
    return this.http.post(baseUrl+'v1/followers_deals/add_user/'+deal_id+'/'+subdomain, data,{ headers: globle_header() });
  }

  delete_followers(uuid): Observable<any>{
    return this.http.put(baseUrl+'v1/followers_deals/added_user/delete/'+uuid,'',{ headers: globle_header() });
  }

  //company setting
  company_setting(agent,data): Observable<any>{
    return this.http.post(baseUrl+'v1/create/organization', data,{ headers:globle_header() });
  }

  // get company data
  get_company_data(): Observable<any>{
    return this.http.get(baseUrl+'v1/list-organization',{ headers:globle_header() });
  }


  // API for search participant & search product
  serach_product_participant_api(url, keyword, subdomain): Observable<any>{
    return this.http.get(baseUrl+url+'/'+keyword+'/'+subdomain, { headers: globle_header() }).map(res=>{
      if (res != null) {
        return res;
      }else{
        return [];
      }
    });
  }

  // search deal name & search contact org name & search contact person API
  search_api(url, keyword, subdomain): Observable<any>{
    return this.http.get(baseUrl+url+keyword+'/'+subdomain, { headers: globle_header() }).map(res=>{
        if (res != null) {
          // console.log('deals_search_filter Response', json);
          return res;
        }else{
          return Observable.of([]);
        }
    });
  }
}

// pipeline api's
@Injectable()
export class Pipeline{
  constructor(public http: HttpClient){}
  // deal detail page api's
  active_green_pipeline_stage(pipeline_uuid, deal_id, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/pipelines/'+pipeline_uuid+'/stages/deal/'+deal_id+'/false', { headers: globle_header() });
  }
  // end

  // create new pipeline
  create_pipeline_api(data, subdomain): Observable<any>{
    return this.http.post(baseUrl+'v1/pipelines/create/'+subdomain, data, { headers: globle_header() });
  }
  
  //  list of pipelines
  get_pipeline_list_api(subdomain): Observable<any> {
    return this.http.get(baseUrl+'v1/pipelines/list/'+subdomain, { headers: globle_header() });
  }

  //  before load pipelines
  before_call_pipeline(pipeline_uuid, subdomain): Observable<any> {
    return this.http.get(baseUrl+'v1/pipelines/is_activate/'+pipeline_uuid+'/'+subdomain, { headers: globle_header() });
  }

  // Show Existing Pipeline
  show_exist_pipeline_api(pipeline_uuid, subdomain): Observable<any>{
    return this.http.get(new_url+'v2/pipelines/'+pipeline_uuid+'/'+subdomain, { headers: globle_header() });
  }
  
  // list of pipelines stages {Not useable}
  show_pipeline_api(pipeline_uuid){
    return this.http.get(baseUrl+'pipelines/'+pipeline_uuid+'/stages', { headers: globle_header() });
  }

  // create new pipeline stages
  create_new_stage_for_pipeline(pipeline_uuid, data){
    return this.http.post(baseUrl+'v1/pipelines/'+pipeline_uuid+'/stages/create', data, { headers: globle_header() });
  }
   
  // update Existing pipeline name
  update_exist_pipeline_api(pipeline_uuid, data){
    return this.http.put(baseUrl+'v1/pipelines/'+pipeline_uuid+'/update', data, { headers: globle_header() });
  }

  // update existing pipeline stages drag & drop
  update_exist_pipeline_stages(pipeline_uuid, data){
    return this.http.put(baseUrl+'v1/pipelines/'+pipeline_uuid+'/updatestages', data, { headers: globle_header() });
  }

  // delete Existing pipeline name
  delete_exist_pipeline_api(pipeline_uuid, subdomain){
    return this.http.put(new_url+'v2/pipelines/'+pipeline_uuid+'/delete/'+subdomain, {},{ headers: globle_header()});
  }

  // delete Existing pipeline stage amd move deals
  delete_exist_pipeline_stage_api(pipeline_stage_uuid, data){
    return this.http.put(baseUrl+"pipelines/stages/"+pipeline_stage_uuid+"/deletetest","",{ headers: globle_header() });
  }

  // delete Existing pipeline stage amd move deals
  delete_exist_pipeline_stage_api_new(pipeline_stage_uuid, data, subdomain){
    return this.http.post(baseUrl+"v1/pipelines/stages/deletenew/"+pipeline_stage_uuid+"/"+subdomain,data,{ headers: globle_header() });
  }

  // upadte Existing pipeline stage name
  upadte_exist_pipeline_stage(pipeline_uuid, stage_uuid, data){
    return this.http.put(baseUrl+'v1/pipelines/'+pipeline_uuid+'/stages/'+stage_uuid+'/update', data, { headers: globle_header() });
  }
}

@Injectable()
export class CustomFieldsService {
  constructor(private http: HttpClient){}
  // list of customize fields
  get_list_custom_field(value, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/get-fields-alltable/'+subdomain, { headers: globle_header() });
  }

  // list of customize fields
  get_list_deals_custom_field(tableName,value, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/get-fields/'+tableName+'/'+value+'/'+subdomain, { headers: globle_header() });
  }

  update_(tableName,data, uuid): Observable<any>{
    return this.http.put(baseUrl+tableName+'/fields/edit/'+uuid, data,{ headers: globle_header() });
  }

  update_details(tableName, data, uuid, subdomain){
    return this.http.put(baseUrl+'v1/'+tableName+'/multiple_fields/edit/'+uuid+'/'+subdomain, data, { headers: globle_header() });
  }
  
  delete_(tableName, uuid){
    return this.http.put(baseUrl+'v1/'+tableName+'/fields/'+uuid+'/delete','', { headers: globle_header() });
  }

  create_field_api(tableName,data, subdomain){
    return this.http.post(baseUrl+'v1/'+tableName+'/fields/create/'+subdomain, data, { headers: globle_header() });
  }

  checkbox_active(tableName,data){
    return this.http.put(baseUrl+'v1/'+tableName+'/fields/is_checked', data, { headers: globle_header() });
  }
}

@Injectable()
export class PersonService {
  constructor(private http: HttpClient){}

  create_person_api(data, subdomain): Observable<any>{
    return this.http.post(baseUrl+'v1/create/contacts_persons/'+subdomain, data, { headers: globle_header() });
  }

  create_contact_organization_api(data, subdomain): Observable<any>{
    return this.http.post(baseUrl+'v1/create/contact-organization/'+subdomain, data, { headers: globle_header() });
  }

  contact_person_list_api(charT, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/contacts_persons_filter/'+charT+'/'+subdomain, { headers: globle_header() });
  }

  contact_person_list_filter_word_api(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/contacts_persons/filterWord/'+subdomain, { headers: globle_header() });
  }

  contact_organization_list_api(charT, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/contact_organization_filter/'+charT+'/'+subdomain, { headers: globle_header() });
  }

  contact_organization_list_filter_word_api(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/contact_organization/filterWord/'+subdomain, { headers: globle_header() });
  }
}

@Injectable()
export class CompanyService {
  constructor(private http: HttpClient){}

  get_organization_currency_field_api(is_show, subdomain){
    return this.http.get(baseUrl+'v1/organization/currency/'+is_show+'/'+subdomain, { headers: globle_header() });
  }

  update_currency(data, uuid){
    return this.http.put(baseUrl+'organization/currency/update/'+uuid,data,{ headers: globle_header() });
  }

  // activity api services
  create_activity_api(data, subdomain){
    return this.http.post(baseUrl+'v1/create/activity_field/'+subdomain,data, { headers: globle_header() });
  }

  get_company_activity_fields_api(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/get_activity_field/'+subdomain, { headers: globle_header() });
  }

  get_userPermissionApi(key, subdomain){
    return this.http.get(baseUrl+'v1/get_users_permissions_fields_byKey/'+key+'/'+subdomain, { headers: globle_header() });
  }

  // get created and activeted true, fa-fa icon 
  get_activity_fa_fa_icon_api(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/get_activity_field/'+subdomain, { headers: globle_header() });
  }

  active_deactive_activity_api(data, activity_uuid, subdomain): Observable<any>{
    return this.http.put(baseUrl+'v1/activities_fields/edit/'+activity_uuid+'/'+subdomain, data, { headers: globle_header() });
  }

  updateCompanyLogo(data, subdomain){
    return this.http.put(new_url+'v2/organization/logo/update/'+subdomain, data, { headers: globle_header() });
  }

  getCompanyLogo(subdomain){
    return this.http.get(baseUrl+'v1/organization/logo/'+subdomain, { headers: globle_header() });
  }  

  company_name_validation_api(data){
    return this.http.post(baseUrl+'v1/check_valid_companyname', data, { headers: globle_header() });
  }

  create_location_api(data){
    return this.http.post(new_url+'v1/create/organization-address', data, { headers: globle_header() });
  }

  get_physical_location_api(org_uuid): Observable<any>{
    return this.http.get(new_url+'v1/get/organization-address/'+org_uuid, { headers: globle_header() });
  }

  delete_physical_location_api(uuid){
    return this.http.get(new_url+'v1/delete/organization-address/'+uuid, { headers: globle_header() });
  }
  // activity trigger api's start
  get_activity_trigger_status_api(): Observable<any>{
    return this.http.get(new_url+'v2/get/activity-trigger-status/false', { headers: globle_header() });
  }

  update_activity_trigger_status_api(deal_activity_trigger_uuid, data){
    return this.http.put(new_url+'v2/update/activity-trigger-status/'+deal_activity_trigger_uuid, data, { headers: globle_header() });
  }

  create_activity_trigger_api(subdomain, data){
    return this.http.post(new_url+'v2/add/activity-schedule-time/'+subdomain, data, { headers: globle_header() });
  }

  get_activity_trigger_data_api(subdomain){
    return this.http.get(new_url+'v2/get/activity-schedule-time/'+subdomain,{ headers: globle_header() });
  }
  // activity trigger api's end
}

@Injectable()
export class DealService {
  constructor(private http: HttpClient){}
  // deal detail page apis
  update_contact_orgnization(tableName, uuid, data){
    return this.http.put(baseUrl+'v1/salesmonster/'+tableName+'/edit/'+uuid, data, { headers: globle_header() });
  }

  update_currency_api(deal_id, data, subdomain){
    return this.http.put(baseUrl+'v1/pipelines/deals_currency/'+deal_id+'/update/'+subdomain, data, { headers: globle_header() });
  }
  // end

  // click on check box set activity on true/false
  change_activity_status_api(activity_uuid, data){
    return this.http.put(baseUrl+'v1/activity/activity_complete_status/'+activity_uuid, data, { headers: globle_header() });
  }

  edit_activity_api(activity_uuid, data){
    return this.http.put(baseUrl+'activity/edit/'+activity_uuid, data, { headers: globle_header() });
  }
  
  // delete activity indeal detail page
  delete_activity_api(activity_uuid, subdomain){
    return this.http.put(baseUrl+'v1/activity/delete/'+activity_uuid+'/'+subdomain, {}, { headers: globle_header() });
  }

  // edit and move created deal in another deal.
  edit_activity_new_api(activity_uuid, deal_uuid, data, subdomain){
    return this.http.put(baseUrl+'v1/activity/edit/'+activity_uuid+'/'+deal_uuid+'/'+subdomain, data, { headers: globle_header() });
  }

  get_statistics_deal_v2(dealstatus, pipeline_uuid, deal_admin_name, startDate, endDate, subdomain){
    return this.http.get(new_url+'v2/statistics/get-deal/'+dealstatus+'/'+pipeline_uuid+'/'+deal_admin_name+'/'+startDate+'/'+endDate+'/'+subdomain, { headers: globle_header() });
  }

  // update single field for organisation
  update_single_field_value_org(tableName, uuid, subdomain, data){
    // uuid => person_uuid, org_uuid
    return this.http.put(baseUrl+'v1/contact-organization-person/'+tableName+'/'+uuid+'/update/'+subdomain, data, {
      headers: globle_header()
    });
  }
  // update single field for organisation
  update_all_single_field_value_org_api(tableName, uuid, subdomain, data){
    // uuid => person_uuid, org_uuid
    return this.http.put(baseUrl+'v1/contact-organization-person/'+tableName+'/'+uuid+'/update-all/'+subdomain, data, { headers: globle_header() });
  }

  // update multiple field for deal details
  update_all_single_field_value_deal_api(deal_uuid, data, subdomain){
    // uuid => person_uuid, org_uuid
    return this.http.put(baseUrl+'v1/pipelines/deals/'+deal_uuid+'/update-all/'+subdomain, data, { headers: globle_header() });
  }
  // get participant listing
  get_participants_list_api(deal_uuid, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/participant_deals/get_participant/'+deal_uuid+'/'+subdomain, { headers: globle_header() });
  }

  // create participant listing
  add_new_participant(deal_id, data, subdomain){
    return this.http.post(baseUrl+'v1/participant_deals/add_people/'+deal_id+'/'+subdomain, data, { headers: globle_header() });
  }
  // delete participants
  delete_participant(uuid, subdomain){
    return this.http.put(baseUrl+'v1/participant_deals/added_participant/delete/'+uuid,{}, { headers: globle_header() });
  }

  // get logs on deal detail page
  get_log_api(deal_uuid, subdomain): Observable<any>{
    return this.http.get(new_url+'v1/deal/stage/log/'+deal_uuid+'/'+subdomain, { headers: globle_header() });
  }
}

@Injectable()
export class ActivityEventClass{
  constructor(public http: HttpClient){}
  get_activities_listing_api(subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/activity/'+subdomain, { headers: globle_header() });
  }
}

@Injectable()
export class StripeServices{
  constructor(public http: HttpClient){}
  header_token(){
    var headers = new HttpHeaders().set('Content-Type', 'application/json');
    return headers;
  }

  plan_listing_api(): Observable<any>{
    return this.http.get(new_url+'v1/plan/listing');
  }
  // this method call into company registration component.
  create_trail_stripe_api(data): Observable<any>{
    return this.http.post(new_url+'v1/trial/subscription', data);
  }

  get_billing_info_api(org_uuid): Observable<any>{
    return this.http.get(new_url+'v1/retrieve/subscription/'+org_uuid, { headers: this.header_token() } );
  }

  update_subscription_api(data): Observable<any>{
    return this.http.post(new_url+'v1/update/subscription',data,{ headers: this.header_token() });
  }

  get_subscription_history_api(org_uuid): Observable<any>{
    return this.http.get(new_url+'v1/retrieve/invoices/'+org_uuid, { headers: this.header_token() });
  }
}

@Injectable()
export class ProductService {
  constructor(private http: HttpClient){}
  
  product_person_list_filter_word_api(subdomain){
    return this.http.get(baseUrl+'v1/product/filterWord/'+subdomain, { headers: globle_header() });
  }

  product_person_list_api(charT,subdomain): Observable<any>{
    return this.http.get(new_url+'v2/product_filter/'+charT+'/'+subdomain, { headers: globle_header() });
  }

  // delete product in deal detail page
  delete_product_deal_detail_api(deal_uuid, product_uuid, subdomain){
    return this.http.put(new_url+'v1/delete/deal_product/'+deal_uuid+'/'+product_uuid+'/'+subdomain, {}, { headers: globle_header() });
  }

  // update product in deal detail page
  update_product_deal_detail_api(product_uuid, data, subdomain){
    return this.http.put(new_url+'v1/edit/deal_product/'+product_uuid+'/'+subdomain, data, { headers: globle_header() });
  }

  // get product detail in product detail page
  product_detail_api(product_uuid, subdomain): Observable<any>{
    return this.http.get(baseUrl+'v1/detail/deal_product/'+product_uuid+'/'+subdomain,{ headers: globle_header() });
  }

  // edit price in product detail page
  edit_product_price_api(deal_product_currency_uuid, data, subdomain){
    return this.http.put(new_url+'v1/edit/deal_product_currency/'+deal_product_currency_uuid+'/'+subdomain, data,{ headers: globle_header() });
  }

  // delete product price in product detail page
  delete_product_price_api(deal_product_currency_uuid,subdomain){
    return this.http.put(new_url+'v1/delete/deal_product_currency/'+deal_product_currency_uuid+'/'+subdomain, {},{ headers: globle_header() });
  }

  // add product price in product detail page
  add_product_price_api(product_uuid, data, subdomain){
    return this.http.post(new_url+'v1/add/deal_product_currency/'+product_uuid+'/'+subdomain, data,{ headers: globle_header() });
  }

  add_product_in_deal_detail(deal_uuid, data, subdomain){
    return this.http.post(new_url+'v1/add/products/'+deal_uuid+'/'+subdomain, data,{ headers: globle_header() });
  }

  product_submit_api(subdomain, data): Observable<any>{ 
    return this.http.post(baseUrl+'v1/create/products/'+subdomain,data,{ headers: globle_header() });
  }

}