import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { StripeServices, UserService }  from '../service/user.service';
import { SharedComponent }  from '../shared/_sharedComponent';
import { Baseurl }  from '../service/baseUrl';

declare var moment: any;
declare var toastr: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  private plan_list;
  private selected_plan_amount;
  private selected_plan_name;
  private selected_plan_currency;
  private number_of_seats = 1;
  private username;
  private _sharedComponent = new SharedComponent();
  private total_amt: number;
  base_class_obj = new Baseurl();

  data = {'seats': 1, 'amount': ''};
  final_data = {'total_seats': 1, 'total_amt': '' }
  address = { 'address_line1': '', 'address_line2': '', 'address_city': '', 
    'address_country': '', 'address_zip': '',
    'address_state': ''
  }

  payment = {'name':'', 'card_number': '', 'cvv': '', 
    'exp_month': '', 'exp_year': '', 'plan': '', "quantity": "1",
    'organization_uuid' : ''
  }
  
  constructor(public stripe_services: StripeServices, public authUser: UserService, public router: Router) { 
    this.get_plan_list();
    this.get_organization();  
  }
  
  ngOnInit() {}

  get_plan_list(){
    this.stripe_services.plan_listing_api().subscribe(res=>{
      // console.log('list of plan', res);
      this.plan_list = res.data;
      this.selected_plan_amount = res.data[0].amount;
      this.total_amt = res.data[0].amount;
      this.selected_plan_name = res.data[0].name;
      this.selected_plan_currency = res.data[0].currency;
      this.data.amount = res.data[0].amount;
      this.payment.plan = res.data[0].id;
      this.final_data.total_seats = 1;
    });
  }

  selectedIndex: number = 0;
  private plan_id;
  setIndex(index: number, plan) {
    this.payment.plan = plan.id;
    this.plan_id = plan.id;
    this.selected_plan_amount = plan.amount;
    this.total_amt = plan.amount;
    this.data.amount = plan.amount;
    this.selected_plan_name = plan.name;
    this.selected_plan_currency = plan.currency;
    this.selectedIndex = index;
    this.final_data.total_seats = 1;
  }

  private company_name;
  private user_name;
  get_organization(){
    this.authUser.get_organization_(this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('show organiozation', data.json());
      this.payment.organization_uuid = data['response_organizations'].Item.uuid;
      this.company_name = data['response_organizations'].Item.company_name;
      this.user_name = data['response_organizations_users'].user_username;
      this.get_billing_info(data['response_organizations'].Item.uuid);
    },
      // The 2nd callback handles errors.
      (err) => console.error(err),
      // The 3rd callback handles the "complete" event.
      () => console.log("observable complete")
    );
  }

  
  get_billing_info(uuid){
    this.stripe_services.get_billing_info_api(uuid).subscribe(data=>{
      // console.log('billing info', data);
      var address_obj =  {
                'address_line1': data.card_obj.address_line1,
                'address_line2': data.card_obj.address_line2,
                'address_city': data.card_obj.address_city,
                'address_country': data.card_obj.address_country,
                'address_zip': data.card_obj.address_zip,
                'address_state': data.card_obj.address_state
              }
      this.address = address_obj;
      this.plan_id =  data.plan.id;
      // var endDate:any = new Date(data.current_period_end * 1000);
      // var current_date = new Date();
      // var today_date = moment(this._sharedComponent.dateToYMD(current_date));
      // var trail_end_date = moment(this._sharedComponent.dateToYMD(endDate));
      // var trail_days = trail_end_date.diff(today_date, "days");
      // var trial_start:any = new Date(data.trial_start * 1000);
    });
  }

  update_billing(){
    this.final_data;
  }

  increment(){
    this.final_data.total_seats++;
    var seat = this.final_data.total_seats;
    this.total_amt = seat * this.selected_plan_amount;
  }

  decrement(){
    if (this.final_data.total_seats>1) {
      var seat = --this.final_data.total_seats;
      this.total_amt = seat * this.selected_plan_amount;
    }
  }

  onInputChange(event){
    if (event>1) {
      this.total_amt = event * this.selected_plan_amount;
    }
  }

  private err_validation;
  private stripe_error_msg;
  private btn_update_bill:boolean = false;
  save_payment(){
    this.err_validation = false;
    var data  = {
      'payment_detail': this.payment,
      'address_detail': this.address,
    }
    // console.log(data);
    this.btn_update_bill = true;
    
    this.stripe_services.update_subscription_api(data).subscribe(data=>{
      this.btn_update_bill = false;
      if(data['status'] == "active"){
        toastr.success('Your subscription has been successfully activated.'+'\n'+'invoice id '+ data.plan.id + '\n' + 'amount ' + (data.plan.amount/100.0));
        this.router.navigate(['/setting/billing']);
      }
      // console.log(data);
      if (data['error']) {
        this.stripe_error_msg = data.error.message;
        this.err_validation = true;
      }
    },(err)=>{this.btn_update_bill = false;});
  }
}