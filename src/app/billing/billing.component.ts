import { Component, OnInit } from '@angular/core';
import { StripeServices, UserService }  from '../service/user.service';
import { SharedComponent }  from '../shared/_sharedComponent';
import { Baseurl }  from '../service/baseUrl';

declare var moment: any;
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit {
	base_class_obj = new Baseurl();
  private _sharedComponent = new SharedComponent();
  constructor(public stripe_services: StripeServices, private auth_user_service: UserService){
    this.get_organization();
    this.get_user_list();
  }
  
  ngOnInit(){
  }

  get_organization(){
    this.auth_user_service.get_organization_(this.base_class_obj.subdomain).subscribe(data=>{
      if (data['organization']){
        this.get_billing_info(data['response_organizations'].Item.uuid);
      }
    });
  }

  billing_data;
  private data = {trail_days:''}
  get_billing_info(uuid){
  	this.stripe_services.get_billing_info_api(uuid).subscribe(data=>{
      console.log('details' , data)
      // var endDate:any = new Date(data.current_period_end * 1000);
      // var current_date = new Date();
      // var today_date = moment(this._sharedComponent.dateToYMD(current_date));
      // var trail_end_date = moment(this._sharedComponent.dateToYMD(endDate));
      // this.data.trail_days = trail_end_date.diff(today_date, "days");
      // var trial_start:any = new Date(data.trial_start * 1000);
      this.billing_data = data;
  	});
  }

  get_subscription_history(org_uuid){

  }

  private user_length;
  // get all users listing created by master admin
  get_user_list(){
    var subdomain = false;
    this.auth_user_service.get_user_list_api(subdomain).subscribe(data=>{
      // The 1st callback handles the data emitted by the observable.
      var user_list_data = data['users'].filter((x)=>{ return x.enabled == true });
      // console.log(user_list_data);
      this.user_length = user_list_data.length;
    },
      // The 2nd callback handles errors.
      (err) => console.error(err),
      // The 3rd callback handles the "complete" event.
      () => console.log("observable complete")
    );
  }
}