import { Component, OnInit, AfterViewInit, ViewChild,Input, Output, EventEmitter, Pipe, PipeTransform, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {IMyDrpOptions} from 'mydaterangepicker';
// import services
import { UserService, Pipeline, CustomFieldsService,CompanyService, ProductService } from '../service/user.service';
import { CognitoUtil } from "../service/cognito.service";
import { JqueryComponent } from '../shared/jqueryComponent';
import { SharedComponent } from '../shared/_sharedComponent';
import { BsModalComponent } from 'ng2-bs3-modal';
import { Baseurl } from '../service/baseUrl';

declare var toastr: any;
declare var $: any;

@Pipe({name: 'demoNumber'})
export class DemoNumber implements PipeTransform {
  transform(value, args:string[]) : any {
    let res = [];
    for (let i = 0; i < value; i++) {
        res.push(i);
      }
      return res;
  }
}
@Pipe({name: 'round'})
export class RoundPipe implements PipeTransform {
    /**
     *
     * @param value
     * @returns {number}
     */
    transform(value: number): number {
      return Math.round(value);
    }
}

@Component({
  selector: 'app-add-deals',
  templateUrl: './add-deals.component.html',
  styleUrls: ['./add-deals.component.css']
})
export class AddDealsComponent implements OnInit {
  @Input() pipeline: any;
  private attach_product: boolean = false;
  private dealForm: FormGroup;
  private submit: boolean = false;
  private pipeline_stage_value;
  private pipeline_id: any;
  private custom_field_list;
  private selected_contcat_person;
  private selected_contcat_org;
  private selected_deal_title;
  private selected_org_name;
  private companyUserPermissionName = "Entire Company";
  private companyUserPermission = true;
  private close_date;
  private uuid_;
  private arr_time = ['12:00 AM', '12:15 AM', '12:30 AM', '12:45 AM', '01:00 AM', '01:15 AM', '01:30 AM', '01:45 AM', '02:00 AM', '02:15 AM', '02:30 AM', '02:45 AM', '03:00 AM', '03:15 AM', '03:30 AM', '03:45 AM', '04:00 AM', '04:15 AM', '04:30 AM', '04:45 AM', '05:00 AM', '05:15 AM', '05:30 AM', '05:45 AM', '06:00 AM', '06:15 AM', '06:30 AM', '06:45 AM', '07:00 AM', '07:15 AM', '07:30 AM', '07:45 AM', '08:00 AM', '08:15 AM', '08:30 AM', '08:45 AM', '09:00 AM', '09:15 AM', '09:30 AM', '09:45 AM', '10:00 AM', '10:15 AM', '10:30 AM', '10:45 AM', '11:00 AM', '11:15 AM', '11:30 AM', '11:45 AM', '12:00 PM', '12:15 PM', '12:30 PM', '12:45 PM', '01:00 PM', '01:15 PM', '01:30 PM', '01:45 PM', '02:00 PM', '02:15 PM', '02:30 PM', '02:45 PM', '03:00 PM', '03:15 PM', '03:30 PM', '03:45 PM', '04:00 PM', '04:15 PM', '04:30 PM', '04:45 PM', '05:00 PM', '05:15 PM', '05:30 PM', '05:45 PM', '06:00 PM', '06:15 PM', '06:30 PM', '06:45 PM', '07:00 PM', '07:15 PM', '07:30 PM', '07:45 PM', '08:00 PM', '08:15 PM', '08:30 PM', '08:45 PM', '09:00 PM', '09:15 PM', '09:30 PM', '09:45 PM', '10:00 PM', '10:15 PM', '10:30 PM', '10:45 PM', '11:00 PM', '11:15 PM', '11:30 PM', '11:45 PM']
  private _sharedComponent = new SharedComponent();
  base_class_obj = new Baseurl();
  myDateRangePickerOptions: IMyDrpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
  };
  constructor(public compnayService: CompanyService,public authUser: UserService, 
              public cognito_user: CognitoUtil, public formBuilder: FormBuilder, 
              public pipeline_service: Pipeline, private product_services:ProductService,
              public custom_field_service: CustomFieldsService, private el: ElementRef){
      
      this.initialize_form();
      this.get_selectCurrency();
      this.get_currency();
      this.custom_field_service.get_list_deals_custom_field('deals_fields','False', this.base_class_obj.subdomain).subscribe(data=>{
        this.custom_field_list = data.filter(function(i){
          return i.is_show ===  true;
        });
        // console.log('custom field:', this.custom_field_list);
      });
      this.close_date = this._sharedComponent.dateToYMD(new Date());
  }

  ngOnInit(){}
  initialize_form(){
    this.dealForm = this.formBuilder.group({
      'contact_organization_name': [''],
      'contact_person_name': [''],
      'contact_person_uuid': ['', Validators.compose([Validators.required])],
      'contact_organization_uuid': ['', Validators.compose([Validators.required])],
      'deal_title': ['', Validators.compose([Validators.required])],
      'deal_value': ['', Validators.compose([Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')])],
      'deal_currency': [this.selectedCurrency, Validators.compose([Validators.required]) ],
      'pipeline_uuid': [this.uuid_ == undefined ? '' : this.uuid_, Validators.compose([Validators.required]) ],
      'user_username': [ this.cognito_user.getCurrentUser().getUsername() ],
      'stage_uuid': [''],
      'expected_close_date': [this._sharedComponent.dateToYMD(new Date()), Validators.compose([Validators.required]) ],
    });
  }

  @Output() deal_list: EventEmitter<any> = new EventEmitter<any>();
  @Output() dashboard: EventEmitter<any> = new EventEmitter<any>();

  increment_deal_listing(){
    this.authUser.get_deal_list_v2(this.base_class_obj.subdomain, 'all').subscribe(data=>{
      this.deal_list.emit(data);
    });
  }

  get_pipeline_stage(){
    // console.log('pipeline object',this.pipeline);
    this.dashboard.emit(this.pipeline);
  }
  
  show_pipeline(obj){
    this.uuid_ = obj.uuid;
    this.show_pipeline_stage(this.uuid_)
  }

  updateCompanyPermission(iscompanyPermission){
    if (iscompanyPermission){
      this.companyUserPermissionName = "Entire Company";
      this.companyUserPermission = true;
    }else{
      this.companyUserPermissionName = "Owner & followers";
      this.companyUserPermission = false;
    }
  }

  ngAfterViewInit(){
    $(document).ready(function(){
        $('.collapsesection').click( function() {
          $(".collapse-section").slideToggle(500);
          $(".model-cls").toggleClass("active");
          $(".form-panel").toggleClass("active");
        });
        
    });
  }

  observableSource = (keyword: any): Observable<any[]> => {
    let url = 'v1/contacts_persons/';
    if (keyword) {
      let persone = this.authUser.search_api(url, keyword, this.base_class_obj.subdomain);
      return persone;
    }else{
      // console.log('p')
      return Observable.of([])
    }
  }

  observableSource_org = (keyword: any): Observable<any[]> => {
    let url = 'v1/contact-organization/';
    if (keyword) {
        let org = this.authUser.search_api(url, keyword, this.base_class_obj.subdomain);
        return org;
    }else{
      return Observable.of([])
    }
  }
  
  focusOut_(){
    $('.ngui-auto-complete').css("display", "none");
  }
  
  focusFunction_person(){
    $('.contact-person .ngui-auto-complete').css("display", "block");
  }
  
  focusFunction_org(){
    $('.contact-org .ngui-auto-complete').css("display", "block");
  }

  valueChanged_contact_person(event){ // event is a organization_name
    // this.dealForm.controls['contact_organization_uuid'].setValue(event.contact_organization_name, {onlySelf: true});
    // this.selected_deal_title = event.contact_organization_name + ' deal';
  }

  valueChanged_contact_org(event){
    // this.selected_deal_title = event + ' deal';
  }

  // create new deal
  create_deal(f){
    // console.log('custom form values', f.value);
    this.submit = true;
    // console.log('staic form values', this.dealForm.value);
    var result={};
    for(var key in f.value) result[key] = f.value[key];
    for(var key in this.dealForm.value) result[key] = this.dealForm.value[key];

    for(var i in result){
      if (result[i] === '') {
        result[i] = ' ';
      }
    }
    
    if (result["contact_person_uuid"].uuid != undefined) {
      var person_uuid = { 'contact_person_uuid': result["contact_person_uuid"].uuid }
      // result["contact_person_uuid"] = result["contact_person_uuid"].person_name;
      result = Object.assign({}, result, person_uuid);
      delete result['contact_person_name'];
    }else{
      result['contact_person_name'] = result['contact_person_uuid'];
      delete result['contact_person_uuid'];
    }

    if (result["contact_organization_uuid"].uuid != undefined) {
      var organization_uuid = {'contact_organization_uuid': result["contact_organization_uuid"].uuid}
      // result["contact_organization_uuid"] = result["contact_organization_uuid"].contact_organization_name;
      result = Object.assign({}, result, organization_uuid);
      delete result['contact_organization_name'];
    }else{
      result['contact_organization_name'] = result['contact_organization_uuid'];
      delete result['contact_organization_uuid'];
    }
    
    result['owner_follower'] = this.companyUserPermission;
    if (result['deal_value'] == ' ') { result['deal_value'] = 0; }
    var prodcut_object = this.product_arr;
    if(prodcut_object.length != 0){
      result['is_product'] = true;
      result["products"] = prodcut_object;
    }else{
      result['is_product'] = false;
    }
    console.log('create deal', result);
    if (this.dealForm.valid) {
      this.authUser.add_deal(this.pipeline_id, result, this.base_class_obj.subdomain).subscribe(data=>{
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          $('#myModal').modal('hide');
          this.attach_product = false;
          this.empty_product();
          this.increment_deal_listing();
          this.get_pipeline_stage();
          toastr.success('create successfully');
          this.submit = false;
          this.initialize_form();
        }
      },(error)=>{
        if (error.status == 500) {
          toastr.success('Error');
        }
      });
    }
  }

  set_stage_uuid(stage_uuid){
    this.dealForm.controls["stage_uuid"].setValue(stage_uuid);
  }

  // list of all pipeline
  pipeline_list = [];
  pipeline_list_for_add_deal(pipeline_list){
    this.pipeline_list = pipeline_list;
    this.pipeline_id = pipeline_list[0].uuid;
    if (this.pipeline_list.length>0) {
      this.show_pipeline_stage(pipeline_list[0].uuid);
    }
  }

  // show pipelile stages by pipeline_uuid
  pipeline_stage: any = [];
  private stage_index:number = 0;
  show_pipeline_stage(uuid){
    // console.log("add deal popup stage list", uuid);
    this.pipeline_id = uuid;
    this.pipeline_service.show_exist_pipeline_api(uuid, this.base_class_obj.subdomain).subscribe(data=>{
      this.pipeline_stage = data.Items;
      this.pipeline_stage_value = data.Items.stages.length;
      var stage_uuid = data.Items.stages[0].uuid;
      this.stage_index = 0;
      this.set_stage_uuid(stage_uuid);
      // console.log('show_pipeline_satge', this.pipeline_stage);
    }, (err)=> {
        // console.clear();
        if (err.ok == false) { 
          // this.show_pipeline_stage(this.pipeline_id)
        }
    });
  }

  private allCurrency;
  //Method for get all currency of a organization
  private selectedCurrency = "";
  get_currency(){
    this.compnayService.get_organization_currency_field_api(true, this.base_class_obj.subdomain).subscribe(data=>{
        this.allCurrency = data['Items'];
    });
  }

  // Method for get selected currency of a organization
  get_selectCurrency(){
    this.authUser.get_user_profile_api(this.base_class_obj.subdomain).subscribe(data=>{
        if ("default_currency" in data['userprofile']){
            this.selectedCurrency = data['userprofile']['default_currency'];
        }else{
          this.selectedCurrency = 'USD';
        }
    });
  }

  product_json(){
    let data = { 'product_name': '', 'price': 0, 'qty': 1, 'discount': 0, 'sum': 0 , 'product_uuid': ''}
    return data
  }  

  public product_arr = [];
  public selected_product;
  delete_product(indx){
    this.product_arr.splice(this.product_arr.indexOf(indx), 1);
    this.getSum();
  }

  calculate_product_value(product){
    var discount = 100 - product.discount;
    var product_price = product.price * discount/100;
    product.sum = product_price * product.qty;
    this.getSum();
  }


  private total_cost_product:number;
  private total_product_qty:number = 0;
  getSum() {
    var sum = 0;
    var total_product_count = 0;
    for(let i = 0; i < this.product_arr.length; i++) {
      var discount = 100 - this.product_arr[i].discount;
      var product_price = this.product_arr[i].price * discount/100;
      sum += product_price * this.product_arr[i].qty;
      total_product_count += this.product_arr[i].qty;
      this.total_product_qty = total_product_count;
      this.total_cost_product = sum;
    }
    if(this.product_arr.length==0){  this.product_sum_or_prod_qty(); }
  }

  product_sum_or_prod_qty(){
    this.total_cost_product = null;
    this.total_product_qty = 0;
  }

  observableSource_product = (keyword: any): Observable<any[]> => {
    let url = 'v1/products/search/'+this.selectedCurrency;
    if (keyword) {
      let persone = this.authUser.serach_product_participant_api(url, keyword, this.base_class_obj.subdomain);
      persone.subscribe(data=>{
        if (data.length!=0) {
          $('.addProductButtonRow').css('display', 'block');
        }else{ // return []
          $('.addProductButtonRow').css('display', 'block');
        }
      });
      return persone;
    }else{
      // console.log('p')
      return Observable.of([])
    }
  }

  valueChanged_product(event){
    let prod_data = this.product_json();
    prod_data.product_name = event.product_name;
    prod_data.price = event.unit_price;
    prod_data.product_uuid = event.uuid;
    if (event.uuid) {
      this.selected_product = null;
      $('.addProductButtonRow').css('display', 'none');
      this.product_arr.push(prod_data);
      this.calculate_product_value(prod_data);
    }
  }

  add_as_new_product(product_name){
    let data = { "product_name": product_name };
    this.product_services.product_submit_api(this.base_class_obj.subdomain, data).subscribe(data=>{
      if (data.contact_person_response['ResponseMetadata'].HTTPStatusCode == 200) {
        $('.addProductButtonRow').css('display', 'none');
        this.selected_product = null;
        let prod_data = this.product_json();
        prod_data.product_name = data.data.product_name;
        prod_data.product_uuid = data.data.uuid;
        this.product_arr.push(prod_data);
        this.getSum();
      }
    });
  }

  empty_product(){
    this.selected_product = null;
    this.product_arr = [];
    this.product_sum_or_prod_qty();
  }
}