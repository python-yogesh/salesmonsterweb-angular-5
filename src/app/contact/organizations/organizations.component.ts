import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder, NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { BsModalComponent } from 'ng2-bs3-modal';
// import services
import { Baseurl } from '../../service/baseUrl';
import { UserService, PersonService,CustomFieldsService } from '../../service/user.service';
import { CognitoUtil } from "../../service/cognito.service";

declare var toastr: any;

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})

export class OrganizationsComponent implements OnInit {
  private user_list_data;
  private owner_id;
  custom_field: any;
  private loadTable: boolean;
  private loadFilterTable: boolean;
  private custom_field_list;
  private organizataionData;
  private organizationFilter;
  private loader_image:boolean;
  @ViewChild('addOrganization')
  addOrganization_modal: BsModalComponent;  
  base_class_obj = new Baseurl();
  constructor(private authUser: UserService, private cognito_util: CognitoUtil, 
              private person_services: PersonService,public custom_field_service: CustomFieldsService) {
    this.get_user_list();
    this.get_contact_organization_filter_list();
    this.list_of_fields();
    this.contact_organization_list('All');
    this.owner_id = this.cognito_util.getCurrentUser().getUsername();
    this.custom_field_service.get_list_deals_custom_field('contacts_organizations_fields','False', 
      this.base_class_obj.subdomain).subscribe(data=>{
        this.custom_field_list = data.filter(function(i){
          return i.is_important === true && i.is_show ===  true;
        });
        // console.log('custom field:', this.custom_field_list);
    });
  }

  public userSettings2: any = { "showRecentSearch": false, "showCurrentLocation":false, "showSearchButton": false, };

  ngOnInit(){}

  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.user_list_data = data['users'];
    },(err) => console.error(err) );
  }

  contact_organization_list(charT){
    this.loader_image=true;
    this.person_services.contact_organization_list_api(charT, this.base_class_obj.subdomain).subscribe(data=>{
      this.organizataionData = data;
      // console.log('contact organization list', data);
      this.loadTable = true;
      this.loader_image=false;
    });
  }

  get_contact_organization_filter_list(){
    this.person_services.contact_organization_list_filter_word_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.loadFilterTable = true;
      // this code for get alphabet first later
      var alphabetFilter=[];
      for(var key in data['Items']) {
        alphabetFilter.push(data['Items'][key]['contact_organization_name'].charAt(0).toUpperCase());
      }
      this.organizationFilter = new Set(alphabetFilter);
    });
  }


  // list of 'isActive' true Custome fields
  list_of_fields(){
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
    // console.log('isActive true custom field listing response', data);
    this.custom_field = data['contacts_organizations_fields'];
     
      for(let key of this.custom_field){
        if (key.is_checked){
            this.checkbox_arr.push({
                'uuid': key.uuid,
                'is_checked': key.is_checked
          });
        }
      }
      this.initialize_fields_data();
    });
  }

  private search_data;
  initialize_fields_data(){
    this.search_data = this.custom_field;
  }

  dynamic_arr(ss){
    if(Array.isArray(ss)){
        var d = []
        for(let i in ss){
          d.push(ss[i].id)
        }
        return d;
    }else{
      try {
            var data = JSON.parse(ss);
            if (data['formatted_address']) {
              return data['formatted_address'];
            }
            return ss;
          } catch (e) { return ss; }
    }
  }

  
  checkbox_arr = [];
  disable_save_btn: boolean = false;
  checkbox(check, uuid){
    this.disable_save_btn = true;
    
    if(check.checked){
        this.checkbox_arr.push({
          'uuid': uuid,
          'is_checked': check.checked
        });
    }
    else{
      for(var i = this.checkbox_arr.length - 1; i >= 0; i--) {
        if(this.checkbox_arr[i].uuid === uuid) {
            this.checkbox_arr[i].is_checked = false;
            break;
        }
      }
    }

    this.checkbox_arr.forEach(x=>{
      if (x.is_checked==true){
        this.disable_save_btn = false;
      }
    });
  }

  // update check box vlue true/false
  update_checkbox(){
    this.custom_field_service.checkbox_active('contacts_organizations_fields',this.checkbox_arr).subscribe(data=>{
      this.checkbox_arr = [];
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.list_of_fields();
        this.contact_organization_list('All');
      }
    });
  }


  filter_fields(event){
    this.initialize_fields_data();
    let val = event.target.value;
    if (val && val.trim() != ''){
      this.search_data = this.search_data.filter((item) => {
        let name: any = item;
        return (name.label.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }
  
  private address = { 'location':'', 'formatted_address':'' };
  autoCompleteCallback(e){
    this.address.formatted_address = e.data.formatted_address;
    this.address.location = e.data.geometry.location;
  }

  // save in DB
  add_organization(addOrganizationForm){
    addOrganizationForm.value['address'] = this.address.location == ''? '' : JSON.stringify(this.address);
    // console.log(addOrganizationForm.value);
    this.person_services.create_contact_organization_api(addOrganizationForm.value, this.base_class_obj.subdomain).subscribe(data=>{
      if (data.status == 200) {
        if (data.organizationstatus==true){
          toastr.warning('Organization Already Exist');
        }else{
          this.addOrganization_modal.close();
          this.owner_id = this.cognito_util.getCurrentUser().getUsername();
          addOrganizationForm.reset();
          this.contact_organization_list('All');
          toastr.success('Create Organization successfully');
        }
      }
    },(err) => console.error(err) );
  }
}