import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup,FormArray, Validators, FormBuilder, NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { BsModalComponent } from 'ng2-bs3-modal';
// import services
import { SharedComponent } from '../../shared/_sharedComponent';
import { UserService, PersonService, CustomFieldsService } from '../../service/user.service';
import { CognitoUtil } from "../../service/cognito.service";
import { Baseurl } from "../../service/baseUrl";

declare var toastr: any;

@Component({
  selector: 'app-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.css']
})

export class PeoplesComponent implements OnInit {
  private arr_time;
  private _sharedComponent = new SharedComponent();
  private user_list_data;
  private phone_arr = ['Work', 'Home', 'Mobile', 'Other'];
  private email_arr = ['Work', 'Home', 'Other'];
  private phone_option = this.phone_arr[0];
  private email_option = this.email_arr[0];
  private owner_id;
  private peopleData;
  private loadTable: boolean;
  private loadFilterTable:boolean;
  private loader_image:boolean;
  private custom_field_list;
  private peopleFilter;
  custom_field: any;
  selected_owner_id;
  public myForm: FormGroup;
  base_class_obj = new Baseurl();

  // this is for add email fields
  add_phone: any = [];
  servicesfeature_length: number = 1;
  listServiceFeature: any = [];
  servicefeature_name: string;

  add_phone_number() {
    this.add_phone.push('service' + this.servicesfeature_length);
    this.servicesfeature_length += 1;
  }

  Remove_phone_number(index) {
    this.servicesfeature_length -= 1;
    this.add_phone.splice(index, 1);
  } 
  // end of email fields

  @ViewChild('addPerson')
  addPerson_modal: BsModalComponent;
  
  private textChanged_email(event) { this.email_option = event; }

  constructor(private _fb: FormBuilder,private authUser: UserService, private cognito_user: CognitoUtil, 
              private person_services: PersonService,public custom_field_service: CustomFieldsService) {
    this.add_phone_number();
    this.get_user_list();
    this.list_of_fields();
    this.owner_id = this.cognito_user.getCurrentUser().getUsername();
    this.selected_owner_id = this.cognito_user.getCurrentUser().getUsername();
    var subdomain = 'false';
    this.custom_field_service.get_list_deals_custom_field('contacts_persons_fields','False', subdomain).subscribe(data=>{
      this.custom_field_list = data.filter(function(i){
        return i.is_important === true && i.is_show ===  true;
      });
      // console.log('custom field:', this.custom_field_list);
    });
    this.arr_time = this._sharedComponent.timeDuration;
  }

  ngOnInit(){
    this.get_contact_person_filter_list();
    this.get_contact_person_list('All');
    this.myForm = this._fb.group({
      person_name: ['', [Validators.required, Validators.minLength(2)]],
      owner_id: [this.owner_id],
      contact_organization_data: [''],
      person_phone: this._fb.array([ this.initPhone() ]),
      person_email: this._fb.array([ this.initEmail() ])
    });
  }
 
  
  private textChanged_phone(event) {  this.phone_option = event; }
  
  // save in DB
  add_person(AddPersonForm,perForm){
    // console.log("Add Person Form "+ AddPersonForm.value);
    // console.log('staic form values',perForm.value);
    var result={};
    for(var key in AddPersonForm.value) result[key] = AddPersonForm.value[key];
    for(var key in perForm.value) result[key] = perForm.value[key];
    for(var key in result['person_phone'])
    {
      // console.log(key);
      if(result['person_phone'][key]['phone'].trim() == ""){
        delete result['person_phone'][key];
      }
    }
    for(var key in result['person_email'])
    {
      // console.log(key);
      if(result['person_email'][key]['email'].trim() == ""){
        delete result['person_email'][key];
      }
    }
    if (result['person_phone'] == '') {
      delete result['person_phone']
    }
    if (result['person_email'] == '') {
      delete result['person_email']
    }
    
    this.person_services.create_person_api(result, this.base_class_obj.subdomain).subscribe(data=>{
      if (data.status == 200) {
        if (data.json().personstatus==true){
          toastr.warning(data.json().message);
        }else{
          this.addPerson_modal.close();
          this.get_contact_person_list('All');
          toastr.success('Create Person successfully');   
        }
      }
    },(err) => console.error(err) );
  }
  
  // get all users listing created by master admin
  get_user_list(){
    var subdomain = false;
    this.authUser.get_user_list_api(subdomain).subscribe(data=>{
      this.user_list_data = data['users'];
    },(err) => console.error(err) );
  }
  

  get_contact_person_list(charT){
    this.loader_image=true;
    this.person_services.contact_person_list_api(charT, this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('contact person list', data);
      this.loadTable = true;
      // this code for get alphabet first later
      this.peopleData = data;
      if(data!=''){
          this.loader_image=false;
        }
    });
  }

  get_contact_person_filter_list(){
    this.person_services.contact_person_list_filter_word_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.loadFilterTable = true;
      // this code for get alphabet first later
      var alphabetFilter=[];
      for(var key in data['Items']) {
        // console.log(data['Items'][key]['person_name'].charAt(0));
        alphabetFilter.push(data['Items'][key]['person_name'].charAt(0).toUpperCase());
      }
      this.peopleFilter = new Set(alphabetFilter);
    });
  }

  observableSource_org = (keyword: any): Observable<any[]> => {
    let url = 'v1/contact-organization/';
    if (keyword) {
        return this.authUser.search_api(url, keyword, this.base_class_obj.subdomain);
    }else{
      return Observable.of([])
    }
  }

  // list of 'isActive' true Custome fields
  list_of_fields(){
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
    // console.log('isActive true custom field listing response', data);
    this.custom_field = data['contacts_persons_fields'];
      for(let key of this.custom_field){
        if (key.is_checked){
            this.checkbox_arr.push({
                'uuid': key.uuid,
                'is_checked': key.is_checked
          });
        }
      }
      this.initialize_fields_data();
    });
  }

  private search_data;
  initialize_fields_data(){
    this.search_data = this.custom_field;
  }

  dynamic_arr(is_arr){
    if(Array.isArray(is_arr)){
        var d = []
        if (is_arr[0].email) {
          return is_arr[0].email +', '+ is_arr[0].selF 
        }
        if (is_arr[0].phone) {
          return is_arr[0].phone +', '+ is_arr[0].selF 
        }
        for(let i in is_arr){
          d.push(is_arr[i].id)
        }
        return d;
    }else{
      return is_arr;
    }
  }

  checkbox_arr = [];
  disable_save_btn: boolean = false;
  checkbox(check, uuid){
    this.disable_save_btn = true;
    
    if(check.checked){
        this.checkbox_arr.push({
          'uuid': uuid,
          'is_checked': check.checked
        });
    }
    else{
      for(var i = this.checkbox_arr.length - 1; i >= 0; i--) {
        if(this.checkbox_arr[i].uuid === uuid) {
            this.checkbox_arr[i].is_checked = false;
            break;
        }
      }
    }

    this.checkbox_arr.forEach(x=>{
      if (x.is_checked==true){
        this.disable_save_btn = false;
      }
    });
  }

  // update check box vlue true/false
  // --
  update_checkbox(){
    this.custom_field_service.checkbox_active('contacts_persons_fields',this.checkbox_arr).subscribe(data=>{
      this.checkbox_arr = [];
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.list_of_fields();
        this.get_contact_person_list('All');
      }
    });
  }
  

  filter_fields(event){
    this.initialize_fields_data();
    let val = event.target.value;
    if (val && val.trim() != ''){
      this.search_data = this.search_data.filter((item) => {
        let name: any = item;
        return (name.label.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }
  //this is for add field/
  initPhone() {
    return this._fb.group({
        phone: [''],
        selF: ['Work']
    });
  }
  
  initEmail() {
    return this._fb.group({
        email: [''],
        selF: ['Work']
    });
  }

  addPhone() {
      const control = <FormArray>this.myForm.controls['person_phone'];
      control.push(this.initPhone());
  }

  removePhone(i: number) {
      const control = <FormArray>this.myForm.controls['person_phone'];
      control.removeAt(i);
  }

  addEmail() {
      const control = <FormArray>this.myForm.controls['person_email'];
      control.push(this.initEmail());
  }

  removeEmail(i: number) {
      const control = <FormArray>this.myForm.controls['person_email'];
      control.removeAt(i);
  }

  private contact_organization_data;
  valueChanged_contact_org(event){ this.contact_organization_data = event; }
}