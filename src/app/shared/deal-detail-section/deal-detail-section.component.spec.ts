import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealDetailSectionComponent } from './deal-detail-section.component';

describe('DealDetailSectionComponent', () => {
  let component: DealDetailSectionComponent;
  let fixture: ComponentFixture<DealDetailSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealDetailSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealDetailSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
