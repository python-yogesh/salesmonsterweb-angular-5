import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CustomFieldsService, UserService, CompanyService, DealService, ActivityEventClass, Pipeline } from '../../service/user.service';
import { SharedComponent } from '../../shared/_sharedComponent';
import { Baseurl } from '../../service/baseUrl';

declare var toastr: any;

@Component({
  selector: 'app-deal-detail-section',
  templateUrl: './deal-detail-section.component.html',
  styleUrls: ['./deal-detail-section.component.css']
})
export class DealDetailSectionComponent implements OnInit {
	base_class_obj = new Baseurl();
	private _sharedComponent = new SharedComponent();
	@Output() sendDealEvent = new EventEmitter<any>();
	private arr_time;
  constructor(public authUser: UserService, public custom_field_service: CustomFieldsService,
  	public deal_services: DealService,) { 
  	this.view_list();
  	this.arr_time = this._sharedComponent.timeDuration;
  }

  ngOnInit() {}

  cancel_edit(){
    this.deal_edit_uuid= '';
  }

  private list=[];
  view_list(){
  	var listing = this._sharedComponent.Listing;
     listing.forEach((list_) => {
      this.list.push({
        icon_:list_.icon,
        id: list_.Text,
        text: `${list_.icon}&nbsp&nbsp<span class="label-component">${list_.Text}</span></br>${list_.Description}`,
      });
    });
  }
  private deal_add_field_dev;
  private deal_edit_uuid;
  update_org_field(tableName, org_data){
    // console.log(org_data);
    var data = { 'label': org_data.label, 'is_show': org_data.is_show, 'is_important': org_data.is_important}
    this.custom_field_service.update_details(tableName, org_data, org_data.uuid, this.base_class_obj.subdomain).subscribe(data=>{
      toastr.success('update successfully');
      this.deal_edit_uuid = '';
    });
  }

  delete_custom_field(table_name, field_uuid){
    var value = confirm('Are you sure you wish to remove this?');
    if (value) {
      this.custom_field_service.delete_(table_name, field_uuid).subscribe(data=>{
        // console.log(data);
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          this.sendDealEvent.emit();
          toastr.success('deleted');
        }
      });
    }
  }

  private errorMsg;
  private error_message_txt;
  create_custom_field_for_org(tableName, org_data){
    // console.log(org_data);
    this.custom_field_service.create_field_api(tableName, org_data, this.base_class_obj.subdomain).subscribe(data=>{
      if(data['response'] == "field name already exist"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else if(data['response'] == "it is reserved word"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else{
        this.errorMsg = false;
        this.sendDealEvent.emit();
        toastr.success('created');
      }
    });
  }

  private deal_field_api_key = '';
  show_deal_fields(deal_field){
    this.deal_field_api_key = deal_field.field_api_key;
  }

  // update single fields for deal details
  update_single_deal_custom_field_value(deal_form_data, deal_uuid, custom_field_type, field_api_key){
    deal_form_data['uuid'] = deal_uuid;
    if (custom_field_type == 'time_range') {
      deal_form_data[field_api_key] = { 'start_time_range': deal_form_data.start_time_range,
                         'end_time_range': deal_form_data.end_time_range }
      delete deal_form_data.start_time_range;
      delete deal_form_data.end_time_range;
    }
    // console.log(deal_form_data);

    this.authUser.update_deal_api(deal_uuid, deal_form_data, this.base_class_obj.subdomain)
    .subscribe(data=>{
      this.sendDealEvent.emit();
    });
  }

  // update multiple fields for deal details
  update_multiple_deal_custom_field_value(deal_form_data, deal_uuid){
    deal_form_data['uuid'] = deal_uuid;
    // console.log(deal_form_data);
    this.deal_services.update_all_single_field_value_deal_api(deal_uuid, deal_form_data, this.base_class_obj.subdomain)
    .subscribe(data=>{
      this.sendDealEvent.emit();
    });
  }

  isBigEnough(element) { 
    return (element.is_deleted==true); 
  }

  private contacts_deal_fields_obj = []; // form variable
  private contacts_deal_fields;
  list_of_fields(deal_obj){
    this.deal_field_api_key = '';
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('custom fields', data);
      this.deal_add_field_dev = false;
      // deal custom field data
      this.contacts_deal_fields = data.deal_fields.filter(this.isBigEnough);
      var contacts_deal_fields = data.deal_fields.filter(this.isBigEnough);
      var contact_deal_arr = [];
      for (var j in contacts_deal_fields){
        var value = deal_obj[contacts_deal_fields[j].field_api_key] != undefined ? deal_obj[contacts_deal_fields[j].field_api_key] : ''
        let obj = {
          'field_api_key': contacts_deal_fields[j].field_api_key,
          'label': contacts_deal_fields[j].label,
          'value': value,
          'is_type': contacts_deal_fields[j].is_type,
          'multiple_value': contacts_deal_fields[j].multiple_value,
          'deal_uuid': deal_obj.uuid
        }
        contact_deal_arr.push(obj);
      }
      // console.log('contact_deal_arr', contact_deal_arr)
      this.contacts_deal_fields_obj = contact_deal_arr;
    });
  }

  deal_data = {  type: '' , is_type: '', label: '', multiple_value: [], is_textbox: false, 
                is_show: false, is_important: false }

  field_type_for_org(event){
    var seleted_text =  event.id;
    this.deal_data.is_type = event.id;
    var is_type = this._sharedComponent.field_type(event);
    if (is_type == 'multiple_option' || is_type == 'single_option') {
       this.deal_data.is_textbox = true;
    }else{
      this.deal_data.is_textbox = false;
    }
    this.deal_data.type = is_type;
  }

  edit_btn_deal(deal_uuid, deal){
    deal.is_important = deal.is_important;
    deal.is_show = deal.is_show;
  }
}