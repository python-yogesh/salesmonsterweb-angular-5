import {FormGroup} from '@angular/forms';

export class ValidationService {
      static isMatching(group: FormGroup){
        var firstPassword = group.controls['newPassword'].value;
        var secondPassword = group.controls['confirm_password'].value;
        if((firstPassword && secondPassword) && (firstPassword != secondPassword)){
          // console.log("mismatch");
          group.controls['confirm_password'].setErrors({"pw_mismatch": true});
          return { "pw_mismatch": true };
        } else{
            // console.log('null')
          return null;
        }
    }
}