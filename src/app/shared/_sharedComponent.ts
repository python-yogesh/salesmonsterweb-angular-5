export class SharedComponent {
  constructor(){}
  timeDuration = [
      '12:00 AM','12:15 AM','12:30 AM','12:45 AM','01:00 AM',
      '01:15 AM','01:30 AM','01:45 AM','02:00 AM','02:15 AM','02:30 AM',
      '02:45 AM','03:00 AM','03:15 AM','03:30 AM','03:45 AM','04:00 AM',
      '04:15 AM','04:30 AM','04:45 AM','05:00 AM','05:15 AM','05:30 AM',
      '05:45 AM','06:00 AM','06:15 AM','06:30 AM','06:45 AM','07:00 AM',
      '07:15 AM','07:30 AM','07:45 AM','08:00 AM','08:15 AM','08:30 AM',
      '08:45 AM','09:00 AM','09:15 AM','09:30 AM','09:45 AM','10:00 AM',
      '10:15 AM','10:30 AM','10:45 AM','11:00 AM','11:15 AM','11:30 AM',
      '11:45 AM','12:00 PM','12:15 PM','12:30 PM','12:45 PM','01:00 PM',
      '01:15 PM','01:30 PM','01:45 PM','02:00 PM','02:15 PM','02:30 PM',
      '02:45 PM','03:00 PM','03:15 PM','03:30 PM','03:45 PM','04:00 PM',
      '04:15 PM','04:30 PM','04:45 PM','05:00 PM','05:15 PM','05:30 PM',
      '05:45 PM','06:00 PM','06:15 PM','06:30 PM','06:45 PM','07:00 PM',
      '07:15 PM','07:30 PM','07:45 PM','08:00 PM','08:15 PM','08:30 PM',
      '08:45 PM','09:00 PM','09:15 PM','09:30 PM','09:45 PM',
      '10:00 PM','10:15 PM','10:30 PM','10:45 PM','11:00 PM','11:15 PM','11:30 PM','11:45 PM'
  ]
  timeData = [
    '00:15','00:30','00:45','01:00','01:15','01:30','01:45',
    '02:00','02:15','02:30','02:45','03:00','03:15','03:30','03:45','04:00',
    '04:15','04:30','04:45','05:00','05:15','05:30','05:45','06:00','06:15',
    '06:30','06:45','07:00','07:15','07:30','07:45','08:00'
  ];

  Listing = [
    {
      'icon':'<span class="fa fa-font"></span>',
      'Text': 'Text',
      'Description': 'Text field is used to store texts up to 255 characters.'
    },
    {
      'icon':'<span class="fa fa-text-height"></span>',
      'Text': 'Large text',
      'Description': ' Long Text field is used to store texts longer that usual.'
    },
    // {
    //   'icon':'<span class="fa fa-magic"></span>',
    //   'Text': 'Autocomplete',
    //   'Description': 'Text field is used to store texts up to 255 characters and is searchable by all inserted option '
    // },
    {
      'icon':'<span class="fa fa-sort-numeric-desc"></span>',
      'Text': 'Numerical',
      'Description': 'Numerical field is used to store data such as amount of commision or other custom numerical data.'
    },
    // {
    //   'icon':'<span class="fa fa-usd"></span>',
    //   'Text': 'Monetary', 
    //   'Description': 'Monetary field is used to store data such as amount of commision'
    // },
    {
      'icon':'<span class="fa fa-check-square"></span>',
      'Text': 'Multiple option',
      'Description': 'Multiple option field lets you predefined a span of value to choosen from'
    },
    {
      'icon':'<span class="fa fa-eercast"></span>',
      'Text': 'Single option', 
      'Description': 'Single option field field lets you predefined a list of value out of which one is selected'
    },
    // {
    //   'icon':'<span class="fa fa-user"></span>',
    //   'Text': 'Person',
    //   'Description': ' Person field contain one organization out of all organization stored on your account'
    // },
    // {
    //   'icon':'<span class="fa fa-building-o"></span>',
    //   'Text': 'Organization',
    //   'Description': 'organization field contain one organization out of all organization stored on your account'
    // },
    // {
    //   'icon':'<span class="fa fa-user-circle-o"></span>',
    //   'Text': 'User',
    //   'Description': 'user field can contain one user amongest user of your account'
    // },
    {
      'icon':'<span class="fa fa-phone"></span>',
      'Text': 'Phone',
      'Description': 'A phone number field can contain phone number and skype name with a click-to-call functionality.'
    },
    {
      'icon':'<span class="fa fa-clock-o"></span>',
      'Text': 'Time', 
      'Description': 'Time field is used to store time,picked from handy inline time picker.'
    },
    // {
    //   'icon':'<span class="fa fa-stop-circle"></span>',
    //   'Text': 'Time range', 
    //   'Description': 'time range is used to store time range picked from handy inline time picker'
    // },
    {
      'icon':'<span class="fa fa-calendar"></span>',
      'Text': 'Date',
      'Description': 'date field is used to store date,picked from handy inline time calender'
    },
    {
      'icon':'<span class="fa fa-calendar-o"></span>',
      'Text': 'Date range',
      'Description': 'date range is used to store date range picked from handy inline time calender'
    },
    // {
    //   'icon':'<span class="fa fa-map-marker"></span>',
    //   'Text': 'Address',
    //   'Description': 'Address field is used to store addresses'
    // },
  ];

  //yyyy-mm-dd
  dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
  }

  dateToHMS(timestamp){
    var time = timestamp;
    var minutes = time.getMinutes();
    var ampm = time.getHours()<=11 ? 'AM' : 'PM';
    var minute = (minutes<=9) ? '0' + minutes : minutes;
    var hours = time.getHours()%12;
    var _time = (hours <= 9 ? ('0'+hours=='00' ? '12': '0'+hours) : hours) + ':' + minute + ' ' + ampm;
    return _time; // return this type of format 01:00 AM/PM
  }

  dateToHMS_24(timestamp){
    var time = timestamp;
    var minutes = time.getMinutes();
    var minute = (minutes<=9) ? '0' + minutes : minutes;
    var hours = time.getHours();
    var _time = (hours <= 9 ? '0'+hours : hours) + ':' + minute;
    return _time; // return this type of format 23:01
  }

  addHours(){
    var milliseconds = new Date().getTime() + (1 * 60 * 60 * 1000);
    var later = new Date(milliseconds);
    return later;
  }

  field_type(event){
    var seleted_text = event.id;
    if(seleted_text == 'Text' || seleted_text == 'Autocomplete'|| seleted_text =='Large text'|| 
      seleted_text == 'Numerical'|| seleted_text ==  'Monetary'|| seleted_text == 'Organization' || 
      seleted_text == 'Person'|| seleted_text == 'Phone' || seleted_text == 'Address'){
      return 'text';
    }
    if(seleted_text == 'Single option'|| seleted_text == 'User'){
      return 'single_option';
    }
    if(seleted_text == 'Multiple option' ){
      return 'multiple_option';
    }
     if(seleted_text == 'Date' || seleted_text == 'Date range'){
      return 'date';
    }
    if(seleted_text == 'Time'|| seleted_text == 'Time range'){
      return 'time';
    }
  }
}