declare var $:any;
export class JqueryComponent{
    initializeJs(){
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            
            $('input[type=radio]').tooltip({
                placement: "bottom",
                trigger: "hover",
            });
          });
    }
}