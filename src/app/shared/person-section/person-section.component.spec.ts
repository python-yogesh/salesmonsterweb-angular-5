import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionComponent } from './person-section.component';

describe('PersonSectionComponent', () => {
  let component: PersonSectionComponent;
  let fixture: ComponentFixture<PersonSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
