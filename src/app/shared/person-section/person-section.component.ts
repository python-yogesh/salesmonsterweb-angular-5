import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CustomFieldsService, UserService, CompanyService, DealService, ActivityEventClass, Pipeline } from '../../service/user.service';
import { SharedComponent } from '../../shared/_sharedComponent';
import { Baseurl } from '../../service/baseUrl';

declare var toastr: any;

@Component({
  selector: 'app-person-section',
  templateUrl: './person-section.component.html',
  styleUrls: ['./person-section.component.css']
})
export class PersonSectionComponent implements OnInit {
	base_class_obj = new Baseurl();
	private _sharedComponent = new SharedComponent();
	@Output() sendPersonEvent = new EventEmitter<any>();
	private arr_time;

  constructor(public authUser: UserService, public custom_field_service: CustomFieldsService,
  	public deal_services: DealService){
  	this.view_list();
  	this.arr_time = this._sharedComponent.timeDuration;
  }

  ngOnInit() {
  }

  private list=[];
  view_list(){
  	var listing = this._sharedComponent.Listing;
     listing.forEach((list_) => {
      this.list.push({
        icon_:list_.icon,
        id: list_.Text,
        text: `${list_.icon}&nbsp&nbsp<span class="label-component">${list_.Text}</span></br>${list_.Description}`,
      });
    });
  }
  private person_edit_uuid = '';
  update_person_field(tableName, person_data){
    // console.log(person_data);
    var data = { 'label': person_data.label, 'is_show': person_data.is_show, 'is_important': person_data.is_important}
    this.custom_field_service.update_details(tableName, person_data, person_data.uuid, this.base_class_obj.subdomain).subscribe(data=>{
      toastr.success('update successfully');
      this.person_edit_uuid = '';
    });
  }

  delete_custom_field(table_name, field_uuid){
    var value = confirm('Are you sure you wish to remove this?');
    if (value) {
      this.custom_field_service.delete_(table_name, field_uuid).subscribe(data=>{
        // console.log(data);
          this.sendPersonEvent.emit();
        if (data['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('deleted');
        }
      });
    }
  }

  private errorMsg;
  private error_message_txt;
  create_custom_field(tableName, person_data){
    // console.log(person_data);
    this.custom_field_service.create_field_api(tableName, person_data, this.base_class_obj.subdomain).subscribe(data=>{
      if(data['response'] == "field name already exist"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else if(data['response'] == "it is reserved word"){
        this.errorMsg = true;
        this.error_message_txt = data['response'];
      }else{
        this.errorMsg = false;
        this.sendPersonEvent.emit();
        toastr.success('created');
      }
    });
  }

  private person_field_api_key = '';
  show_person_fields(person_field){
    this.person_field_api_key = person_field.field_api_key;
  }

  isBigEnough(element, index, array) {
    return (element.is_deleted==true); 
  }

  private person_rename_css;
  private person_input_filed_css = 'none';
  person_rename_btn(rename_btn, input_field){
    this.person_input_filed_css = input_field;
    this.person_rename_css = rename_btn;
  }

  save_contact_person(section_person_name, contact_person_uuid){
    var data = {"fieldName":"person_name", "fieldValue": section_person_name.value};
    this.partial_method_for_contact_org_person('contacts_persons', contact_person_uuid, data);
  }

  partial_method_for_contact_org_person(table_name, uuid, data){
    this.deal_services.update_contact_orgnization(table_name, uuid, data)
      .subscribe(data=>{
        this.sendPersonEvent.emit();
        if (data['ResponseMetadata'].HTTPStatusCode==200) {
          this.person_rename_btn('block', 'none');
          toastr.success('update successfully');
        }
    });
  }

  private contacts_persons_fields_obj = []; // form variable
  private contacts_persons_fields;
  private person_add_field_dev;
  private deal;
  list_of_fields(deal_obj){
  	this.deal = deal_obj;
    this.person_field_api_key = '';
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
      // console.log('custom fields', data)
      this.person_add_field_dev = false;
      // person custom fields data
      this.contacts_persons_fields = data.contacts_persons_fields.filter(this.isBigEnough);
      var contacts_persons_fields = data.contacts_persons_fields.filter(this.isBigEnough);
      var contact_person_arr = [];
      for (var j in contacts_persons_fields){
        var value = deal_obj.contact_person_uuid[contacts_persons_fields[j].field_api_key] != undefined ? deal_obj.contact_person_uuid[contacts_persons_fields[j].field_api_key] : ''
        let obj = {
          'field_api_key': contacts_persons_fields[j].field_api_key,
          'label': contacts_persons_fields[j].label,
          'value': value,
          'is_type': contacts_persons_fields[j].is_type,
          'multiple_value': contacts_persons_fields[j].multiple_value,
          'person_uuid': deal_obj.contact_person_uuid.uuid
        }
        contact_person_arr.push(obj);
      }
      // console.log('contact_person_arr', contact_person_arr)
      this.contacts_persons_fields_obj = contact_person_arr;
    });
  }

  person_data = {  type: '' , is_type: '', label: '', multiple_value: [], is_textbox: false, 
                is_show: false, is_important: false }

  field_type_for_org(event){
    var seleted_text =  event.id;
    this.person_data.is_type = event.id;
    var is_type = this._sharedComponent.field_type(event);
    if (is_type == 'multiple_option' || is_type == 'single_option') {
       this.person_data.is_textbox = true;
    }else{
      this.person_data.is_textbox = false;
    }
    this.person_data.type = is_type;
  }

  edit_btn_person(person_uuid, person){
    person.is_important = person.is_important;
    person.is_show = person.is_show;
  }

  // update multiple person and organisatoin fields.
  update_all_custom_field_value(table_name, org_form_data_value, uuid){
    let data = org_form_data_value;
    this.deal_services.update_all_single_field_value_org_api(table_name, uuid, this.base_class_obj.subdomain, data).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode==200) {
        toastr.success('update successfully');
        this.sendPersonEvent.emit();
      }
    });
  }

  // update single person and org fields.
  update_single_org_custom_field_value(table_name, org_form_data_value, uuid){
    // uuid => person_uuid, org_uuid, deal_uuid
    let data = org_form_data_value;
    this.deal_services.update_single_field_value_org(table_name, uuid, this.base_class_obj.subdomain, data)
      .subscribe(data=>{
        this.sendPersonEvent.emit();
        toastr.success('success');
    });
  }

}
