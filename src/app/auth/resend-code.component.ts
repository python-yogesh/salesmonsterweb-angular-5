import { Component, OnInit, } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserLoginService } from "../service/user-login.service";

declare var waitingDialog: any;
declare var toastr: any;

@Component({
  selector: 'app-resend-code',
  templateUrl: './resend-code.component.html',
  styleUrls: ['./resend-code.component.css']
})
export class ResendCodeComponent implements OnInit {
  private email;
  private errorMessage;
  constructor(private location: Location, private router: Router, private loginService: UserLoginService) { }

  ngOnInit() {}
  
  onBack(){ this.location.back(); }

  onNext(){
    if (this.email == '' || this.email == undefined || this.email == null) {
      return;
    }
    this.loginService.resendCode(this.email, this);
    waitingDialog.show('Please wait');
  }

  cognitoCallback_confirm_code(error: any, result: any) {
    waitingDialog.hide();
    if (error != null) {
      toastr.error(error);
    } else {
      this.router.navigate(['/confirm', this.email]);
    }
  }
}