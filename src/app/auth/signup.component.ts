import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router} from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { UserLoginService } from "../service/user-login.service";
import { UserRegistrationService } from "../service/user-registration.service";
import { DynamoDBService } from "../service/ddb.service";
import { UserService } from "../service/user.service";
/*import { ConfirmComponent } from "../confirm/confirm";
*/
declare var waitingDialog: any;
declare var toastr: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  
  show = 'Show';
  private signupForm: FormGroup;
  constructor(public userRegistration: UserRegistrationService, public router: Router, 
    public ddb: DynamoDBService, public userService: UserLoginService,
    public auth_user_service: UserService, private fb: FormBuilder){
    toastr.options = { "progressBar": true}
    this.signupForm = fb.group({
      'name': [''],
      'email': ['', Validators.compose([Validators.required, Validators.pattern(this.check_valid_email())] )],
      'password': ['', Validators.compose([Validators.required])],
    });
  }

  check_valid_email(){
    var pureEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pureEmail;
  }

  ngOnInit() {
    this.userService.isAuthenticated(this);
  }
  ngAfterViewInit(){}
  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
  }

  // registration method
  onRegister(){
    let registrationUser = this.signupForm.value;
    if (this.signupForm.valid) {
      this.userRegistration.register(registrationUser, this);
      localStorage.setItem("user_detail", JSON.stringify(registrationUser));
      waitingDialog.show('Please wait');
    }
  }

  // call back for signUp
  cognitoCallback(message: string, result: any){
    waitingDialog.hide();
    if (message != null) { //error
        toastr.error(message);
    }else{ //success
        // this.create_group_admihjgfsgfsgfsgfsgfsgfsgfsdhn();
        this.router.navigate(['/confirm', this.signupForm.value.email])
    }
  }
  
  showPassword(input: any){
    input.type = input.type === 'password' ? 'text' : 'password';
    this.show = this.show === 'Show' ? 'Hide' : 'Show';
  }
}