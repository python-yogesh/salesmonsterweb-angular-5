import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
// import services
import { UserLoginService } from "../service/user-login.service";
import { CognitoUtil } from "../service/cognito.service";
import { UserRegistrationService } from "../service/user-registration.service";
import { DynamoDBService } from "../service/ddb.service";
import { UserService, CompanyService, StripeServices } from "../service/user.service";
import { Baseurl } from '../service/baseUrl';

declare var AWS: any;
declare var waitingDialog: any;

@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: ['./company-registration.component.css']
})

export class CompanyRegistrationComponent implements OnInit {
  organization_form: FormGroup;
  domain = new Baseurl();
  public user_object = {};
  private submitted = false;
  private validation_error;
  constructor(public userService: UserLoginService, public router: Router, 
              public location: Location, public cognito_user: CognitoUtil,
              public ddb: DynamoDBService, public auth_user_service: UserService, 
              public company_services: CompanyService,public stripe_services: StripeServices,
              public formBuilder: FormBuilder){
    this.userService.isAuthenticated(this);
    this.userService.getUserParameters(this);
    this.createForm();
    this.get_organization();
    this.get_existing_org();
  }
  private submit:boolean = false;
  createForm() {
    this.organization_form = this.formBuilder.group({
      'organization_type_uuid': ['', Validators.compose([Validators.required])],
      'company_name': ['', Validators.compose([Validators.required])],
      'employee_size': ['',  Validators.compose([Validators.required])],
    });
  }
  
  get_organization(){
    this.auth_user_service.get_organization_(this.domain.subdomain).subscribe(data=>{
      if (data['organization']){ // return undefined
        this.router.navigate(['/dashboard']);
      }else{
        this.router.navigate(['/company-registration']);
      }
    });
  }

  private existing_org_list = [];
  get_existing_org(){
    this.auth_user_service.getExistingOrgList().subscribe(data=>{
      this.existing_org_list = data['Items'];
    });
  }

  ngOnInit() {
      // setTimeout((router: Router) => {
      //     this.router.navigate(['/dashboard']);
      // }, 5000);  //5s
  }

  private timer;
  private company_loader_img = true;
  routerOnActivate(){
    this.timer = setInterval(()=>{
        this.router.navigate(['/dashboard']);
        this.routerOnDeactivate();
    }, 15000);
  }

  routerOnDeactivate() {
    clearInterval(this.timer);
  }

  isLoggedIn(message: string, isLoggedIn: boolean){
    if(!isLoggedIn){
      this.location.back();
    }
  }

  callback(){}
  // call back for getUserParameters
  callbackWithParam(result: any){
    for (let i = 0; i < result.length; i++){ 
      this.user_object[result[i].Name] = result[i].Value; 
    }
    // console.log(this.user_object);
  }
  
  private submite_disabled = false;
  // this is focusOutFunction check for comapany name charactor 
  focusOutFunction(company_name){
    var data = {'companyName': company_name.target.value}
    this.company_services.company_name_validation_api(data).subscribe(data=>{
      if (data) {
        this.submite_disabled = false;
        this.validation_error = false;
      }else{
        this.submite_disabled = true;
        this.validation_error = true;
      }
    },(error)=>{});
  }

  employees = [
    {value: '10', employee:'1 to 10'},
    {value: '25', employee:'upto 25'},
    {value: '50', employee:'upto 50'},
    {value: '100', employee:'upto 100'},
    {value: '101', employee:'more than 100'},
  ]

  create_company(){
    this.submitted = true;
    if (this.organization_form.valid) {
      this.submite_disabled = true;
      waitingDialog.show('Please wait');
      var data = this.organization_form.value;
      // let org_name: string = this.convertToSlug(name);
      this.auth_user_service.create_company_(data).subscribe(res=>{
        this.submite_disabled = false;
        // console.log('create company response:', res.json());
        if (res['organization_response']['ResponseMetadata'].HTTPStatusCode == 200) {
          waitingDialog.hide();
          this.routerOnActivate();
          this.company_loader_img = false;
          this.create_stripe_trail(res['organization_json'].Item);
        }
      });
    }
  }

  create_stripe_trail(data){
    var obj = { 'username': this.cognito_user.getCurrentUser().getUsername(), 
      "organization_uuid" : data.uuid, "organization_name" : data.company_name
    }
    this.stripe_services.create_trail_stripe_api(obj).subscribe(data=>{
      if (data.status_code==200){
        // alert('success ');
        // localStorage.setItem('customer_id', data.customer)
      }
    },(err)=>{
      alert(err);
    });
  }

  convertToSlug(Text:any){
      return Text
          .toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
  }

  onBack(){ this.location.back(); }
}