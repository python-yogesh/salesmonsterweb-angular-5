import { Component, OnInit, ViewChild } from '@angular/core';
import { Router} from "@angular/router";
import { BsModalComponent } from 'ng2-bs3-modal';
import { StripeServices }  from '../service/user.service';
import { UserLoginService } from "../service/user-login.service";
import { DynamoDBService } from "../service/ddb.service";

declare var waitingDialog: any;
declare var toastr: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  email: string;
  password: string;
  errorMessage: string;
  show = 'Show';

  @ViewChild('newPasswordpopup') new_password_modal: BsModalComponent;
  @ViewChild('forgotPassworsMsgPopup') forgotPassworsMsgPopup: BsModalComponent;
  
  constructor(public userService: UserLoginService, public router: Router, 
              public ddb: DynamoDBService, public stripe_services: StripeServices){
      toastr.options = { "progressBar": true }
      this.userService.isAuthenticated(this);
  }

  ngOnInit() {}

  // login method
  onLogin(){
    if (this.email == null || this.password == null) {
        toastr.error('All fields are required');
        return;
    }
    this.errorMessage = null;
    this.userService.authenticate(this.email, this.password, this);
    waitingDialog.show('Please wait');
  }

  // call back for login onLogin()
  cognitoCallback_login(message: string, result: any){
    if (message != null) { //error
        waitingDialog.hide();
        toastr.error(message);
        if (message === 'User is not confirmed.') {
           this.router.navigate(['/confirm', this.email])
        }
    }else{ //success
        waitingDialog.hide();
        this.ddb.writeLogEntry("login");
        this.router.navigate(['/dashboard']);
    }
  }
  // callback onNext
  callback_login_new_password(message: string, result: any){
    if (message != null) { //error
        waitingDialog.hide();
        toastr.error(message);
        if (message === 'User is not confirmed.') {
          this.router.navigate(['/confirm', this.email])
        }
    }else{ //success
          waitingDialog.hide();
          this.new_password_modal.close();
          this.userService.getUserAttributeVerificationCode();
          this.forgotPassworsMsgPopup.open();
    }
  }

  UserAttributeVerificationCode(VerificationCode){
    if (VerificationCode=='') {
      alert('Enter VerificationCode');
      return;
    }
    this.userService.verifyUserAttribute(VerificationCode, this);
  }

  cognitoCallback_forgot_pass_agent(result){
    if (result!=null) {
      toastr.error(result.message);
    }else{
      this.forgotPassworsMsgPopup.close();
      this.router.navigate(['/dashboard']);
    }
  }

  // get_subscription_detail(){
  //   this.stripe_services.get_subscription_detail(this.email).subscribe(data=>{
  //     localStorage.setItem('customer_id', data.customer_id);
  //   });
  // }

  onNext(new_password){
    if (new_password=='') {
      alert('Enter new password');
      return;
    }
    this.userService.newPassword(this.email, this.password ,new_password, this);
  }
  
  cognitoCallback_new_password(){
    waitingDialog.hide();
    this.open();
  };

  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
      // window.location.href = 'http://dev1.salesmonster.com:4200/dashboard';
    }
  }
  
  showPassword(input: any){
    input.type = input.type === 'password' ? 'text' : 'password';
    this.show = this.show === 'Show' ? 'Hide' : 'Show';
  }

  open() { this.new_password_modal.open('sm'); }
}