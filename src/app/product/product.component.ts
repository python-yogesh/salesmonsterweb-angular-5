import { Component, OnInit , ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalComponent } from 'ng2-bs3-modal';
import {IMyDrpOptions} from 'mydaterangepicker';
import { CompanyService,UserService,ProductService,CustomFieldsService } from '../service/user.service';
import { Baseurl } from '../service/baseUrl';
import { FormBuilder, FormGroup, Validators, NgForm} from '@angular/forms';

declare var toastr: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  private product_form: FormGroup;
  private productData = [];
  private loadFilterTable:boolean;
	private companyUserPermissionName = "Entire Company";
	private companyUserPermission = true;
  private loader_image:boolean;
	private user_list_data;
  private custom_field_list;
  private productFilter;
  custom_field: any;
  base_class_obj = new Baseurl();
  
  constructor(public compnayService: CompanyService, private authUser: UserService, public formBuilder: FormBuilder,
              private productUser: ProductService, public custom_field_service: CustomFieldsService) {
     this.get_currency();
     this.get_user_list();
     this.initialize_form();
     this.get_product_list('All');
     this.list_of_fields();
     var subdomain = 'false';
    this.custom_field_service.get_list_deals_custom_field('products_fields','False', subdomain).subscribe(data=>{
      this.custom_field_list = data.filter(function(i){
        return i.is_important === true && i.is_show === true;
      });
    });
  }

  ngOnInit() {}
  messageMapping: { [k: string]: string} = {'=0': 'No product', '=1': ' product', 'other': 'products' };
  @ViewChild('addProduct')
  addProduct_modal: BsModalComponent;

  updateCompanyPermission(iscompanyPermission){
    if (iscompanyPermission){
      this.companyUserPermissionName = "Entire Company";
      this.product_form.controls['owner_follower'].setValue(iscompanyPermission);

    }else{
      this.companyUserPermissionName = "Owner & followers";
      this.product_form.controls['owner_follower'].setValue(iscompanyPermission);
    }
  }

  private selected_prodduct = false;
  private check_all: boolean = false;

  change_(){
  }

  private allCurrency;
  //Method for get all currency of a organization
  private selectedCurrency = "USD";
  get_currency(){
    this.compnayService.get_organization_currency_field_api(true, this.base_class_obj.subdomain).subscribe(data=>{
      this.allCurrency = data['Items'];
    });
  }

  initialize_form(){
    this.product_form = this.formBuilder.group({
      "product_name": ['', Validators.compose([Validators.required])],
      'product_code': [''],
      'product_currency': [], 
      'cost_per_unit': [], 
      'direct_cost':[''],
      'unit_price':[''],
      'owner_follower':[this.companyUserPermission],
    });
  }

  // get users listing
  private owner_name;
  get_user_list(){
    this.authUser.get_user_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.user_list_data = data['users'];
    });
  }

  list_of_fields(){
    this.custom_field_service.get_list_custom_field('True', this.base_class_obj.subdomain).subscribe(data=>{
    // console.log('isActive true custom field listing response', data);
    this.custom_field = data['products_fields'];
      for(let key of this.custom_field){
        if (key.is_checked){
            this.checkbox_arr.push({
                'uuid': key.uuid,
                'is_checked': key.is_checked
          });
        }
      }
      this.initialize_fields_data();
    });
  }

  private search_data;
  initialize_fields_data(){
    this.search_data = this.custom_field;
  }

  dynamic_arr(is_arr){
    if(Array.isArray(is_arr)){
        var d = []
        for(let i in is_arr){
          d.push(is_arr[i].id)
        }
        return d;
    }else{
      return is_arr;
    }
  }

  checkbox_arr = [];
  disable_save_btn: boolean = false;
  checkbox(check, uuid){
    this.disable_save_btn = true;
    
    if(check.checked){
        this.checkbox_arr.push({
          'uuid': uuid,
          'is_checked': check.checked
        });
    }
    else{
      for(var i = this.checkbox_arr.length - 1; i >= 0; i--) {
        if(this.checkbox_arr[i].uuid === uuid) {
            this.checkbox_arr[i].is_checked = false;
            break;
        }
      }
    }

    this.checkbox_arr.forEach(x=>{
      if (x.is_checked==true){
        this.disable_save_btn = false;
      }
    });
  }

  // update check box vlue true/false
  update_checkbox(){
    this.custom_field_service.checkbox_active('products_fields',this.checkbox_arr).subscribe(data=>{
      this.checkbox_arr = [];
      if (data['ResponseMetadata'].HTTPStatusCode == 200) {
        this.list_of_fields();
        this.get_product_list('All');
      }
    });
  }

  filter_fields(event){
    this.initialize_fields_data();
    let val = event.target.value;
    if (val && val.trim() != ''){
      this.search_data = this.search_data.filter((item) => {
        let name: any = item;
        return (name.label.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  get_product_person_filter_list(){
    this.productUser.product_person_list_filter_word_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.loadFilterTable = true;
    });
  }

  private product_currency;
  get_product_list(charT){
    this.loader_image=true;
    this.productUser.product_person_list_api(charT, this.base_class_obj.subdomain).subscribe(data=>{
      var alphabetFilter=[];
      data.forEach( x => {
        x['is_checked'] = false;
        alphabetFilter.push(x.product_name.charAt(0).toUpperCase());
      });
      // console.log(data);
      this.productFilter = new Set(alphabetFilter);
      this.productData = data;
      this.product_currency = data[0].product_currency;
      console.log(data);
      this.loader_image = false;
    });
  }

  check_all_prod(){
    this.check_all = (this.check_all == true) ? false : true;
    if(this.check_all){
      this.productData.filter(x=>{ x['is_checked'] = true; })
    }else{
      this.productData.filter(x=>{ x['is_checked'] = false; })
    }
  }

  single_selection_checkbox(product, event){
    product.is_checked = event.target.checked;
    let check_value = this.productData.filter(x=>{ 
      if(x.is_checked == false){
        this.check_all = false;
        return x;
      }
    });
    if(check_value.length==0){
      this.check_all = true;
    }
  }

  filter_product_list(charT){
    this.loader_image=true;
    this.productUser.product_person_list_api(charT, this.base_class_obj.subdomain).subscribe(data=>{
      this.productData = data;
      this.loader_image = false;
    });
  }

  private submit;
  save_product(f){
    this.submit = true;
    var result = {};
    for(var key in f.value) result[key] = f.value[key];
    for(var key in this.product_form.value) result[key] = this.product_form.value[key];
    for(var i in result){
      if (result[i] === '' || result[i] === null){
        result[i] = 0;
      }
    }
    // console.log(result, this.product_form.valid);
    if(this.product_form.valid){
      this.productUser.product_submit_api(this.base_class_obj.subdomain, result).subscribe(data=>{
        if (data.contact_person_response['ResponseMetadata'].HTTPStatusCode == 200) {
          toastr.success('create successfully');
          this.addProduct_modal.close();
          this.product_form.reset();
          this.get_product_list('All');
        }
      });
    }
  }
}

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})

export class ProductDetailComponent implements OnInit{
  base_class_obj = new Baseurl();
  private sub: any;
  private prodcut_uuid: number;
  @ViewChild('PricePopUp') price_modal_popup: BsModalComponent;
  @ViewChild('priceForm') price_form_obj: NgForm;
  constructor(public compnayService: CompanyService, private authUser: UserService, private route: ActivatedRoute,
              private product_services: ProductService) {
    this.get_currency();
    this.get_selectCurrency();
    this.route.params.subscribe(params => {   
      this.prodcut_uuid = params['uuid']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.get_product_details_list();
  }
  ngOnInit() {}
  private allCurrency;
  //Method for get all currency of a organization
  private selectedCurrency = "";
  get_currency(){
    this.compnayService.get_organization_currency_field_api(true, this.base_class_obj.subdomain).subscribe(data=>{
      this.allCurrency = data['Items'];
    });
  }

  // Method for get selected currency of a organization
  get_selectCurrency(){
    this.authUser.get_user_profile_api(this.base_class_obj.subdomain).subscribe(data=>{
      if ("default_currency" in data['userprofile']){
          this.selectedCurrency = data['userprofile']['default_currency'];
      }else{
        this.selectedCurrency = 'USD';
      }
    });
  }

  private product_price_data = [];
  private product_name = '';
  private deal_product_response = [];
  get_product_details_list(){
    this.product_services.product_detail_api(this.prodcut_uuid, this.base_class_obj.subdomain).subscribe(data=>{
      // console.log(data)
      this.product_price_data = data.product_currency_response;
      this.deal_product_response = data.deal_product_response;
      this.product_name = data.product[0].product_name;
    });
  }

  private edit_form_value: boolean = true;
  save_price(priceForm){
    // console.log(priceForm);
    if (this.edit_form_value) { // add new price
      // code...
      this.product_services.add_product_price_api(this.prodcut_uuid, priceForm, this.base_class_obj.subdomain)
        .subscribe(data=>{
          this.get_product_details_list();
          this.price_modal_popup.close();
      });
    } else { // update existing price
      this.product_services.edit_product_price_api(this.product_price_uuid, this.price_form_obj.value, this.base_class_obj.subdomain).subscribe(data=>{
        this.get_product_details_list();
        this.price_modal_popup.close();
      });
    }
  }
  
  private product_price_uuid;
  edit_product_price(price){
    // this.price_form_obj.value.cost_per_unit = price.cost_per_unit
    this.price_modal_popup.open();
    this.product_price_uuid = price.uuid;
    this.price_form_obj.controls.cost_per_unit.patchValue(price.cost_per_unit);
    this.price_form_obj.controls.unit_price.patchValue(price.unit_price);
    this.price_form_obj.controls.direct_cost.patchValue(price.direct_cost);
    this.price_form_obj.controls.product_currency.patchValue(price.product_currency);
  }

  delete_product_price(price_uuid){
    this.product_services.delete_product_price_api(price_uuid, this.base_class_obj.subdomain).subscribe(data=>{
      this.get_product_details_list();
      toastr.success('deleted successfully');
    });
  }
}