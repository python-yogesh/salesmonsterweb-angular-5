import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BsModalComponent } from 'ng2-bs3-modal';
import { UserService, Pipeline } from '../service/user.service';
import { Baseurl } from '../service/baseUrl';

@Component({
  selector: 'app-pipelines',
  templateUrl: './pipelines.component.html',
  styleUrls: ['./pipelines.component.css'],
})
export class PipelinesComponent implements OnInit {
  private pipeline_list;
  private pipeline_length;
  private pipeline_edit_name;
  private pipeline_id;
  private stage_hide: boolean = false;
  private stage_lenght;
  private stages_move;
  private stage_count;
  private add_stage_condition:boolean = true;
  private edit_stage_name;
  private stage_uuid;
  private loader: boolean;
  private loader_image:boolean;
  private addPipelineForm: FormGroup;
  base_class_obj = new Baseurl();
  
  @ViewChild('addstage') addstage_modal: BsModalComponent;

  @ViewChild('DeletePopUpPipeline') DeletePopUpPipeline_modal: BsModalComponent;

  @ViewChild('DeleteStagesPopUp') DeleteStagesPopUp_modal: BsModalComponent;

  @ViewChild('add_pipeline_popup') addnewpipe_modal: BsModalComponent;
  
  constructor(public auth_user: UserService, public pipeline_service: Pipeline, 
    private _router:Router, private route: ActivatedRoute, private fb: FormBuilder){
    this.get_pipeline_list();
    this.initialize_form();
  }
  
  private submit: boolean = false;
  initialize_form(){
    this.submit = false;
    this.addPipelineForm = this.fb.group({
      'pname': ['', Validators.compose([Validators.required])],
      'is_active': false
    });
  }

  ngOnInit(){
    let currentUrl = this._router.url;
  }
  
  drag_drop_stages(pipeline_uuid){
    for(var j = 0; j<this.pipeline.stages.length; j++){
      this.pipeline.stages[j].position_id = j;
    }
    var data = this.pipeline.stages;
    // console.log('drag drop change position', data);
    this.pipeline_service.update_exist_pipeline_stages(pipeline_uuid, data)
    .subscribe(data=>{
      // console.log(data);
    });
  }

  private pipeline_form_value: boolean = true;
  create_pipeline(){
    this.submit = true;
    if (this.pipeline_form_value) { // create new pipeline
      if (this.addPipelineForm.valid) {
        var data = this.addPipelineForm.value;
        this.addnewpipe_modal.close();
        this.pipeline_service.create_pipeline_api(data, this.base_class_obj.subdomain).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200){
            this.get_pipeline_list();
          }
        });
      }
    } else { // update pipeline name...
      var data_update = { 'pname': this.addPipelineForm.value.pname}
      if (this.addPipelineForm.valid) {
        this.addnewpipe_modal.close();
        this.pipeline_service.update_exist_pipeline_api(this.pipeline_id, data_update).subscribe(data=>{
          if (data['ResponseMetadata'].HTTPStatusCode == 200){
            this.submit = false;
            this.get_pipeline_list();
          }
        });
      }
    }
  }

  // list og all pipeline
  get_pipeline_list(){
    this.loader_image=true;
    this.pipeline_service.get_pipeline_list_api(this.base_class_obj.subdomain).subscribe(data=>{
      this.pipeline_list = data.Items;
      this.pipeline_length = data.Items.length;
      // console.log('list of pipeline', this.pipeline_length);
      if (data.Count!=0) {
        this.loader_image=false;
        this.show_pipeline_stage(data.Items[0].uuid);
      }

      if(data!=''){this.loader_image=false;}
    }, error=> console.log('error=',error));
  }

  // show all stages for a particular pipeline
  pipeline: any;
  show_pipeline_stage(uuid){
    this.pipeline_service.show_exist_pipeline_api(uuid, this.base_class_obj.subdomain).subscribe(data=>{
      this.pipeline = data.Items;
      this.stage_lenght = data.Items.stages.length;
      this.stage_hide = true;
      console.log('show_pipeline_satge', this.pipeline);
    }, error=> console.log('error=',error) );
  }

  // open modal popup for edit/add stages
  add_stage_popup(pipeline_uuid, stage_name, stage_uuid, value, stage_arr, stage_count, stage_data){
    // value is get true/false
    this.addstage_modal.open();
    this.stage_uuid = stage_uuid;
    this.edit_stage_name = stage_name;
    this.add_stage_condition = value;
    this.pipeline_id = pipeline_uuid;
    this.stages_move = stage_arr; // 
    this.stage_count = stage_count;
    if (stage_data!=undefined) {
      this.deal_rotting = stage_data.deal_rotting;
      if (stage_data.deal_rotting) { // (condition) == true
        this.deal_rotting = true;
        this.on_rotting = true;
        this.off_rotting = false;
      }else{
        this.deal_rotting = false;
        this.off_rotting = true;
        this.on_rotting = false;
      }
      this.edit_rotting_days = stage_data.rotting_days;
    }else{  
      this.off_rotting = true;
      this.on_rotting = false;
    }
  }

  private off_rotting = true;
  private on_rotting;
  private deal_rotting = false; // true / false
  private edit_rotting_days:number = 1;

  off_rotting_event(){
    this.deal_rotting = false;
    this.off_rotting = true;
    this.on_rotting = false;
  }

  on_rotting_event(){
    this.deal_rotting = true;
    this.on_rotting = true;
    this.off_rotting = false;
  }

  private err_msg;
  create_new_pipeline_stage(stage_name){
    if (this.on_rotting) {// if true
      if (this.edit_rotting_days==0) {
        this.err_msg = true;
        return;
      }
    }else{
      this.edit_rotting_days = 0;
    }
    this.err_msg = false;
    var data = { 'sname': stage_name, 'deal_rotting': this.deal_rotting, 
      'rotting_days': this.edit_rotting_days, 'position_id': this.stage_lenght
    }
    
    this.pipeline_service.create_new_stage_for_pipeline(this.pipeline_id, data).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200){
          this.addstage_modal.close();
          this.show_pipeline_stage(this.pipeline_id);
      }
    });
  }

  // click on edit button set pipeline uuid
  pipeline_edit(pipeline_uuid, pname){
    this.pipeline_id = pipeline_uuid;
    this.pipeline_edit_name = pname;
    this.addPipelineForm.controls.pname.patchValue(pname);
    this.addnewpipe_modal.open('sm');
  }

  // update pipeline name
  update_pipeline(){
  }

  // update pipeline stage name
  update_pipeline_stage(stage_name, ){
    if (this.on_rotting) {// if true
      if (this.edit_rotting_days==0) {
        this.err_msg = true;
        return;
      }
    }else{
      this.edit_rotting_days = 0;
    }
    this.err_msg = false;
    var data = { 'sname': stage_name, 'deal_rotting': this.deal_rotting, 
      'rotting_days': this.edit_rotting_days }
    
    this.pipeline_service.upadte_exist_pipeline_stage(this.pipeline_id, this.stage_uuid, data).subscribe(data=>{
      // console.log(data);
      if (data['ResponseMetadata'].HTTPStatusCode == 200){
        this.addstage_modal.close();
        this.show_pipeline_stage(this.pipeline_id);
      }
    });
  }

  // delete pipeline name
  delete_pipeline(){
    this.pipeline_service.delete_exist_pipeline_api(this.pipeline_id, this.base_class_obj.subdomain).subscribe(data=>{
      if (data['ResponseMetadata'].HTTPStatusCode == 200){
        this.DeletePopUpPipeline_modal.close();
        this.get_pipeline_list();
      }
    });
  }

  data = {}
  onChangeRadioButton(stageuuid){
    this.data = { 'stage_id': stageuuid }
  }

  // delete existing pipeline stage and move stage in another stage
  pipeline_stage_delete(){
    this.pipeline_service.delete_exist_pipeline_stage_api_new(this.stage_uuid, this.data, this.base_class_obj.subdomain)
      .subscribe(data=>{
      // console.log(data);
      if (data['ResponseMetadata'].HTTPStatusCode == 200){
        this.DeleteStagesPopUp_modal.close();
        this.show_pipeline_stage(this.pipeline_id);
      }
    });
  }

  addstage(){ this.addnewpipe_modal.open(); }

  pipeline_delete_btn(pipeline_uuid){
    this.pipeline_id = pipeline_uuid;
    this.DeletePopUpPipeline_modal.open();
  }

  dismissed_delete_popup(){ this.data = {} }
}